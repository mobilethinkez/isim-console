package com.mobilethinkez.sim.application.tabs.ui;

import java.util.Arrays;
import java.util.List;

import com.mtz.isim.application.views.HomeView;
import com.mtz.sim.application.data.DatabaseHelper;
import com.vaadin.addon.sqlcontainer.query.QueryDelegate;
import com.vaadin.addon.sqlcontainer.query.QueryDelegate.RowIdChangeEvent;
import com.vaadin.data.Item;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

public class IMSIForm extends Form implements ClickListener,QueryDelegate.RowIdChangeListener {

	private Button save = new Button("Save", (ClickListener) this);
	private Button cancel = new Button("Cancel", (ClickListener) this);
	private Button edit = new Button("Edit", (ClickListener) this);
	
//	private final ComboBox cities = new ComboBox();

	private HomeView app;
	public IMSIForm(HomeView homeView){
        this.app = app;

        /*
         * Enable buffering so that commit() must be called for the form before
         * input is written to the data source. (Form input is not written
         * immediately through to the underlying object.)
         */
        setWriteThrough(false);

        /* Init form footer */
        HorizontalLayout footer = new HorizontalLayout();
        footer.setSpacing(true);
        footer.addComponent(save);
        footer.addComponent(cancel);
        footer.addComponent(edit);
        footer.setVisible(false);
        setFooter(footer);

         /*
         * Field factory for overriding how the fields are created.
         */
        setFormFieldFactory(new DefaultFieldFactory() {
            @Override
            public Field createField(Item item, Object propertyId,
                    Component uiContext) {
                Field field;
                {
                    field = super.createField(item, propertyId, uiContext);
                }

                if (propertyId.equals("imsi")) {
                    /* Add a validator for postalCode and make it required */
                      field.setRequired(true);
                } else if (propertyId.equals("EMAIL")) {
                    /* Add a validator for email and make it required */
                    field.addValidator(new EmailValidator(
                            "Email must contain '@' and have full domain."));
                    field.setRequired(true);
                }
                /* Set null representation of all text fields to empty */
                if (field instanceof TextField) {
                    ((TextField) field).setNullRepresentation("");
                }

                field.setWidth("200px");

                /* Set the correct caption to each field */
                for (int i = 0; i < DatabaseHelper.NATURAL_COL_ORDER.length; i++) {
                    if (DatabaseHelper.NATURAL_COL_ORDER[i].equals(propertyId)) {
                        field.setCaption(DatabaseHelper.COL_HEADERS_ENGLISH[i]);
                    }
                }
                return field;
            }
        });

        /* Add PersonForm as RowIdChangeListener to the CityContainer */
//        app.getDbHelp().getSimInfoContainer().addListener(this);
    }

    public void buttonClick(ClickEvent event) {
        Button source = event.getButton();
        if (source == save) {
            /* If the given input is not valid there is no point in continuing */
            if (!isValid()) {
                return;
            }
            commit();
        } else if (source == cancel) {
            discard();
        } else if (source == edit) {
            setReadOnly(false);
        }
    }

    @Override
    public void setItemDataSource(Item newDataSource) {
        if (newDataSource != null) {
            setReadOnly(false);
            List<Object> orderedProperties = Arrays
                    .asList(DatabaseHelper.NATURAL_COL_ORDER);
            super.setItemDataSource(newDataSource, orderedProperties);
             setReadOnly(true);
            getFooter().setVisible(true);
        } else {
            super.setItemDataSource(null);
            getFooter().setVisible(false);
        }
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        save.setVisible(!readOnly);
        cancel.setVisible(!readOnly);
        edit.setVisible(readOnly);
    }

	@Override
	public void rowIdChange(RowIdChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}

   
}
