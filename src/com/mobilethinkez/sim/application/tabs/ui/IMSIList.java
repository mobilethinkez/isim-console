package com.mobilethinkez.sim.application.tabs.ui;

import com.mtz.sim.application.data.SimInfoContainer;
import com.vaadin.ui.Table;

public class IMSIList extends Table{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2490195148157658191L;
	public IMSIList(SearchFilter searchFilter){
		//public IMSIList(final HomeView app,SearchFilter searchFilter){
				 
        setSizeFull();
        setContainerDataSource(SimInfoContainer.getImsiList(searchFilter));
        
        setVisibleColumns(SimInfoContainer.NATURAL_COL_ORDER);
        setColumnHeaders(SimInfoContainer.COL_HEADERS_ENGLISH);
        /*
         * Make table selectable, react immediatedly to user events, and pass events to the
         * controller (our main application)
         */
        setSelectable(true);
        setImmediate(true);
       // addListener((ValueChangeListener) app);
        /* We don't want to allow users to de-select a row */
        setNullSelectionAllowed(false);
    	setColumnCollapsingAllowed(true);
		setColumnReorderingAllowed(true);
  }
	 /**
     * Checks that selection is not null and that the selection actually exists
     * in the container. If no valid selection is made, the first item will be
     * selected. Finally, the selection will be scrolled into view.
     */
    public void fixVisibleAndSelectedItem() {
        if ((getValue() == null || !containsId(getValue())) && size() > 0) {
            Object itemId = getItemIds().iterator().next();
            select(itemId);
            setCurrentPageFirstItemId(itemId);
        } else {
            setCurrentPageFirstItemId(getValue());
        }
    }
}
