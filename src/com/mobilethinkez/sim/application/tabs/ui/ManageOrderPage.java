package com.mobilethinkez.sim.application.tabs.ui;

import com.mtz.isim.application.module.ordermanagement.OrderManagementModuleView;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.sim.application.data.OrderContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;

public class ManageOrderPage extends Panel  {
	private TextField tf;
	private NativeSelect fieldToSearch;
	private OrderManagementModuleView homeView;
	public ManageOrderPage(final OrderManagementModuleView homeView){
		this.homeView = homeView;
		setCaption("MANAGE ORDER");
		//setSizeFull();
		setWidth("400px");
		setHeight("200px");
		setStyleName("manageOrderForm");
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true, true, true, true);
		setContent(formLayout);
		/* Create UI components */
		
		//Order ID, IMSI PREFIX, SIM PREFIX, IMSI SERIAL NO, SIM
		fieldToSearch = new NativeSelect("Field to search");
		
		/* Initialize fieldToSearch */
		for (int i = 0; i < OrderContainer.NATURAL_COL_ORDER_SEARCH.length; i++) {
			fieldToSearch.addItem(OrderContainer.NATURAL_COL_ORDER_SEARCH[i]);
			fieldToSearch.setItemCaption(OrderContainer.NATURAL_COL_ORDER_SEARCH[i],OrderContainer.COL_HEADERS_ENGLISH_SEARCH[i].toString());
		}
		
		fieldToSearch.setNullSelectionAllowed(false);
	
		tf = new TextField("Enter Value");
		
	
		Button search = new Button("Search");
		search.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				performSearch();
			}

		});
		

		/* Add all the created components to the form */
		addComponent(fieldToSearch);
		addComponent(tf);
		
	//	addComponent(searchName);
		addComponent(search);

		
	}
	private void performSearch() {
		String searchTerm = (String) tf.getValue();
		LoggerExtension.log(1, "searchTerm::"+searchTerm+" ,fieldToSearch.getValue()::"+fieldToSearch.getValue());
		SearchFilter searchFilter = new SearchFilter(fieldToSearch.getValue(),searchTerm);
		homeView.search(searchFilter);
	}
}
