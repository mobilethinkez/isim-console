package com.mobilethinkez.sim.application.tabs.ui;

import com.mtz.isim.application.module.ordermanagement.ViewOrderTab;
import com.mtz.isim.application.views.HomeView;
import com.mtz.sim.application.data.OrderContainer;
import com.vaadin.ui.Table;

public class OrderList  extends Table{

	
	 public OrderList(HomeView app,SearchFilter searchFilter) {
		 
        setSizeFull();
        setContainerDataSource(OrderContainer.getOrderList(searchFilter));
        
        setVisibleColumns(OrderContainer.NATURAL_COL_ORDER);
        setColumnHeaders(OrderContainer.COL_HEADERS_ENGLISH);
        /*
         * Make table selectable, react immediatedly to user events, and pass events to the
         * controller (our main application)
         */
        setSelectable(true);
        setImmediate(true);
        addListener((ValueChangeListener) app);
        /* We don't want to allow users to de-select a row */
        setNullSelectionAllowed(false);
    	setColumnCollapsingAllowed(true);
		setColumnReorderingAllowed(true);
  }
	 
	 public OrderList(ViewOrderTab app,String orderid) {
		 
	        setSizeFull();
	        setContainerDataSource(OrderContainer.getOrderList(orderid));
	        setVisibleColumns(OrderContainer.NATURAL_COL_ORDER);
	        setColumnHeaders(OrderContainer.COL_HEADERS_ENGLISH);
			
	        /*
	         * Make table selectable, react immediatedly to user events, and pass events to the
	         * controller (our main application)
	         */
	        setSelectable(true);
	        setImmediate(true);
	        addListener((ValueChangeListener) app);
	        /* We don't want to allow users to de-select a row */
	        setNullSelectionAllowed(false);
	    	setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
	  }
		  
	 
}
