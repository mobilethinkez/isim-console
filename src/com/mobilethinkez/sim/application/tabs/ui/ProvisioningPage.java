/**
 * 
 */
package com.mobilethinkez.sim.application.tabs.ui;

import java.util.ArrayList;
import java.util.List;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.views.HomeView;
import com.mtz.isim.application.vo.IMSISummary;
import com.mtz.isim.application.vo.SimInfo;
import com.mtz.sim.application.data.DatabaseHelper;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

/**
 * @author Satish.Yadav
 *
 */
public class ProvisioningPage extends VerticalLayout implements Property.ValueChangeListener{
	private TextField tf;
	private NativeSelect fieldToSearch;
	private HomeView homeView;
	private Panel panel;

	String quantity;
	String provImsi;
	String notProImsi;
	Label provImsiText;
	Label notProvImsiText;
	Form form;
	Form imsiDetailForm;
	IMSISummary imsiSummary;
	SimInfo simInfo;
	private static final String COMMON_FIELD_WIDTH = "12em";


	public ProvisioningPage(HomeView homeView){
		this.homeView = homeView;
		panel = new Panel();
		panel.setCaption("Provisioning");
		panel.setWidth("500px");
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true, true, true, true);
		panel.setContent(formLayout);

		//Order ID, IMSI PREFIX, SIM PREFIX, IMSI SERIAL NO, SIM
		fieldToSearch = new NativeSelect("Field to search");
		fieldToSearch.addListener(this);

		/* Initialize fieldToSearch */
		for (int i = 0; i < DatabaseHelper.NATURAL_COL_ORDER_SEARCH.length; i++) {
			fieldToSearch.addItem(DatabaseHelper.NATURAL_COL_ORDER_SEARCH[i]);
			fieldToSearch.setItemCaption(DatabaseHelper.NATURAL_COL_ORDER_SEARCH[i],DatabaseHelper.COL_HEADERS_ENGLISH_SEARCH[i].toString());
		}

		fieldToSearch.setNullSelectionAllowed(false);
		fieldToSearch.setImmediate(true);
		
		tf = new TextField("Enter Value");
		Button search = new Button("Search");
		search.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				String fieldSearch = fieldToSearch.getValue().toString();
				if(fieldSearch.equalsIgnoreCase("ORDER_ID")){
					// search for order id
					getOrderIDSummury();
					if(form!=null){
						removeComponent(form);
					}
					if(imsiDetailForm!=null){
						removeComponent(imsiDetailForm);
					}
					//call orderid details form page
					callOrderDetails();	
				}else if(fieldSearch.equalsIgnoreCase("IMSI")){
					//search for imsi and sim serial number
					//performSearch();
					performImsiSearch();
					if(imsiDetailForm!=null){
						removeComponent(imsiDetailForm);
					}
					imsiDetailForm();
				}
			}

		});
		/* Add all the created components to the form */
		panel.addComponent(fieldToSearch);
		panel.addComponent(tf);

		panel.addComponent(search);
		addComponent(panel);
	}


	protected void callOrderDetails() {
		form = new Form();
		BeanItem<IMSISummary> personItem = new BeanItem<IMSISummary>(imsiSummary);
		form.setCaption("IMSI Provisioning Summary");
		form.setFormFieldFactory(new PersonFieldFactory());
		form.setItemDataSource(personItem); // bind to POJO via BeanItem
		   // The cancel / apply buttons
		form.setWidth("500px");
		form.setHeight("280px");
		form.setStyleName("formImsiSummary");
		HorizontalLayout buttonsHLayout = new HorizontalLayout();
		buttonsHLayout.setSpacing(true);
     
		Button provisionButton = new Button("Provision Now", new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
                try {
                   doProvisionNow(fieldToSearch.getValue().toString(),(String) tf.getValue());
                } catch (Exception e) {
                    // Ignored, we'll let the Form handle the errors
                }
            }
        });
		buttonsHLayout.addComponent(provisionButton);
		  // button for showing the internal state of the POJO
        Button unprovisionButton = new Button("De Provision",
                new Button.ClickListener() {
                    public void buttonClick(ClickEvent event) {
                    	doDeProvision(fieldToSearch.getValue().toString(),(String) tf.getValue());
                    }
                });
        buttonsHLayout.addComponent(unprovisionButton);
        
        Button exportButton = new Button("Export",
                new Button.ClickListener() {
                    public void buttonClick(ClickEvent event) {
                    	exportSIMProvisioningDataToFile();
                    }
                });
        exportButton.setEnabled(false);
        buttonsHLayout.addComponent(exportButton);
		
		form.getFooter().addComponent(buttonsHLayout);
        form.getFooter().setMargin(false, false, true, true);
        addComponent(form);

	}
	protected void imsiDetailForm() {
		imsiDetailForm = new Form();
		BeanItem<SimInfo> personItem = new BeanItem<SimInfo>(simInfo);
		imsiDetailForm.setCaption("IMSI Status");
		imsiDetailForm.setFormFieldFactory(new ImsiFieldFactory());
		imsiDetailForm.setItemDataSource(personItem); // bind to POJO via BeanItem
		   // The cancel / apply buttons
		imsiDetailForm.setWidth("500px");
		imsiDetailForm.setHeight("300px");
		imsiDetailForm.setStyleName("formImsiSummary");
		HorizontalLayout buttonsHLayout = new HorizontalLayout();
		buttonsHLayout.setSpacing(true);
     
		Button provisionButton = new Button("Provision Now", new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
                try {
                   doProvisionNow(fieldToSearch.getValue().toString(),(String) tf.getValue());
                } catch (Exception e) {
                    // Ignored, we'll let the Form handle the errors
                }
            }
        });
		buttonsHLayout.addComponent(provisionButton);
		  // button for showing the internal state of the POJO
        Button unprovisionButton = new Button("De Provision",
                new Button.ClickListener() {
                    public void buttonClick(ClickEvent event) {
                    	doDeProvision(fieldToSearch.getValue().toString(),(String) tf.getValue());
                    }
                });
        buttonsHLayout.addComponent(unprovisionButton);
        
        Button exportButton = new Button("Export",
                new Button.ClickListener() {
                    public void buttonClick(ClickEvent event) {
                    	exportSIMProvisioningDataToFile();
                    }
                });
        exportButton.setEnabled(false);
        
        buttonsHLayout.addComponent(exportButton);
		
        imsiDetailForm.getFooter().addComponent(buttonsHLayout);
        imsiDetailForm.getFooter().setMargin(false, false, true, true);
        addComponent(imsiDetailForm);

	}
	protected void exportSIMProvisioningDataToFile() {
		// TODO Auto-generated method stub
		getWindow().showNotification("export SIM Provisioning Data To File is Done",Notification.TYPE_HUMANIZED_MESSAGE);
	}


	protected void doDeProvision(String colName, String colValue) {
		// TODO Auto-generated method stub
		DBManager.doDeProvision(colName,colValue);
		getWindow().showNotification("De Provision is Done",Notification.TYPE_HUMANIZED_MESSAGE);
		form.requestRepaintRequests();
	}


	protected void doProvisionNow(String colName, String colValue) {
		// TODO Auto-generated method stub
		DBManager.doProvision(colName,colValue);
	
		getWindow().showNotification("Provision is Done",Notification.TYPE_HUMANIZED_MESSAGE);
		
		form.requestRepaintRequests();
	}
	private class PersonFieldFactory extends DefaultFieldFactory {

		@Override
		public Field createField(Item item, Object propertyId,
				Component uiContext) {
			Field f;
			f = super.createField(item, propertyId, uiContext);
			if ("provImsi".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Total Successfully Provisioned IMSI:");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
			} else if ("notProImsi".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Total Not Provisione IMSI:");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
			}
			else if ("quantity".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Total Order Quantity:");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
			}

			return f;
		}
	}
	private class ImsiFieldFactory extends DefaultFieldFactory {

		@Override
		public Field createField(Item item, Object propertyId,
				Component uiContext) {
			Field f;
			f = super.createField(item, propertyId, uiContext);
			if ("order_id".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("ORDER ID");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				//tf.setVisible(false);
			} else if ("imsi".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("IMSI");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				//tf.setVisible(false);
			}else if ("serial_number".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("SERIAL NUMBER");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
			}
			else if ("msisdn".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("MSISDN");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
			}else if ("status".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
			}
			else if ("pin1".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setVisible(false);
			}

			else if ("puk1".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setVisible(false);
			}
			else if ("pin2".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setVisible(false);
			}
			else if ("puk2".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setVisible(false);
			}else if ("access_control".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setVisible(false);
			}else if ("ki".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setVisible(false);
			}else if ("adm1".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("STATUS");
				tf.setEnabled(false);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setVisible(false);
			}

			return f;
		}
	}
		private void getOrderIDSummury() {
			String searchTerm = (String) tf.getValue();
			if (searchTerm == null || searchTerm.equals("")) {
				getWindow().showNotification("Search term cannot be empty!",Notification.TYPE_WARNING_MESSAGE);
				return;
			}
			LoggerExtension.log(1, "searchTerm::"+searchTerm+" ,fieldToSearch.getValue()::"+fieldToSearch.getValue()+
					" ,fieldToSearch.getItemCaption(fieldToSearch.getValue()::"+fieldToSearch.getItemCaption(fieldToSearch.getValue()));
			List<SearchFilter> searchFilters = new ArrayList<SearchFilter>();
			SearchFilter searchFilter = new SearchFilter(fieldToSearch.getValue(),searchTerm);
			getOrderDetails(searchFilter);
		}


		private void getOrderDetails(SearchFilter searchFilter) {
			String[] info =	DBManager.getImsiProvisionInfo(searchFilter);
			imsiSummary = new IMSISummary();
			
			if(info!=null)
			{
				imsiSummary.setProvImsi(info[0]);
				imsiSummary.setNotProImsi(info[1]);
				int total = Integer.parseInt(info[0])+Integer.parseInt(info[1]);
				String totalstr = String.valueOf(total);
				imsiSummary.setQuantity(totalstr);
			}


		}

		private void performSearch() {
			String searchTerm = (String) tf.getValue();
			if (searchTerm == null || searchTerm.equals("")) {
				getWindow().showNotification("Search term cannot be empty!",
						Notification.TYPE_WARNING_MESSAGE);
				return;
			}
			LoggerExtension.log(1, "searchTerm::"+searchTerm+" ,fieldToSearch.getValue()::"+fieldToSearch.getValue()+
					" ,fieldToSearch.getItemCaption(fieldToSearch.getValue()::"+fieldToSearch.getItemCaption(fieldToSearch.getValue()));
			List<SearchFilter> searchFilters = new ArrayList<SearchFilter>();

			/* If this is NOT a City search, one filter is enough. */
			//      searchFilters.add(new SearchFilter(fieldToSearch.getValue(), searchTerm, fieldToSearch.getItemCaption(fieldToSearch.getValue()), searchTerm));
			//      SearchFilter[] sf = {};
			//		 homeView.searchImsi(searchFilters.toArray(sf));


			SearchFilter searchFilter = new SearchFilter(fieldToSearch.getValue(),searchTerm);
		//	homeView.searchImsi(searchFilter);
		}

		
		
		/*
	     * Shows a notification when a selection is made.
	     */
	    public void valueChange(ValueChangeEvent event) {
	       // getWindow().showNotification("Selected city: " + event.getProperty());
	    	tf.setValue("");
	    	if(form!=null){
				removeComponent(form);
			}

	    }
	    
	    private void performImsiSearch() {
			String searchTerm = (String) tf.getValue();
			if (searchTerm == null || searchTerm.equals("")) {
				getWindow().showNotification("Search term cannot be empty!",
						Notification.TYPE_WARNING_MESSAGE);
				return;
			}
			LoggerExtension.log(1, "searchTerm::"+searchTerm+" ,fieldToSearch.getValue()::"+fieldToSearch.getValue()+
					" ,fieldToSearch.getItemCaption(fieldToSearch.getValue()::"+fieldToSearch.getItemCaption(fieldToSearch.getValue()));
			List<SearchFilter> searchFilters = new ArrayList<SearchFilter>();

			/* If this is NOT a City search, one filter is enough. */
			//      searchFilters.add(new SearchFilter(fieldToSearch.getValue(), searchTerm, fieldToSearch.getItemCaption(fieldToSearch.getValue()), searchTerm));
			//      SearchFilter[] sf = {};
			//		 homeView.searchImsi(searchFilters.toArray(sf));


			SearchFilter searchFilter = new SearchFilter(fieldToSearch.getValue(),searchTerm);
			//homeView.searchImsi(searchFilter);
			//imsiList = new IMSIList(searchFilter);
		//	simInfo = SimInfoContainer.getImsiList(searchFilter);
			getImsiDetails(searchFilter);
		}


		private void getImsiDetails(SearchFilter searchFilter) {
			try {
				simInfo = DBManager.getImsiList(searchFilter);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}
