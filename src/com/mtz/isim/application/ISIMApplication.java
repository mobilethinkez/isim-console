package com.mtz.isim.application;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.vaadin.jouni.animator.AnimatorProxy;
import org.vaadin.jouni.animator.AnimatorProxy.AnimationEvent;
import org.vaadin.jouni.animator.client.ui.VAnimatorProxy.AnimType;

import com.mtz.isim.application.module.admin.AdminModule;
import com.mtz.isim.application.module.customercare.ManageImsiModule;
import com.mtz.isim.application.module.ordermanagement.OrderManagementModule;
import com.mtz.isim.application.module.provisioning.ProvisioningModule;
import com.mtz.isim.application.module.report.ReportsModule;
import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.util.PropertyManager;
import com.mtz.isim.application.views.HomeView;
import com.mtz.isim.application.views.LoginView;
import com.mtz.isim.application.vo.UserData;
import com.mtz.isim.module.ModulableApplication;
import com.mtz.isim.module.OsgiModule;
import com.mtz.sim.application.permission.Entitlements;
import com.vaadin.Application;
import com.vaadin.event.MouseEvents;
import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.terminal.gwt.server.WebApplicationContext;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ISIMApplication extends Application implements ModulableApplication
{
	private static final String CLASSNAME="ISIMApplication";
	private static final long serialVersionUID = -5416292893349646656L;
	private SacSession mSession;
	private transient VerticalLayout mContentAreaLayout;
	private transient VerticalLayout mMainLayout;
	private transient Window mainApplicationWindow;
	private transient AnimatorProxy mProxy;
	private transient HomeView homeView;
	private static WebApplicationContext applicationContext;
	
	@Override
	public void init() {
		PropertyManager.getPropertyManager();
		LoggerExtension.init();
		DBManager.getInstance();
		LoggerExtension.info(CLASSNAME, "inside the ISIMApplication class.....");
		Entitlements.load(this);
		mProxy = new AnimatorProxy();
		// Instantiate main window and set the page title and the theme.
		mainApplicationWindow = new Window("Sim Administration Application");
		setTheme("tadtheme");
		
		// Instantiate main layout.
		mMainLayout = new VerticalLayout();
		mMainLayout.setCaption(null);
		// Set the size of the root layout to full width and height
		mainApplicationWindow.setContent(mMainLayout);
		mainApplicationWindow.getContent().setSizeFull();
		setMainWindow(mainApplicationWindow);
		// Create custom layout from "index.html" template.
		CustomLayout lCustomLayout = new CustomLayout("index");

		//mSession = new SIMManagerSession(this);
		mSession = new SacSession(this,applicationContext);
		LoginView lLoginView = new LoginView(mSession);

		mMainLayout.addComponent(lCustomLayout);
		mMainLayout.addComponent(lLoginView);
		mMainLayout.setExpandRatio(lLoginView, 1.0f);
		
	}

	public ApplicationContext getApplicationContext() {
		applicationContext = (WebApplicationContext) getContext();
		return applicationContext;
	}
	
//	@Override
	public void showView(final Component aComp) {
		mProxy.animate(mContentAreaLayout.getComponent(0),
				AnimType.ROLL_LEFT_CLOSE).setDuration(500).setDelay(100);

		mProxy.addListener(new AnimatorProxy.AnimationListener() {
			public void onAnimation(AnimationEvent event) {
				if (event.getAnimation().getType().equals(AnimType.ROLL_LEFT_CLOSE)) {
					mContentAreaLayout.removeAllComponents();
					mContentAreaLayout.addComponent(aComp);
					mContentAreaLayout.setExpandRatio(aComp, 1.0f);
					mProxy.animate(aComp, AnimType.ROLL_RIGHT_OPEN)
							.setDuration(500).setDelay(100);
				}
			}
		});
	}

//	@Override
	public void renderHomePage() {	

		mainApplicationWindow.removeAllComponents();
		mainApplicationWindow.addComponent(mProxy);

		// Create custom layout from "body.html" template.
		CustomLayout lCustomLayout = new CustomLayout("body");
		mainApplicationWindow.addComponent(lCustomLayout);

		Embedded lHomeButton = new Embedded(null, new ThemeResource("../tadtheme/images/home_48.png"));
		//Embedded lHomeButton = new Embedded(null, new ThemeResource("HOME"));
		lHomeButton.setDescription("Home");
		lCustomLayout.addComponent(lHomeButton, "homeButton");
		lHomeButton.addListener(new MouseEvents.ClickListener() {
			public void click(ClickEvent event) {
				showHomePage();
			}
		});

		Date toDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currdt = sdf.format(toDate);
		UserData u = mSession.getUser();
		String userName = u.getUserName();
		
		Label welcomeLabel = new Label("Welcome | "+ userName	+"| "+currdt);
		lCustomLayout.addComponent(welcomeLabel, "welcomeLabel");
		
		Embedded lLogoutButton = new Embedded(null, new ThemeResource("../tadtheme/images/log_out_48.png"));
		lLogoutButton.setDescription("Logout");
		lCustomLayout.addComponent(lLogoutButton, "logoutButton");
		lLogoutButton.addListener(new MouseEvents.ClickListener() {
			public void click(ClickEvent event) {
				logout();
			}
		});

		mContentAreaLayout = new VerticalLayout();
		float contentPanelHeight = mainApplicationWindow.getHeight() - 117;
		float contentPanelWidth = mainApplicationWindow.getWidth();
		LoggerExtension.log(1, "width:"+contentPanelHeight + "px"+" higth::"+contentPanelWidth + "px");
		mContentAreaLayout.setHeight(contentPanelHeight + "px");
		mContentAreaLayout.setWidth(contentPanelWidth + "px");
	//	mContentAreaLayout.setWidth("200px");
		lCustomLayout.addComponent(mContentAreaLayout, "contentArea");

		mContentAreaLayout.addComponent(homeView);
	}

//	@Override
	public void initHomePage() {
		homeView = new HomeView(this,mSession,osgiModuleList);
		
	}

	private void showHomePage() {
		renderHomePage();
	}

	private void logout() {
		close();
	}
	@Override
	public void close() {
		super.close();
	}

//	OsgiModule adminModule,OrderManagementModule,ProvisioningModule;
	AdminModule adminModule = new AdminModule();
	OrderManagementModule orderManagementModule = new OrderManagementModule();
	ProvisioningModule provisioningModule = new ProvisioningModule();
	ReportsModule reportsModule = new ReportsModule();
	ManageImsiModule imsiModule = new ManageImsiModule();
	List<OsgiModule> osgiModuleList = new ArrayList<OsgiModule>();
	@Override
	public List<OsgiModule> getModules() {
		osgiModuleList.add(adminModule);
		osgiModuleList.add(orderManagementModule);
		osgiModuleList.add(provisioningModule);
		osgiModuleList.add(reportsModule);
		osgiModuleList.add(imsiModule);
		return osgiModuleList;
	}

	
}

