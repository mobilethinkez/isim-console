/**
 * 
 */
package com.mtz.isim.application.module.customercare;

import java.io.Serializable;

import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.module.AbstractOsgiModule;
import com.vaadin.ui.Component;

/**
 * @author Satish.Yadav
 *
 */
public class ManageImsiModule extends AbstractOsgiModule implements Serializable {

	private static final String BUNDLE_SYMBOLIC_NAME = "com.zenithss.together.modules.admin";

	private static final long serialVersionUID = 132456754L;
	
	
	public String getName() {
		return "CustomerCare";
	}
	
	public String getDescription() {
		return "This module contains administration functionality.";
	}
	
	public String getBundleSymbolicName() {
		return BUNDLE_SYMBOLIC_NAME;
	}
	
	public String getImage() {
		return "/com/mtz/isim/application/module/customercare/customer-care.gif";
	}

	public Class<?> getImplementationClass() {
		return getClass();
	}

	public Component getView(SacSession session) {
		//SacSession newSession = new SacSession(session.getApplication(),applicationContext,session.getUser());
		return new ManageImsiModuleView(session);

	}

	public void deactivate() {
	}

	public boolean isVisible() {
		return true;
	}

	
}