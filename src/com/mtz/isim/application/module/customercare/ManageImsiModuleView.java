/**
 * 
 */
package com.mtz.isim.application.module.customercare;

import com.mobilethinkez.sim.application.tabs.ui.OrderList;
import com.mtz.isim.application.module.report.ViewInventoryPage;
import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.LoggerExtension;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.Tab;

/**
 * @author Satish.Yadav
 *
 */
public class ManageImsiModuleView  extends HorizontalSplitPanel implements Accordion.SelectedTabChangeListener{

	private static final long serialVersionUID = -7949452745004903078L;
	private SacSession mSession;
	//	private Logger logger = Logger.getLogger(getClass());
	VerticalLayout rightSide;	
	OrderList orderList;

	public static final String MANAGE_IMSI = "Manage IMSI";
	ManageImsiPage imsiMappingInfo;
	
	public ManageImsiModuleView(SacSession aSession) {
		mSession = aSession;
		setSplitPosition(230, UNITS_PIXELS);
		setLocked(false);
		setFirstComponent(getSearchAccordian());
		setSizeFull();
	}
	private Accordion getSearchAccordian() {
		Accordion a = new Accordion();
        a = new Accordion();
        a.setHeight("300px");
        a.setWidth("450px");
        addManageIMSI(a);
        showManageIMSI();
        a.addListener(this);
		return a;
	}
	private void addManageIMSI(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, MANAGE_IMSI);
		a.setSizeFull();
	}
	private void showManageIMSI() {
		LoggerExtension.log(1, "inside the showExportProvisioningView view..");
		rightSide = new VerticalLayout();	
		rightSide.setSizeFull();
		rightSide.addComponent(getIMSIManageRightPage());
		setSecondComponent(rightSide);
 	}
	private ManageImsiPage getIMSIManageRightPage() {
		imsiMappingInfo = new ManageImsiPage();
		return imsiMappingInfo;
	}
	
	@Override
	public void selectedTabChange(SelectedTabChangeEvent event) {

        TabSheet tabsheet = event.getTabSheet();
        Tab tab = tabsheet.getTab(tabsheet.getSelectedTab());
        if (tab != null) {
          if(tab.getCaption().equalsIgnoreCase(MANAGE_IMSI)) {
       		 showManageIMSI();
       	}
        	setMargin(true);
        }
	
		
	}
	

}

