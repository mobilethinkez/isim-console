package com.mtz.isim.application.module.customercare;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.vo.ImsiMappingInfo;
import com.mtz.isim.application.vo.SimInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ManageImsiPage  extends VerticalLayout {
	//CustomerInfo customerInfo;
	//private static final String COMMON_FIELD_WIDTH = "12em";
	Panel imsiDetailPanel ;
	Button save;
	//Button cancel;
	//Button edit;
	private Panel panel;
	//private NativeSelect customerIDDropDown;
	TextField msisdnTF;
	ImsiMappingInfo imsiMappingInfo;
	SimInfo simInfo;
	String newImsi;
	public ManageImsiPage(){
		panel = new Panel();
		panel.setCaption("IMSI Mapping Module");
		panel.setWidth("500px");
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true, true, true, true);
		panel.setContent(formLayout);
		msisdnTF = new TextField("Enter MSISDN");
		msisdnTF.setRequired(true);
		msisdnTF.setNullRepresentation("[MSISDN]");
		imsiMappingInfo = new ImsiMappingInfo();
		Button mappingDetailsButton = new Button("Get Mapping Details", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				String enterMsisdnValue = msisdnTF.getValue().toString();
				if(enterMsisdnValue.equalsIgnoreCase("")){
					getWindow().showNotification("Please Enter MSISDN.");
					return;
				}else{
					if(getMSISDNInfo(enterMsisdnValue).equalsIgnoreCase("failed")){
						getWindow().showNotification("MSISDN Not Exist in System.");
						return;
					}else{
						
						if(imsiDetailPanel!=null){
							removeComponent(imsiDetailPanel);
						}
						displayCustomerDetails();
					}
//					if(newImsi==null||newImsi.equalsIgnoreCase("")){
//						getWindow().showNotification("No Free IMSI Available");
//						return;
//					}
				}
				
				
			}
		});
		panel.addComponent(msisdnTF);
		panel.addComponent(mappingDetailsButton);
		addComponent(panel);
	}
	protected String getMSISDNInfo(String msisdn) {
		imsiMappingInfo =	DBManager.getMSISDNDetails(msisdn);
		LoggerExtension.log(1,"getMSISDNInfo::"+ imsiMappingInfo.toString());
	//	newImsi = DBManager.getNewImsiDetails(imsiMappingInfo.getCard_type());
	//	LoggerExtension.log(1, "newImsi::"+newImsi);
		String result= "success" ;
		
		if( imsiMappingInfo.getMsisdn().equalsIgnoreCase("")){
			result ="failed";
		}
		return result;
		
		
	}
	private void displayCustomerDetails() {
		
    	imsiDetailPanel = new Panel();
		imsiDetailPanel.setCaption("MSISDN Information");
		imsiDetailPanel.setWidth("500px");
		imsiDetailPanel.setHeight("380px");
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true, true, true, true);
		imsiDetailPanel.setContent(formLayout);
		
		TextField oldIMSI = new TextField("Old IMSI");
		oldIMSI.setValue(imsiMappingInfo.getImsi());
		oldIMSI.setReadOnly(true);
		final TextField newIMSI = new TextField("New IMSI");
		
		//newIMSI.setValue(newImsi);
		//newIMSI.setReadOnly(true);
		addComponent(imsiDetailPanel);
		HorizontalLayout buttonsHLayout = new HorizontalLayout();
		buttonsHLayout.setSpacing(true);
		save = new Button("Create New Assignment", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				try {
					
					simInfo = DBManager.getIMSIDetails(newIMSI.getValue().toString());
					if(simInfo.getStatus().equalsIgnoreCase("")){
						getWindow().showNotification("NEW IMSI is Not Available.");
					}else{
						// update the sim info table
						mappedNewImsiToMsisdn(imsiMappingInfo,newIMSI.getValue().toString());
						getWindow().showNotification("NEW IMSI Associated Successfully.");
					}
					} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	
		imsiDetailPanel.addComponent(oldIMSI);
		imsiDetailPanel.addComponent(newIMSI);
		imsiDetailPanel.addComponent(save);
		addComponent(imsiDetailPanel);
	
	}
	protected void mappedNewImsiToMsisdn(ImsiMappingInfo imsiMappingInfo2, String newIMSI2) {
		
		DBManager.mappedNewImsiToMSISDN(imsiMappingInfo2,newIMSI2);
		
		DBManager.updateOldImsiRecord(imsiMappingInfo2);
		// TODO Auto-generated method stub
		
	}
	
	
}
