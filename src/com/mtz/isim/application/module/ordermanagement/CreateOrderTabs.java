/**
 * 
 */
package com.mtz.isim.application.module.ordermanagement;

import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.vo.CustomerInfo;
import com.mtz.isim.application.vo.OrderInfo;
import com.mtz.isim.application.vo.VendorInfo;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;

/**
 * @author Satish.Yadav
 *
 */
public class CreateOrderTabs extends VerticalLayout implements TabSheet.SelectedTabChangeListener{

	private TabSheet tabSheet;
	private VerticalLayout l1;
	private VerticalLayout l2;
	private VerticalLayout l3;
	@SuppressWarnings("unused")
	public Tab t1, t2, t3;

	CustomerInfoForm customerInfoForm;
	VendorDetailsForm vendorDetailsForm;
	OrderInfoForm orderInfoForm;
	ViewOrderTab viewOrderTab;
	CustomerInfo customerInfo;
	VendorInfo vendorInfo;
	OrderInfo orderInfo;
	
	public CreateOrderTabs(){
		LoggerExtension.log(1, "CreateOrderTabs classs.......");
		setSpacing(true);
//		if(customerInfoForm==null){
			customerInfo = new CustomerInfo();
			customerInfoForm =  new CustomerInfoForm(customerInfo);
//		if(vendorDetailsForm==null){
//		}
	//	if(orderInfoForm==null){
//	}

		// Tab 1 content
		l1 = new VerticalLayout();
		l1.setSizeFull();
		l1.setMargin(true);
		l1.addComponent(customerInfoForm);

		// Tab 2 content
		l2 = new VerticalLayout();
		l2.setSizeFull();

		l2.setMargin(true);
	//	l2.addComponent(vendorDetailsForm);
		// Tab 3 content
		l3 = new VerticalLayout();
		l3.setSizeFull();
		l3.setMargin(true);
	//	l3.addComponent(orderInfoForm);

		tabSheet = new TabSheet();
		//tabSheet.setHeight("400px");
		//  tabSheet.setWidth("400px");

		t1 = tabSheet.addTab(l1,"Customer Details");
		t2 = tabSheet.addTab(l2,"Vendor Details");
		t3 = tabSheet.addTab(l3,"Order Details");

//		t1.setEnabled(true);
//		t2.setEnabled(false);
//		t3.setEnabled(false);
	
		tabSheet.addListener(this);

		addComponent(tabSheet);
	}

	@Override
	public void selectedTabChange(SelectedTabChangeEvent event) {
		// TODO Auto-generated method stub
		 
		String tabName = tabSheet.getTab(event.getTabSheet().getSelectedTab()).getCaption();
		
		getWindow().showNotification("Selected tab: " + tabName);
	
		if(tabName.equalsIgnoreCase("Customer Details")){
			
		}else if(tabName.equalsIgnoreCase("Vendor Details")){
			vendorInfo = new VendorInfo();
			if(vendorDetailsForm!=null){
			
			}else{
				vendorDetailsForm =  new VendorDetailsForm(vendorInfo);
			}
			LoggerExtension.log(1,"createOrderTabs Class: !after vendorDetailsForm so Vendor..........................");
			l2.addComponent(vendorDetailsForm);

		}else if(tabName.equalsIgnoreCase("Order Details")){
			orderInfo = new OrderInfo();
			if(orderInfoForm==null){
				
			}else{
				l3.removeComponent(orderInfoForm);
			}
			LoggerExtension.log(1,"createOrderTabs Class:: customerInfo from customerTab::"+customerInfoForm.getCustomerInfo());
			if(vendorDetailsForm==null){
				LoggerExtension.log(1,"createOrderTabs Class:: vendorDetailsForm is null...");
				orderInfoForm =  new OrderInfoForm(orderInfo,customerInfoForm.getCustomerInfo(),vendorInfo);
				l3.addComponent(orderInfoForm);
				getWindow().showNotification("Please Select Vendor Details First.");
			}else{
				LoggerExtension.log(1,"createOrderTabs Class:: vendorInfo from vendorTab::"+vendorDetailsForm.getVendorInfo());	
				orderInfoForm =  new OrderInfoForm(orderInfo,customerInfoForm.getCustomerInfo(),vendorDetailsForm.getVendorInfo());
				l3.addComponent(orderInfoForm);
			}
			
		}

	}


}
