/**
 * 
 */
package com.mtz.isim.application.module.ordermanagement;

import java.util.Arrays;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.vo.CustomerInfo;
import com.mtz.isim.application.vo.VendorListInfo;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

/**
 * @author Satish.Yadav
 *
 */
public class CustomerInfoForm extends VerticalLayout  implements Property.ValueChangeListener{

	CustomerInfo customerInfo;
	private static final String COMMON_FIELD_WIDTH = "12em";
	Form customerForm ;
	Button save;
	Button cancel;
	Button edit;
//	final Button next;
//	final ComboBox customerId = new ComboBox("Customer Id");
	//final Form customerIDForm ;
//	public static final Alignment ALIGNMENT_DEFAULT = Alignment.MIDDLE_RIGHT;
	
	private Panel panel;
	private NativeSelect customerIDDropDown;
	TextField customerIDTextField;

	public CustomerInfoForm(CustomerInfo customerInfo1){
		this.customerInfo = customerInfo1;
		
		panel = new Panel();
		panel.setCaption("Customer Details");
		panel.setWidth("500px");
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true, true, true, true);
		panel.setContent(formLayout);

		//Order ID, IMSI PREFIX, SIM PREFIX, IMSI SERIAL NO, SIM
		customerIDDropDown = new NativeSelect("Select Customer Name");
		customerIDDropDown.addListener(this);
		customerIDDropDown.setImmediate(true);
		customerIDDropDown.setWidth(COMMON_FIELD_WIDTH);
		customerIDDropDown.setNullSelectionAllowed(false);
		List<CustomerInfo> customerInfosList = DBManager.getCustomerList();
		for (int i = 0; i < customerInfosList.size(); i++) {
			CustomerInfo v = customerInfosList.get(i);
			customerIDDropDown.addItem(v.getCustomerId());
		}
		Button addCustomer = new Button("New Customer Click Here", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				if(customerForm!=null){
					removeComponent(customerForm);
					customerInfo=new CustomerInfo();
				}
				   
					displayCustomerDetails(customerInfo);
			}

			
		});
		panel.setImmediate(true);
		panel.addComponent(customerIDDropDown);
		panel.addComponent(addCustomer);
		addComponent(panel);
	}
	private void disableUpdateButton() {
		edit.setEnabled(false);
		
	}
	private void enableUpdateButton() {
		edit.setEnabled(true);
		
	}
	public CustomerInfoForm() {
		// TODO Auto-generated constructor stub
	}

	private void displayCustomerDetails(CustomerInfo customerInfo2) {
		BeanItem<CustomerInfo> beanItem = new BeanItem<CustomerInfo>(customerInfo2);

		customerForm = new Form();
		customerForm.setCaption("Customer Information");
		customerForm.setWidth("500px");
		customerForm.setHeight("380px");
		customerForm.setWriteThrough(false);
		customerForm.setImmediate(true);
		customerForm.setFormFieldFactory(new CustomerFormFieldFactory());
		customerForm.setItemDataSource(beanItem);
		customerForm.setVisibleItemProperties(Arrays.asList(new String[]{
				"customerId","name","address","city","state","country","pinCode","email","phoneNumber"
		}));

		addComponent(customerForm);
		HorizontalLayout buttonsHLayout = new HorizontalLayout();
		buttonsHLayout.setSpacing(true);
		save = new Button("SAVE", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				try {
					if(source == save){
						customerForm.commit();
						
						ConfirmDialog.show(getWindow(), "Please Confirm:", "Are you really sure to save the customer information?", "Ok", "Cancel", new ConfirmDialog.Listener() {
							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to continue
									feedback(dialog.isConfirmed());
								} else {
									// User did not confirm
									feedback(dialog.isConfirmed());
								}
							}

							private void feedback(boolean confirmed) {
								// TODO Auto-generated method stub
								if(confirmed){
									LoggerExtension.log(1, "customerInfoForm::saveCustomerData customerInfo::"+customerInfo);
									String result = saveCustomerData();
									if(result.equalsIgnoreCase("success")){
										getWindow().showNotification("Customer Information Saved Successfully.Please Select Vendor Info Tab for countinue.",
												Notification.TYPE_HUMANIZED_MESSAGE);	
										save.setEnabled(false);
										cancel.setEnabled(false);
										customerForm.setEnabled(false);
										
									}else{

										getWindow().showNotification(
												"Customer Id is already exist in system. Please select customer id from drop down and update the details.",
												Notification.TYPE_ERROR_MESSAGE);	
									}
									
									
								}else{
									LoggerExtension.log(1, "customerInfoForm::updateCustomerData ConfirmDialog response is no!!!!!!!");
									//getWindow().showNotification("Next time.....");	
								}
							}
						});
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		edit = new Button("UPDATE", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				try {
					if(source == edit){
						/* If the given input is not valid there is no point in continuing */
						LoggerExtension.log(1, "customerInfoForm::updateCustomerData customerInfo::"+customerInfo);
						// Using custom captions.
						ConfirmDialog.show(getWindow(), "Please Confirm:", "Are you really sure to update the customer information?", "Ok", "Cancel", new ConfirmDialog.Listener() {
							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to continue
									feedback(dialog.isConfirmed());
								} else {
									// User did not confirm
									feedback(dialog.isConfirmed());
								}
							}

							private void feedback(boolean confirmed) {
								// TODO Auto-generated method stub
								if(confirmed){
									customerForm.commit();
									updateCustomerData();
									getWindow().showNotification("Customer Updation Done Successfully..");
								}else{
									LoggerExtension.log(1, "customerInfoForm::updateCustomerData ConfirmDialog response is no!!!!!!!");
									//getWindow().showNotification("Next time.....");	
								}
							}
						});
					
					}
					} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		cancel = new Button("CANCEL", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				if(source == cancel)
				{
					LoggerExtension.log(1, "customerInfoForm::cancel button clicked");
					customerForm.discard();	
				}

			}
		});
		
		 disableUpdateButton();
		
		buttonsHLayout.addComponent(save);
		buttonsHLayout.addComponent(edit);
		buttonsHLayout.addComponent(cancel);
		// cancel.setStyleName(BaseTheme.BUTTON_LINK);
		buttonsHLayout.setComponentAlignment(cancel, Alignment.MIDDLE_LEFT);

		customerForm.getFooter().addComponent(buttonsHLayout);
		customerForm.getFooter().setMargin(false, false, true, true);
	}
	
	protected void createCustomerID() {

		String custID = customerInfo.getName();
		customerInfo.setCustomerId(custID);
		LoggerExtension.log(1, "createCustomerID() customerID:"+custID);
	}

	protected String saveCustomerData() {
		// TODO Auto-generated method stub
		//customerInfo.setCustomerId(customerInfo.getName());
		return DBManager.saveCustomerInfo(customerInfo);
	
	}
	
	protected void updateCustomerData() {
		// TODO Auto-generated method stub
		//customerInfo.setCustomerId(customerInfo.getName());
		DBManager.updateCustomerInfo(customerInfo);
	
	}
	private class CustomerFormFieldFactory extends DefaultFieldFactory{
		
//		public CustomerFormFieldFactory(String action){
//			if(action.equalsIgnoreCase("update")){
//				
//			}
//		}
		
		@Override
		public Field createField(Item item, Object propertyId, Component uiContext) {
			Field f;
			f = super.createField(item, propertyId, uiContext);
			if ("customerId".equals(propertyId)) {
				customerIDTextField = (TextField) f;
				customerIDTextField.setRequired(true);
				customerIDTextField.setNullSettingAllowed(false);
				customerIDTextField.setRequiredError("Please enter a Customer Id");
				customerIDTextField.setWidth(COMMON_FIELD_WIDTH);
				customerIDTextField.addValidator(new StringLengthValidator("Customer Id must be 3-45 characters", 3, 45, false));

			}else if ("name".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a First Name");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("First Name must be 3-150 characters", 3, 150, false));

			}else if ("country".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Country");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("Country must be 3-45 characters", 3, 45, false));
			} else if ("address".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Address");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("Address must be 3-150 characters", 3, 150, false));
			} else if ("state".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a State");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("State must be 3-45 characters", 3, 45, false));
			} else if ("city".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a City");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("City must be 3-45 characters", 3, 45, false));
			} else if ("email".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Email");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new EmailValidator("Email must be in this format abc@gmail.com"));
			}else if ("pinCode".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setNullRepresentation("");
				tf.setRequired(true);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setRequiredError("Please enter a PostalCode");
				tf.addValidator(new StringLengthValidator("PostalCode must be an Alpanumeric"));
			} else if ("phoneNumber".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Contact Number");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setNullRepresentation("");
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Contact number");
				tf.addValidator(new StringLengthValidator("Contact number must be an Alpanumeric",10,14,false));
			}

			return f;
		}

	}
	/*
	private class CustomerIDFieldFactory extends DefaultFieldFactory{
		public CustomerIDFieldFactory() {
			customerId.setWidth(COMMON_FIELD_WIDTH);
			customerId.setCaption("Customer ID");
			customerId.setNullSelectionAllowed(false);
			List<CustomerInfo> customerInfosList = DBManager.getCustomerList();
			for (int i = 0; i < customerInfosList.size(); i++) {
				CustomerInfo v = customerInfosList.get(i);
				customerId.addItem(v.getCustomerId());
			}
			customerId.setFilteringMode(ComboBox.FILTERINGMODE_STARTSWITH);
		}

		@Override
		public Field createField(Item item, Object propertyId, Component uiContext) {
			Field f;
			if ("customerId".equals(propertyId)) {
				// filtering ComboBox w/ country names
				return customerId;
			} else {
				// Use the super class to create a suitable field base on the
				// property type.
				f = super.createField(item, propertyId, uiContext);
				f.setVisible(false);

			}
			return f;

	}
	}
*/	
	
	@Override
	public void valueChange(ValueChangeEvent event) {
		Property property = event.getProperty();
	
		if (property == customerIDDropDown) {
			//	disableOtherFields();
				String custID = customerIDDropDown.getValue().toString();
				LoggerExtension.log(1, "selected customerIDDropDown id...."+custID);
				// get data from table...
				customerInfo = DBManager.getCustomerInfo(custID);
				if(customerForm!=null){
					removeComponent(customerForm);
				}
				displayCustomerDetails(customerInfo);
				enableUpdateButton();
				disableSaveCancelFields();
			}
	
	}

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}

	public void disableSaveCancelFields() {
		customerIDTextField.setReadOnly(true);
		save.setEnabled(false);
		cancel.setEnabled(false);
	}
	
}
