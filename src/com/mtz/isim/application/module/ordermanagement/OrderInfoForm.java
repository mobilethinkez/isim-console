package com.mtz.isim.application.module.ordermanagement;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.util.RandomGenerator;
import com.mtz.isim.application.util.SACUtility;
import com.mtz.isim.application.vo.CustomerInfo;
import com.mtz.isim.application.vo.OrderInfo;
import com.mtz.isim.application.vo.VendorInfo;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;
public class OrderInfoForm extends VerticalLayout{


	//OrderInfo orderInfo;
	private static final String COMMON_FIELD_WIDTH = "12em";
	final Form orderForm ;
	final Button save;
	final Button cancel;
	String successMsg = "Order saved with Order <order_no> To submit order for processing click here";
	Window subwindow;
	Label message;
	String confilctOrderNo;
	String finalFilePath;
	public OrderInfoForm(final OrderInfo orderInfo,final CustomerInfo customerInfo, final VendorInfo vendorInfo){
		BeanItem<OrderInfo> beanItem = new BeanItem<OrderInfo>(orderInfo);
		orderForm = new FormWithComplexLayout(beanItem);
		orderForm.setImmediate(true);
		
		
		addComponent(orderForm);

		// The cancel / apply buttons
		HorizontalLayout buttons = new HorizontalLayout();
		buttons.setSpacing(true);
	
		// cancel.setStyleName(BaseTheme.BUTTON_LINK);

		// save = new Button("SAVE");
		save = new Button("SAVE", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				try {
					if(source == save){
						/* If the given input is not valid there is no point in continuing */
					
						LoggerExtension.log(1, "OrderInfoForm class::feedback....");
						orderForm.commit();
						
						if(checkIMSIValidation(orderInfo)){
							getWindow().showNotification("SAC-121 : Order Submit Failed - Due to IMSI range conflict with Order No:"+confilctOrderNo,Notification.TYPE_ERROR_MESSAGE);
							return;
						}
						// save the data in database 
						
						orderInfo.setOrderId(createOrderId(orderInfo));
						orderInfo.setCustomerId(getCustomerIdFromDb(customerInfo));
						orderInfo.setVendorId(getVendorIdFromDb(vendorInfo));
						String result = DBManager.saveOrderInfo(orderInfo);
						LoggerExtension.log(1, "result:"+result);
						if(result.equalsIgnoreCase("failed")){
							getWindow().showNotification("SAC-121 : Order Submit Failed - Due to IMSI range conflict",Notification.TYPE_ERROR_MESSAGE);
						}
						// confirm dialog to create the sim order request file.
						ConfirmDialog.show(getWindow(), "Please Confirm:", "Order saved with Order No. "+orderInfo.getOrderId()+" To submit order for processing click Ok", "Ok", "Cancel", new ConfirmDialog.Listener() {
							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to continue
									feedback(dialog.isConfirmed());
								} else {
									// User did not confirm
									feedback(dialog.isConfirmed());
								}
							}

							private void feedback(boolean confirmed) {
								// TODO Auto-generated method stub
								if(confirmed){
									LoggerExtension.log(1, "OrderInfoForm class::confirmed.");
									createrRequestFile(orderInfo,customerInfo,vendorInfo);
									
									//set  status........ status : order_submitted
									DBManager.updateOrderStatus(orderInfo.getOrderId());
									getWindow().showNotification("Request File For Order No."+orderInfo.getOrderId()+" is created successfully.");	
									orderForm.setEnabled(true);
									
									{
										FileResource resource;
										//Initiate the file resource to the newly-generated file at its generated location of "src/main/webapp/tmp/generatedfile.filetype"
										resource = new FileResource(new File( finalFilePath), getApplication());
										//Open the generated file for download
										getWindow().open(resource);
									}
								}else{
									LoggerExtension.log(1, "OrderInfoForm class::Not Confirmed.");
									getWindow().showNotification("You are not submitting the Order No."+orderInfo.getOrderId());	
								}
							}

							
						});
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
		buttons.addComponent(save);
		cancel = new Button("Cancel", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				if(source == cancel)
				{
					orderForm.discard();
				}

			}
		});
		
	
		// Create the window...
        subwindow = new Window("Last 5 Defined IMSI Ranges");
        // ...and make it modal
        subwindow.setModal(true);

        // Configure the windws layout; by default a VerticalLayout
        VerticalLayout layout = (VerticalLayout) subwindow.getContent();
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.setWidth("500px");
        layout.setHeight("300px");
        VerticalLayout vv= new VerticalLayout();
        Label messageHeader = new Label("ORDER_NO|START_IMSI_RANGE|END_IMSI_RANGE|CREATED_DATE");
        vv.addComponent(messageHeader);
        List<OrderInfo> list =	DBManager.getIMSIRangeFrom_OrderTable();
        
		for (int i = 0; i < list.size(); i++) {
			OrderInfo orderInfoPop = list.get(i);
			message = new Label();
			message.setStyleName("popMessage");
			message.setCaption(orderInfoPop.getOrderId()+"  | "+ orderInfoPop.getStart_imsi_range()+"  | "+orderInfoPop.getEnd_imsi_range()+" | "+orderInfoPop.getCreationDate());
			vv.addComponent(message);
			
		}
        // Add some content; a label and a close-button
       
        subwindow.addComponent(vv);

        Button close = new Button("Close", new Button.ClickListener() {
            // inline click-listener
            public void buttonClick(ClickEvent event) {
                // close the window by removing it from the parent window
                (subwindow.getParent()).removeWindow(subwindow);
            }
        });
        // The components added to the window are actually added to the window's
        // layout; you can use either. Alignments are set using the layout
        layout.addComponent(close);
        layout.setComponentAlignment(close, Alignment.TOP_RIGHT);

        // Add a button for opening the subwindow
       
        Button open = new Button("Recently Defined IMSI Ranges",
                new Button.ClickListener() {
                    // inline click-listener
                    public void buttonClick(ClickEvent event) {
                        if (subwindow.getParent() != null) {
                            // window is already showing
                            getWindow().showNotification(
                                    "Window is already open");
                        } else {
                            // Open the subwindow by adding it to the parent
                            // window
                            getWindow().addWindow(subwindow);
                        }
                    }
                });
        addComponent(open);
	
	

		
		buttons.addComponent(cancel);
		buttons.addComponent(open);
		buttons.setComponentAlignment(cancel, Alignment.MIDDLE_LEFT);
		orderForm.getFooter().addComponent(buttons);
		orderForm.getFooter().setMargin(true, true, true, true);

	}

	protected boolean checkIMSIValidation(OrderInfo orderInfo) {
		boolean flag;
		OrderInfo orderInfoBean = DBManager.getIMSIValidationInfo(orderInfo.getImsi_prefix().toString(),orderInfo.getStart_imsi().toString());
		if(orderInfoBean==null){
			flag=false;
		LoggerExtension.log(1, "orderinfo bean is null..validate success....");	
		}else{
			LoggerExtension.log(1, "orderinfo bean is not null..validate failed....");
			flag =true;
			confilctOrderNo= orderInfoBean.getOrderId();
		}
		return flag;
	}

	protected String getCustomerIdFromDb(CustomerInfo customerInfo) {
		String custId = customerInfo.getCustomerId();
		LoggerExtension.log(1, "OrderInfoForm class::getCustomerIdFromDb customerId:"+custId);
		return custId;
	}
	protected String getVendorIdFromDb(	VendorInfo vendorInfo) {
		String vendorId = vendorInfo.getVendorId();
		LoggerExtension.log(1, "OrderInfoForm class::getVendorIdFromDb vendorId:"+vendorId);
		return vendorId;
	}
	protected void sendMailToVendor(OrderInfo orderInfo2) {


	}
	private void createrRequestFile(OrderInfo orderInfo, CustomerInfo customerInfo, VendorInfo vendorInfo) {
		LoggerExtension.log(1, "OrderInfoForm class::Write Request File for Vendor...");
		finalFilePath = SACUtility.createRequestFile(orderInfo,customerInfo,vendorInfo);
	}

	private String createOrderId(OrderInfo orderInfo2) {
		String orderId ="";
		RandomGenerator randomGenerator = new RandomGenerator();
		orderId = randomGenerator.generateRandomNumber().toString();
		LoggerExtension.log(1, "OrderInfoForm class::OrderID:"+orderId);
		return orderId;
	}


	public class FormWithComplexLayout extends Form {

		private GridLayout ourLayout;

		public FormWithComplexLayout(BeanItem<OrderInfo> orderInfoBean) {
			setCaption("Order details");

			// Create our layout (3x3 GridLayout)
			ourLayout = new GridLayout(4, 8);
			ourLayout.setWidth("100%");
			// Use top-left margin and spacing
			ourLayout.setMargin(true, false, false, true);
			ourLayout.setSpacing(true);
			ourLayout.addStyleName("basic-gridlayout");

			setLayout(ourLayout);

			// Set up buffering
			setWriteThrough(false); // we want explicit 'apply'
			setInvalidCommitted(false); // no invalid values in datamodel
			setValidationVisibleOnCommit(true);
			// FieldFactory for customizing the fields and adding validators
			setFormFieldFactory(new OrderInfoFieldFactory());
			setItemDataSource(orderInfoBean); // bind to POJO via BeanItem

			// Determines which properties are shown, and in which order:
			setVisibleItemProperties(Arrays.asList(new String[] { "quantity","sim_prefix", "imsi_prefix", "start_sim", "start_imsi","sim_card_format",
					"transport_key","electrical_profile","graphical_profile","carrier_name","network_element","brand","order_type","card_type","expectedDeliveryDate" }));
			setImmediate(true);
		}

		/*
		 * Override to get control over where fields are placed.
		 */
		@Override
		protected void attachField(Object propertyId, Field field) {
			if (propertyId.equals("quantity")) {
				ourLayout.addComponent(field, 0, 0,3,0);
			} else if (propertyId.equals("sim_prefix")) {
				ourLayout.addComponent(field, 0, 1,1,1);
			} else if (propertyId.equals("start_sim")) {
				ourLayout.addComponent(field, 2, 1,3,1);
			} else if (propertyId.equals("imsi_prefix")) {
				ourLayout.addComponent(field, 0, 2,1,2);
			} else if (propertyId.equals("start_imsi")) {
				ourLayout.addComponent(field, 2, 2,3,2);
			} else if (propertyId.equals("sim_card_format")) {
				ourLayout.addComponent(field, 0, 3,1,3);
			}else if (propertyId.equals("brand")) {
				ourLayout.addComponent(field, 2, 3,3,3);
			}else if (propertyId.equals("transport_key")) {
				ourLayout.addComponent(field, 0, 4,1,4);
			}else if (propertyId.equals("order_type")) {
				ourLayout.addComponent(field, 2,4,3,4);
			}
			else if (propertyId.equals("electrical_profile")) {
				ourLayout.addComponent(field, 0, 5,1,5);
			}else if (propertyId.equals("graphical_profile")) {
				ourLayout.addComponent(field, 2, 5,3,5);
			}else if (propertyId.equals("carrier_name")) {
				ourLayout.addComponent(field, 0, 6,1,6);
			}else if (propertyId.equals("network_element")) {
				ourLayout.addComponent(field, 2, 6,3,6);
			}
			else if (propertyId.equals("card_type")) {
				ourLayout.addComponent(field, 0, 7,1,7);
			}
			else if (propertyId.equals("expectedDeliveryDate")) {
				ourLayout.addComponent(field, 2, 7,3,7);
			}

		}

	}


	private class OrderInfoFieldFactory extends DefaultFieldFactory {
		final ComboBox sim_card_format = new ComboBox("SIM card plugin");
		final ComboBox brand = new ComboBox("Brand");
		final ComboBox network_element = new ComboBox("Network element");
		final ComboBox card_type = new ComboBox("Card Type");
		final ComboBox order_type = new ComboBox("Order Type");
	
		public OrderInfoFieldFactory() {
			card_type.setWidth(COMMON_FIELD_WIDTH);
			card_type.addItem("32k");
			card_type.addItem("64k");
			card_type.setRequired(true);
			card_type.setNullSelectionAllowed(false);
			card_type.setDescription("Choose SIM Card specification");
			card_type.addValidator(new StringLengthValidator("Choose SIM Card specification"));
			card_type.setFilteringMode(ComboBox.FILTERINGMODE_STARTSWITH);

			order_type.setWidth(COMMON_FIELD_WIDTH);
			order_type.addItem("Prepaid");
			order_type.addItem("Postpaid");
			order_type.setNullSelectionAllowed(false);
			order_type.setRequired(true);
			order_type.setDescription("Choose type of order Prepaid / Post Paid");
			order_type.addValidator(new StringLengthValidator("Choose type of order Prepaid / Post Paid"));
			order_type.setFilteringMode(ComboBox.FILTERINGMODE_STARTSWITH);

			
			sim_card_format.setWidth(COMMON_FIELD_WIDTH);
			sim_card_format.addItem("Plug-in");
			sim_card_format.setRequired(false);
			sim_card_format.setNullSelectionAllowed(false);
			sim_card_format.setDescription("Select Plugin supported by SIM Card");
			sim_card_format.setFilteringMode(ComboBox.FILTERINGMODE_STARTSWITH);

			brand.setWidth(COMMON_FIELD_WIDTH);
			brand.addItem("Brand-A");
			brand.addItem("Brand-B");
			brand.addItem("Brand-C");
			brand.setNullSelectionAllowed(false);
			brand.setRequired(false);
			brand.setDescription("Select Brand Options");
			brand.setFilteringMode(ComboBox.FILTERINGMODE_STARTSWITH);

			network_element.setWidth(COMMON_FIELD_WIDTH);
			network_element.addItem("Portal");
			network_element.setRequired(false);
			network_element.setNullSelectionAllowed(false);
			network_element.setDescription("Select Network Element");
			network_element.setFilteringMode(ComboBox.FILTERINGMODE_STARTSWITH);

		}
		@Override
		public Field createField(Item item, Object propertyId, Component uiContext) {
			Field f;
			if("sim_card_format".equals(propertyId)){
				return sim_card_format;
			}else if("brand".equals(propertyId)){
				return brand;
			}else if("network_element".equals(propertyId)){
				return network_element;
			}else if("order_type".equals(propertyId)){
				return order_type;
			}else if("card_type".equals(propertyId)){
				return card_type;
			}
			else{
				f = super.createField(item, propertyId, uiContext);
			}	
			if ("quantity".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Quantity");
				tf.setRequired(true);
				//tf.setValue("1000");
				//tf.setInputPrompt("1000");
				tf.setRequiredError("Please Enter Qunatity of SIM Cards to be Ordered");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new IntegerValidator("Quantity must be a number[minimum-3 digits]"));
				tf.addValidator(new StringLengthValidator("Quantity must be a number[minimum-3 digits]", 3, 25, false));
			} else if ("sim_prefix".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("SIM prefix");
				tf.setRequired(true);
				//tf.setValue("890139");
				tf.setRequiredError("Please enter a SIM prefix");
				//tf.setInputPrompt("890139");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("SIM prefix must be a number[Exactly 6 digits]", 6, 6, false));
			} else if ("imsi_prefix".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("IMSI prefix");
				tf.setRequired(true);
			//	tf.setValue("364390");
			//	tf.setInputPrompt("364390");
				tf.setRequiredError("Please enter a IMSI prefix");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("IMSI prefix must be a number[Exactly 6 digits]", 6, 6, false));
			}else if ("transport_key".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Transport key");
				tf.setRequired(false);//optional
				tf.setValue("");
				tf.setDescription("This key allows to perform operations on the SIM Card");
				tf.setRequiredError("Please enter a Transport key");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("Transport key must be a number[minimum-2 digits]", 2, 10, false));
			}else if ("electrical_profile".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Electrical profile");
				tf.setRequired(false);//optional
				tf.setNullRepresentation("");
				tf.setValue("");
				tf.setNullSettingAllowed(true);
				tf.setRequiredError("Enter SIM Electonic Profile structure that needs to be followed");
				tf.addValidator(new StringLengthValidator("Electrical profile must be a number",1,12,false));
			} else if ("carrier_name".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Carrier name");
				tf.setNullRepresentation("");
				tf.setValue("BTC");
				tf.setNullSettingAllowed(true);
				tf.setRequiredError("Please enter a Carrier name");
				tf.addValidator(new StringLengthValidator("Carrier name must be character[3-32]",3,32,false));
			}
			else if ("start_imsi".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Start IMSI Range");
				tf.setRequired(true);
				tf.setNullRepresentation("");
				tf.setValue("");
				tf.setNullSettingAllowed(true);
				tf.setDescription("Enter 9 digits following IMSI  Prefix");
				tf.setRequiredError("Enter 9 digits following IMSI  Prefix");
				tf.addValidator(new StringLengthValidator("Start IMSI must be number[Exactly 9 digits]", 9, 9, false));
				}
			else if ("start_sim".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Start SIM Range");
				tf.setRequired(true);
				tf.setNullRepresentation("");
				tf.setValue("120");
				tf.setInputPrompt("120");
				tf.setNullSettingAllowed(true);
				tf.setDescription("Enter 3 digits following SIM Prefix");
				tf.setRequiredError("Enter 3 digits following SIM Prefix");
				tf.addValidator(new StringLengthValidator("Start SIM must be number[Exactly 3 digits]", 3, 3, false));
				}
			else if ("graphical_profile".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Graphical profile");
				tf.setValue("");
				tf.setNullRepresentation("");
				tf.setNullSettingAllowed(true);
				tf.setRequiredError("Please enter a Graphical profile");
				tf.addValidator(new StringLengthValidator("Graphical profile must be number[1-12]",1,12,false));
			}
			return f;
		}


	}


}
