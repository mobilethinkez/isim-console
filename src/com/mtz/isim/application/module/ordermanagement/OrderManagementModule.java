package com.mtz.isim.application.module.ordermanagement;

import java.io.Serializable;

import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.module.AbstractOsgiModule;
import com.vaadin.ui.Component;

public class OrderManagementModule extends AbstractOsgiModule implements Serializable {

	private static final String BUNDLE_SYMBOLIC_NAME = "com.zenithss.together.modules.admin";

	private static final long serialVersionUID = 132456754L;
	
	
	public String getName() {
		return "OrderManagement";
	}
	
	public String getDescription() {
		return "This module contains sim order functionality.";
	}
	
	public String getBundleSymbolicName() {
		return BUNDLE_SYMBOLIC_NAME;
	}
	
	public String getImage() {
		return "/com/mtz/isim/application/module/ordermanagement/create_order-icon.png";
	}

	public Class<?> getImplementationClass() {
		return getClass();
	}

	public Component getView(SacSession session) {
		//SacSession newSession = new SacSession(session.getApplication(),applicationContext,session.getUser());
		return new OrderManagementModuleView(session);

	}

	public void deactivate() {
	}

	public boolean isVisible() {
		return true;
	}

	
}