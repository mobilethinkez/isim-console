package com.mtz.isim.application.module.ordermanagement;

import com.mobilethinkez.sim.application.tabs.ui.IMSIForm;
import com.mobilethinkez.sim.application.tabs.ui.IMSIList;
import com.mobilethinkez.sim.application.tabs.ui.ManageOrderPage;
import com.mobilethinkez.sim.application.tabs.ui.OrderList;
import com.mobilethinkez.sim.application.tabs.ui.ProvisioningPage;
import com.mobilethinkez.sim.application.tabs.ui.SearchFilter;
import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.LoggerExtension;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;

public class OrderManagementModuleView extends HorizontalSplitPanel implements Accordion.SelectedTabChangeListener{

	private static final long serialVersionUID = -7949452745004903078L;
	private SacSession mSession;
	//	private Logger logger = Logger.getLogger(getClass());
	VerticalLayout rightSide;	
	CreateOrderTabs createOrderTabs;
	ManageOrderPage manageOrderPage;
	ProvisioningPage provisioningPage;
	OrderList orderList;

	IMSIList imsiList;
	IMSIForm imsiForm;


	ViewOrderTab viewOrderTab;
	
	public final String ORDER_MANAGEMENT="Order Management";
	
	public final String CREATE_ORDER="Create Order";
	public final String MODIFY_ORDER="Modify Order";
	public final String CANCEL_ORDER="Cancel Order";
	
	
	
	
	public final String BUTTONSTYLE_INSIDE_ACCORDIAN="buttonLinkStyle";
	
	public final String VIEW_INVENTORY = "View Inventory";
	Button viewInvButtonLink;

	public OrderManagementModuleView(SacSession aSession) {
		mSession = aSession;
		setSplitPosition(230, UNITS_PIXELS);
		setLocked(true);
		setFirstComponent(getSearchAccordian());
		setSizeFull();
	}
	private Accordion getSearchAccordian() {
		Accordion a = new Accordion();
        a = new Accordion();
        a.setHeight("300px");
        a.setWidth("450px");
 
        addCreateOrderTab(a);
        addModifyOrderTab(a);
        addCancelOrderTab(a);
        showTabView();
        a.addListener(this);
		return a;
	}
	
	private void addCreateOrderTab(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, CREATE_ORDER);
		a.setSizeFull();
	}
	private void addModifyOrderTab(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, MODIFY_ORDER);
		a.setSizeFull();
	}
	private void addCancelOrderTab(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, CANCEL_ORDER);
		a.setSizeFull();
	}
	
	

	public void showViewOrderTab(){
		rightSide = new VerticalLayout();
		rightSide.setSizeFull();
		//rightSide.setSpacing(true);
		//rightSide.setMargin(true);
		rightSide.addComponent(getViewOrderTabView());
		setSecondComponent(rightSide);
	}
	
	public ViewOrderTab getViewOrderTabView(){
		viewOrderTab = new ViewOrderTab();
		return viewOrderTab;
	}
	
	public void showTabView() {
		LoggerExtension.log(1, "inside the showtab view..");
		rightSide = new VerticalLayout();	
		rightSide.setSizeFull();
		//rightSide.setSpacing(true);
		//rightSide.setMargin(true);
		rightSide.addComponent(getTabView());
		setSecondComponent(rightSide);
		//this.addComponent(rightSide);

	}
	private CreateOrderTabs getTabView() {
		LoggerExtension.log(1, "inside the CreateOrderTabs getTabView view..");
		//if(createOrderTabs == null)
		{
			createOrderTabs = new CreateOrderTabs();
		}
		return createOrderTabs;
	}
	public void search(SearchFilter searchFilter) {
		showListView(searchFilter);
	}
	
   private void showListView(SearchFilter searchFilter) {
		LoggerExtension.log(1, "inside the showManageView view..");
		rightSide = new VerticalLayout();	
		rightSide.setSizeFull();
		setSecondComponent(rightSide);
   }

	
	
	@Override
	public void selectedTabChange(SelectedTabChangeEvent event) {

        TabSheet tabsheet = event.getTabSheet();
        Tab tab = tabsheet.getTab(tabsheet.getSelectedTab());
        if (tab != null) {
        	if(tab.getCaption().equalsIgnoreCase(CREATE_ORDER)){
        		LoggerExtension.log(1, "CREATE_NEW_ORDER is clicked>>>>>>>>>>>>>>>>>>>");
        		showTabView();
			}else if(tab.getCaption().equalsIgnoreCase(MODIFY_ORDER)){
        		LoggerExtension.log(1, "MODIFY_ORDER is clicked>>>>>>>>>>>>>>>>>>>");
        		showViewOrderTab();
			}else if(tab.getCaption().equalsIgnoreCase(CANCEL_ORDER)){
        		LoggerExtension.log(1, "CANCEL_ORDER is clicked>>>>>>>>>>>>>>>>>>>");
        		showViewOrderTab();
			}
        	setMargin(true);
        }
	
		
	}
	

}
