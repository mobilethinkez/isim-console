/**
 * 
 */
package com.mtz.isim.application.module.ordermanagement;

import java.util.Arrays;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.vo.VendorInfo;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * @author Satish.Yadav
 *
 */
public class VendorDetailsForm extends VerticalLayout  implements Property.ValueChangeListener{

	VendorInfo vendorInfo;
	private static final String COMMON_FIELD_WIDTH = "14em";
	Form vendorDetailsForm ;
	Button save;
	Button cancel;
	Button edit;
	private Panel panel;
	private NativeSelect vendorIDDropDown;
	TextField vendorIDTextField;

	public VendorDetailsForm(VendorInfo customerInfo1){
		this.vendorInfo = customerInfo1;
		LoggerExtension.log(1, "inside VendorDetailsForm............construtor");

		panel = new Panel();
		panel.setCaption("SIM Vendor Information");
		panel.setWidth("500px");
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true, true, true, true);
		panel.setContent(formLayout);

		//Order ID, IMSI PREFIX, SIM PREFIX, IMSI SERIAL NO, SIM
		vendorIDDropDown = new NativeSelect("Select Vendor Name");
		vendorIDDropDown.addListener(this);
		vendorIDDropDown.setImmediate(true);
		vendorIDDropDown.setNullSelectionAllowed(false);
		vendorIDDropDown.setWidth(COMMON_FIELD_WIDTH);
		List<VendorInfo> vendorListInfos = DBManager.getVendorList();
		for (int i = 0; i < vendorListInfos.size(); i++) {
			VendorInfo v = vendorListInfos.get(i);
			vendorIDDropDown.addItem(v.getVendorId());
		}
		Button addCustomer = new Button("New Vendor Click Here", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				if(vendorDetailsForm!=null){
					removeComponent(vendorDetailsForm);
					vendorInfo=new VendorInfo();
				}
				displayCustomerDetails(vendorInfo);
			}
		});
		panel.addComponent(vendorIDDropDown);
		panel.addComponent(addCustomer);
		addComponent(panel);
	}

	public VendorInfo getVendorInfo() {
		return vendorInfo;
	}

	public void setVendorInfo(VendorInfo vendorInfo) {
		this.vendorInfo = vendorInfo;
	}

	private void displayCustomerDetails(VendorInfo customerInfo2) {
		BeanItem<VendorInfo> beanItem = new BeanItem<VendorInfo>(customerInfo2);

		vendorDetailsForm = new Form();
		vendorDetailsForm.setCaption("Vendor Details");
		vendorDetailsForm.setWidth("500px");
		vendorDetailsForm.setHeight("380px");
		vendorDetailsForm.setWriteThrough(false);
		vendorDetailsForm.setFormFieldFactory(new CustomerFormFieldFactory());
		vendorDetailsForm.setItemDataSource(beanItem);
		vendorDetailsForm.setVisibleItemProperties(Arrays.asList(new String[]{
				"vendorId","name","address","city","state","country","pinCode","email","contactNumber"
		}));

		addComponent(vendorDetailsForm);
		HorizontalLayout buttonsHLayout = new HorizontalLayout();
		buttonsHLayout.setSpacing(true);
		save = new Button("SAVE", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				try {
					if(source == save){
						vendorDetailsForm.commit();
						
						ConfirmDialog.show(getWindow(), "Please Confirm:", "Are you really sure to save the Vendor information?", "Ok", "Cancel", new ConfirmDialog.Listener() {
							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to continue
									feedback(dialog.isConfirmed());
								} else {
									// User did not confirm
									feedback(dialog.isConfirmed());
								}
							}

							private void feedback(boolean confirmed) {
								// TODO Auto-generated method stub
								if(confirmed){
									LoggerExtension.log(1, "customerInfoForm::saveCustomerData customerInfo::"+vendorInfo);
									saveCustomerData();
									save.setEnabled(false);
									cancel.setEnabled(false);
									vendorDetailsForm.setEnabled(false);
									getWindow().showNotification("Vendor Information Saved Successfully.Please Select Order Info Tab for countinue.");

								}else{
									LoggerExtension.log(1, "customerInfoForm::updateCustomerData ConfirmDialog response is no!!!!!!!");
									//getWindow().showNotification("Next time.....");	
								}
							}
						});

				

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		edit = new Button("UPDATE", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				try {
					if(source == edit){
						/* If the given input is not valid there is no point in continuing */
						LoggerExtension.log(1, "customerInfoForm::updateCustomerData customerInfo::"+vendorInfo);
						// Using custom captions.
						ConfirmDialog.show(getWindow(), "Please Confirm:", "Are you really sure to update the Vendor information?", "Ok", "Cancel", new ConfirmDialog.Listener() {
							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									// Confirmed to continue
									feedback(dialog.isConfirmed());
								} else {
									// User did not confirm
									feedback(dialog.isConfirmed());
								}
							}

							private void feedback(boolean confirmed) {
								// TODO Auto-generated method stub
								if(confirmed){
									vendorDetailsForm.commit();
									updateCustomerData();
									getWindow().showNotification("Vendor Updation Done Successfully..");
								}else{
									LoggerExtension.log(1, "customerInfoForm::updateCustomerData ConfirmDialog response is no!!!!!!!");
									//getWindow().showNotification("Next time.....");	
								}
							}
						});

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		cancel = new Button("CANCEL", new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Button source = event.getButton();
				if(source == cancel)
				{
					LoggerExtension.log(1, "customerInfoForm::cancel button clicked");
					vendorDetailsForm.discard();	
				}

			}
		});
		buttonsHLayout.addComponent(save);
		buttonsHLayout.addComponent(edit);
		buttonsHLayout.addComponent(cancel);
		// cancel.setStyleName(BaseTheme.BUTTON_LINK);
		buttonsHLayout.setComponentAlignment(cancel, Alignment.MIDDLE_LEFT);
		vendorDetailsForm.getFooter().addComponent(buttonsHLayout);
		vendorDetailsForm.getFooter().setMargin(false, false, true, true);
	}

	protected void createCustomerID() {

		String custID = vendorInfo.getName();
		vendorInfo.setVendorId(custID);
		LoggerExtension.log(1, "createCustomerID() customerID:"+custID);
	}

	protected void saveCustomerData() {
		// TODO Auto-generated method stub
		//customerInfo.setCustomerId(customerInfo.getName());
		DBManager.saveVendorInfo(vendorInfo);

	}

	protected void updateCustomerData() {
		// TODO Auto-generated method stub
		//customerInfo.setCustomerId(customerInfo.getName());
		DBManager.updateVendorInfo(vendorInfo);

	}
	private class CustomerFormFieldFactory extends DefaultFieldFactory{

		//		public CustomerFormFieldFactory(String action){
		//			if(action.equalsIgnoreCase("update")){
		//				
		//			}
		//		}

		@Override
		public Field createField(Item item, Object propertyId, Component uiContext) {
			Field f;
			f = super.createField(item, propertyId, uiContext);
			if ("vendorId".equals(propertyId)) {
				vendorIDTextField = (TextField) f;
				vendorIDTextField.setRequired(true);
				vendorIDTextField.setNullSettingAllowed(false);
				vendorIDTextField.setRequiredError("Please enter a Vendor Id");
				vendorIDTextField.setWidth(COMMON_FIELD_WIDTH);
				vendorIDTextField.addValidator(new StringLengthValidator("Vendor Id must be 3-45 characters", 3, 45, false));

			}else if ("name".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a First Name");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("First Name must be 3-150 characters", 3, 150, false));

			}else if ("country".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Country");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("Country must be 3-45 characters", 3, 45, false));
			} else if ("address".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Address");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("Address must be 3-150 characters", 3, 150, false));
			} else if ("state".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a State");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("State must be 3-45 characters", 3, 45, false));
			} else if ("city".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a City");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new StringLengthValidator("City must be 3-45 characters", 3, 45, false));
			} else if ("email".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Email");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.addValidator(new EmailValidator("Email must be in this format abc@gmail.com"));
			}else if ("pinCode".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setNullRepresentation("");
				tf.setRequired(true);
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setRequiredError("Please enter a PostalCode");
				tf.addValidator(new IntegerValidator("PostalCode must be an Alphanumeric"));
			} else if ("contactNumber".equals(propertyId)) {
				TextField tf = (TextField) f;
				tf.setCaption("Contact Number");
				tf.setWidth(COMMON_FIELD_WIDTH);
				tf.setNullRepresentation("");
				tf.setRequired(true);
				tf.setRequiredError("Please enter a Contact number");
				tf.addValidator(new StringLengthValidator("Contact number must be Alphanumeric",10,14,false));
			}

			return f;
		}

	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		Property property = event.getProperty();

		if (property == vendorIDDropDown) {
			//	disableOtherFields();
			String custID = vendorIDDropDown.getValue().toString();
			LoggerExtension.log(1, "selected customerIDDropDown id...."+custID);
			// get data from table...
			vendorInfo = DBManager.getVendorInfo(custID);
			if(vendorDetailsForm!=null){
				removeComponent(vendorDetailsForm);
			}

			displayCustomerDetails(vendorInfo);
			disableSaveCancelFields();
		}
		/**
		if (property == customerId) {
		//	disableOtherFields();
			String custID = customerId.getValue().toString();
			LoggerExtension.log(1, "selected cust id...."+custID);
			// get data from table...
			customerInfo = DBManager.getCustomerInfo(custID);
			if(customerForm!=null){
				removeComponent(customerForm);
			}
			displayCustomerDetails(customerInfo);
		}
		 */
	}
	public void disableSaveCancelFields() {
		vendorIDTextField.setReadOnly(true);
		save.setEnabled(false);
		cancel.setEnabled(false);
	}
	public void ebableSaveCancelFields() {
		save.setEnabled(true);
		cancel.setEnabled(true);
	}




}
