package com.mtz.isim.application.module.ordermanagement;

import java.util.List;

import com.mobilethinkez.sim.application.tabs.ui.OrderList;
import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.vo.OrderInfo;
import com.mtz.sim.application.data.OrderContainer;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class ViewOrderTab extends VerticalLayout implements Property.ValueChangeListener {
	private NativeSelect orderId;
	private Panel panel;
	private static final String COMMON_FIELD_WIDTH = "12em";
	List<OrderInfo> orderInfoList;
	OrderList orderList ;
	Table orderTable ;
	public ViewOrderTab(){
		//setSizeFull();
		setMargin(false);
		panel = new Panel();
		panel.setCaption("View Order Details");
		//panel.setHeight("50px");
		panel.setWidth("700px");
		panel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(false, false, true, true);
		panel.setContent(formLayout);
		
		orderId = new NativeSelect("Select Order Id:");
		orderInfoList = DBManager.getOrderListofALLStatusFromOrderDetails();
		for (int i = 0; i < orderInfoList.size(); i++) {
			OrderInfo orderInfo = orderInfoList.get(i);
			orderId.addItem(orderInfo.getOrderId());
		}
		orderId.setWidth(COMMON_FIELD_WIDTH);
		orderId.addListener(this);
		orderId.setNullSelectionAllowed(false);
		orderId.setImmediate(true);
		
		panel.addComponent(orderId);
		addComponent(panel);
		
	}

	protected void showOrderDetailTable(String orderid) {
		//		orderList = new OrderList(this,orderid);
		//		LoggerExtension.log(1, "orderList::"+orderList);
		//		orderList.setHeight("300px");
		//		orderList.setWidth("600px");
		//		
		orderTable = new Table();
		orderTable.setSizeFull();
	//	orderTable.setHeight("300px");
	//	orderTable.setWidth("600px");
		orderTable.addContainerProperty("orderId",Button.class,  null);
		// The Table item identifier for the row.
	    
	    // Create a button and handle its click. A Button does not
	    // know the item it is contained in, so we have to store the
	    // item ID as user-defined data.
	    Button detailsField = new Button("orderId");
	 //   detailsField.setData(itemId);
	    detailsField.addListener(new Button.ClickListener() {
	        public void buttonClick(ClickEvent event) {
	            // Get the item identifier from the user-defined data.
	            String itemId = (String)event.getButton().getData();
	            getWindow().showNotification("Link "+
	                                   itemId+" clicked.");
	        } 
	    });
	    detailsField.addStyleName("link");
		orderTable.setContainerDataSource(OrderContainer.getOrderList(orderid));
		orderTable.setVisibleColumns(OrderContainer.NATURAL_COL_ORDER);
		orderTable.setColumnHeaders(OrderContainer.COL_HEADERS_ENGLISH);
		/*
		 * Make table selectable, react immediatedly to user events, and pass events to the
		 * controller (our main application)
		 */
		orderTable.setSelectable(true);
		orderTable.setImmediate(true);
		orderTable.addListener(this);
		/* We don't want to allow users to de-select a row */
		orderTable.setNullSelectionAllowed(false);
		orderTable.setColumnCollapsingAllowed(true);
		orderTable.setColumnReorderingAllowed(true);
		
		addComponent(orderTable);
	}

	
	
	@Override
	public void valueChange(ValueChangeEvent event) {
		Property property = event.getProperty();
	
		if (property == orderId) {
			String orderid = orderId.getValue().toString();
			if(orderTable!=null)
				removeComponent(orderTable);
			showOrderDetailTable(orderid);
		}
		if(property == orderTable){
			
		}
	
	}

	

}

