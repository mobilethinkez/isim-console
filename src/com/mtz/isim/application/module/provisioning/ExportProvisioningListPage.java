package com.mtz.isim.application.module.provisioning;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.util.PropertyManager;
import com.mtz.isim.application.vo.IMSISummary;
import com.mtz.isim.application.vo.OrderInfo;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class ExportProvisioningListPage extends VerticalLayout implements Property.ValueChangeListener {
	private NativeSelect orderIdDropDown;
//	private NativeSelect fileType;
	private Panel panel;
	
	Panel reportPanel;
	Panel attachedMsisdnPanel;
	Label orderId;
	Label totalImsi;
	Label successFullCount;
	Label failedCount;
	Label mdnUnavailabilityCount;
	Label fileNameLabel;
	IMSISummary imsiSummary;
	Button processFileButton;
	Button attachMsisdnButton;
	private static final String COMMON_FIELD_WIDTH = "12em";
	
	int orderIdCount;
	int mdnShortage;
	int successCount;
	int failedCountDueToDuplicate;
	String fileName;
	
	OrderInfo orderInfo;
	List<OrderInfo> orderInfoList;
	String filePathToExport = PropertyManager.getProperty("filePathToExport");
	String postfilePathToExport;
	String prefilePathToExport;
	
	public ExportProvisioningListPage(){
		setSizeFull();
		panel = new Panel();
		panel.setCaption("Export Provisioning List");
		panel.setWidth("700px");
		panel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(false, false, true, true);
		panel.setContent(formLayout);
		
		orderIdDropDown = new NativeSelect("Select Order Id:");
		//orderIdDropDown.setStyleName("textFieldStyle");
		orderIdDropDown.setWidth(COMMON_FIELD_WIDTH);
		orderIdDropDown.setNullSelectionAllowed(false);
		orderIdDropDown.setImmediate(true);
		orderIdDropDown.addListener(this);
		orderInfoList = DBManager.getOrderListFromOrder_Details_Table();
		for (int i = 0; i < orderInfoList.size(); i++) {
			OrderInfo orderInfo = orderInfoList.get(i);
			orderIdDropDown.addItem(orderInfo.getOrderId());
		}
		
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		attachMsisdnButton = new Button("Assign MSISDN");
		attachMsisdnButton.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				//select msisdn from msisdn_master and update sim_info table
				getMsisdnList(orderIdDropDown.getValue().toString(),orderInfo.getOrder_type());
				processFileButton.setEnabled(true);
				if(reportPanel==null){
				}else{
					removeComponent(reportPanel);
				}
				if(attachedMsisdnPanel==null){
					LoggerExtension.log(1, "attachedMsisdnPanel panel null......");
					
				}else{
					LoggerExtension.log(1, "attachedMsisdnPanel panel not null....");
					removeComponent(attachedMsisdnPanel);
				}
				showReportOfAttachedMsisdn();
			}

		});
		
		//uploadFileClass= new UploadFileClass("Upload the Response File","Upload Now");
		processFileButton = new Button("Export Provisioning List");
		processFileButton.setEnabled(false);
		processFileButton.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				String result = exportProvisioingList(orderIdDropDown.getValue().toString(),orderInfo.getOrder_type());
				if(result.equalsIgnoreCase("success")){
					{
						FileResource resource;
						//Initiate the file resource to the newly-generated file at its generated location of "src/main/webapp/tmp/generatedfile.filetype"
						resource = new FileResource(new File( prefilePathToExport), getApplication());
						//Open the generated file for download
						getWindow().open(resource);
					}
				}
				getAttachMSISDNReport(orderIdDropDown.getValue().toString(),orderInfo.getOrder_type());
				if(attachedMsisdnPanel==null){
				}else{
					removeComponent(attachedMsisdnPanel);
				}
				if(reportPanel==null){
				}else{
					removeComponent(reportPanel);
				}
				showReport();
			}

		});
		horizontalLayout.addComponent(attachMsisdnButton);
		horizontalLayout.addComponent(processFileButton);
		panel.addComponent(orderIdDropDown);
	//	panel.addComponent(fileType);
		panel.addComponent(horizontalLayout);
		//panel.addComponent(processFileButton);
		addComponent(panel);
		
	}
	protected String exportProvisioingList(String orderID,String fileType) {
		String result="success";
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddss");
		String strDate = dateFormat.format(date);
		String fileNamePre = "ProvisioningFile_"+orderID+"_Prepaid_"+strDate+".csv";
		String fileNamePost = "ProvisioningFile_"+orderID+"_Postpaid_"+strDate+".csv";
		
		if(fileType.equalsIgnoreCase("prepaid")){
			prefilePathToExport = filePathToExport+"/"+fileNamePre;
			result = DBManager.exportSimInfoForPrepaid(orderID,fileType,prefilePathToExport);
		}
		else if(fileType.equalsIgnoreCase("postpaid")){
			prefilePathToExport = filePathToExport+"/"+fileNamePost;
			result = DBManager.exportSimInfoForPostpaid(orderID,fileType,prefilePathToExport);
		}
		return result;
	}
	protected void getMsisdnList(String orderId, String fileType) {
		
		orderIdCount = DBManager.getOrderIdCount(orderId,fileType);
		List<String> msisdnList= DBManager.getMsisdnList(orderIdCount);
		
		mdnShortage = orderIdCount-msisdnList.size();
		String result;
		
		//update sim_info table for each msisdn
		for (int i = 0; i < msisdnList.size(); i++) {
			result = DBManager.assignMsisdnToIMSI(orderId,msisdnList.get(i),fileType);
			if(result.equalsIgnoreCase("success")){
				successCount++;
			}else{
				failedCountDueToDuplicate++;
			}
		}
		
		
		//update msisdn_master
		DBManager.updateMsisdnMasterTable(orderId,fileType,msisdnList);
		
		
	}
	private void getAttachMSISDNReport(String	orderId,String cardType) {
		String[] info =	DBManager.getAttachMSISDNReport(orderId,cardType);
		imsiSummary = new IMSISummary();
		
		if(info!=null)
		{
			imsiSummary.setProvImsi(info[0]);
			imsiSummary.setNotProImsi(info[1]);
			int total = Integer.parseInt(info[0])+Integer.parseInt(info[1]);
			String totalstr = String.valueOf(total);
			imsiSummary.setQuantity(totalstr);
		}


	}
	protected void showReportOfAttachedMsisdn() {
		attachedMsisdnPanel = new Panel();
		attachedMsisdnPanel.setCaption("Attach MSISDN Report");
		attachedMsisdnPanel.setWidth("700px");
		attachedMsisdnPanel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
	//	FormLayout formLayout = new FormLayout();
	//	formLayout.setMargin(true, true, true, true);
	//	attachedMsisdnPanel.setContent(formLayout);
		orderId = new Label("Order No:"+orderIdDropDown.getValue());
		orderId.setStyleName("labelStyle");
		//orderId.setValue(orderIdTextField.getValue());
		totalImsi = new Label("Total Order Quantity:"+orderIdCount);
		totalImsi.setStyleName("labelStyle");
		//totalImsi.setValue(imsiSummary.getQuantity());
		successFullCount = new Label("MSISDN Attach Successful:"+successCount);
		successFullCount.setStyleName("labelStyle");
		//successFullCount.setValue(imsiSummary.getProvImsi());
	//	failedCount = new Label("Failed due to already MSISDN associated:"+failedCountDueToDuplicate);
	//	failedCount.setStyleName("labelStyle");
	
		mdnUnavailabilityCount = new Label("Failed due to unavailability of MSISDN:"+mdnShortage);
		mdnUnavailabilityCount.setStyleName("labelStyle");
	
		//failedCount.setValue(imsiSummary.getNotProImsi());
		attachedMsisdnPanel.addComponent(orderId);
		attachedMsisdnPanel.addComponent(totalImsi);
		attachedMsisdnPanel.addComponent(successFullCount);
	//	attachedMsisdnPanel.addComponent(failedCount);
		attachedMsisdnPanel.addComponent(mdnUnavailabilityCount);
		addComponent(attachedMsisdnPanel);
		
	}

	protected void showReport() {
		reportPanel = new Panel();
		reportPanel.setCaption("Attach MSISDN Report");
		reportPanel.setWidth("700px");
		reportPanel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
	//	FormLayout formLayout = new FormLayout();
	//	formLayout.setMargin(true, true, true, true);
	//	reportPanel.setContent(formLayout);
		orderId = new Label("Order No:"+orderIdDropDown.getValue());
		orderId.setStyleName("labelStyle");
		//orderId.setValue(orderIdTextField.getValue());
		totalImsi = new Label("Total IMSI:"+imsiSummary.getQuantity());
		totalImsi.setStyleName("labelStyle");
		//totalImsi.setValue(imsiSummary.getQuantity());
		successFullCount = new Label("Attach Successful: "+imsiSummary.getProvImsi());
		successFullCount.setStyleName("labelStyle");
		//successFullCount.setValue(imsiSummary.getProvImsi());
		failedCount = new Label("Attach Failed: "+imsiSummary.getNotProImsi());
		failedCount.setStyleName("labelStyle");
		//failedCount.setValue(imsiSummary.getNotProImsi());
		fileNameLabel = new Label("File Path For Exported Provisioining File: "+prefilePathToExport);
		fileNameLabel.setStyleName("labelStyle");
		
		reportPanel.addComponent(orderId);
		reportPanel.addComponent(totalImsi);
		reportPanel.addComponent(successFullCount);
		reportPanel.addComponent(failedCount);
		reportPanel.addComponent(fileNameLabel);
		addComponent(reportPanel);
		
	}
	@Override
	public void valueChange(ValueChangeEvent event) {
	Property property = event.getProperty();
	 if(property == orderIdDropDown){
			orderInfo = DBManager.getOrderTypeFromOrderDetails(orderIdDropDown.getValue().toString());
			if(orderInfo.getOrder_type().equalsIgnoreCase("postpaid")){
				attachMsisdnButton.setEnabled(false);
				processFileButton.setEnabled(true);
			}else{
				attachMsisdnButton.setEnabled(true);
			}
		}
	
	}
	
}
