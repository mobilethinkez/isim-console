package com.mtz.isim.application.module.provisioning;

import java.io.Serializable;

import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.module.AbstractOsgiModule;
import com.vaadin.ui.Component;

public class ProvisioningModule extends AbstractOsgiModule implements Serializable {

	private static final String BUNDLE_SYMBOLIC_NAME = "com.zenithss.together.modules.ProvisioningModule";

	private static final long serialVersionUID = 132456754L;
	
	
	public String getName() {
		return "Provisioning";
	}
	
	public String getDescription() {
		return "This module contains sim provisioning functionality.";
	}
	
	public String getBundleSymbolicName() {
		return BUNDLE_SYMBOLIC_NAME;
	}
	
	public String getImage() {
		return "/com/mtz/isim/application/module/provisioning/prov.png";
	}

	public Class<?> getImplementationClass() {
		return getClass();
	}

	public Component getView(SacSession session) {
		//SacSession newSession = new SacSession(session.getApplication(),applicationContext,session.getUser());
		return new ProvisioningView(session);

	}

	public void deactivate() {
	}

	public boolean isVisible() {
		return true;
	}

	
}
