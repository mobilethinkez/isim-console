package com.mtz.isim.application.module.provisioning;

import com.mobilethinkez.sim.application.tabs.ui.OrderList;
import com.mobilethinkez.sim.application.tabs.ui.ProvisioningPage;
import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.LoggerExtension;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;

public class ProvisioningView extends HorizontalSplitPanel implements Accordion.SelectedTabChangeListener{

	private static final long serialVersionUID = -7949452745004903078L;
	private static final String CLASSNAME="ProvisioningView";
	private SacSession mSession;
	//	private Logger logger = Logger.getLogger(getClass());
	VerticalLayout rightSide;	
	ProvisioningPage provisioningPage;
	OrderList orderList;

	UploadResponseFilePage uploadResponseFilePage;
	ExportProvisioningListPage exportProvisioningListPage;
	importProvisioningListPage importProvisioningListPage;
	
	public final String UPLOAD_RESPONSE_FILE = "Upload Response File";
	public final String EXPORT_FILE = "Export Provisioning File";
	public final String IMPORT_FILE = "Import Provisioning File";

	public ProvisioningView(SacSession aSession) {
		mSession = aSession;
		setSplitPosition(230, UNITS_PIXELS);
		setLocked(false);
		setFirstComponent(getSearchAccordian());
		setSizeFull();
	}
	private Accordion getSearchAccordian() {
		Accordion a = new Accordion();
        a = new Accordion();
        a.setHeight("300px");
        a.setWidth("450px");
       
        addUploadSimTab(a);
        
     //   addProvisioningTab(a);
        
        addExportProvisioningFile(a);
        addImportProvisioningFile(a);
        
        a.addListener(this);
        showUploadResposeFileView();
       // addComponent(a);
		return a;
	}
	private void addUploadSimTab(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, UPLOAD_RESPONSE_FILE);
		a.setSizeFull();
	}
	private void addExportProvisioningFile(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, EXPORT_FILE);
		a.setSizeFull();
	}
	private void addImportProvisioningFile(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, IMPORT_FILE);
		a.setSizeFull();
	}
	
	private void showImportProvisioningView() {
		LoggerExtension.log(1, "inside the showExportProvisioningView view..");
		rightSide = new VerticalLayout();	
		rightSide.setSizeFull();
		rightSide.addComponent(getImportProvisioningListRightView());
		setSecondComponent(rightSide);
	}


	private importProvisioningListPage getImportProvisioningListRightView() {

		importProvisioningListPage = new importProvisioningListPage();
		// TODO Auto-generated method stub
		return importProvisioningListPage;
	}
	private void showExportProvisioningView() {	
		LoggerExtension.log(1, "inside the showExportProvisioningView view..");
		rightSide = new VerticalLayout();	
		rightSide.setSizeFull();
		rightSide.addComponent(getExportProvisioningListRightView());
		setSecondComponent(rightSide);
	}
	private void showUploadResposeFileView() {
		LoggerExtension.debug(CLASSNAME, "inside the showUploadResposeFileView view..");
		rightSide = new VerticalLayout();	
		rightSide.setSizeFull();
		rightSide.addComponent(getUploadResponseFileRightView());
		setSecondComponent(rightSide);

	}
	private UploadResponseFilePage getUploadResponseFileRightView() {
		LoggerExtension.debug(CLASSNAME, "inside the getProvisioningRightView view..");
		//if(uploadResponseFilePage == null)
		{
			uploadResponseFilePage = new UploadResponseFilePage();
		}
		return uploadResponseFilePage;
	}
	private ExportProvisioningListPage getExportProvisioningListRightView() {
		LoggerExtension.log(1, "inside the getExportProvisioningListRightView view..");
		//if(uploadResponseFilePage == null)
		{
			exportProvisioningListPage = new ExportProvisioningListPage();
		}
		return exportProvisioningListPage;
	}

	
	@Override
	public void selectedTabChange(SelectedTabChangeEvent event) {

        TabSheet tabsheet = event.getTabSheet();
        Tab tab = tabsheet.getTab(tabsheet.getSelectedTab());
        if (tab != null) {
        	if(tab.getCaption().equalsIgnoreCase(UPLOAD_RESPONSE_FILE)) {
        		showUploadResposeFileView();
				LoggerExtension.log(1, "UPLOAD_RESPONSE_FILE is clicked>>>>>>>>>>>>>>>>>>>");
        	} else if(tab.getCaption().equalsIgnoreCase(EXPORT_FILE)) {
        		showExportProvisioningView();
        	} else if(tab.getCaption().equalsIgnoreCase(IMPORT_FILE)) {
        		showImportProvisioningView();
        	} 
        	setMargin(true);
        }
	
		
	}
	

}
