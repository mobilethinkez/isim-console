package com.mtz.isim.application.module.provisioning;

import java.util.List;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.util.PropertyManager;
import com.mtz.isim.application.vo.IMSISummary;
import com.mtz.isim.application.vo.OrderInfo;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class UploadResponseFilePage extends VerticalLayout  implements Property.ValueChangeListener{
	private NativeSelect orderIdDropDown;
	
	//private NativeSelect fileType;
	
	private Panel panel;
	MyUploadFileClass myUploadFileClass;
	private static final String COMMON_FIELD_WIDTH = "12em";
	Panel reportPanel;
	Label orderId;
	Label totalImsi;
	Label successFullCount;
	Label failedCount;
	IMSISummary imsiSummary;
	String responseFilePath= PropertyManager.getProperty("RESPONSE_UPLOADED_FILE_PATH");
	//final ProgressIndicator indicator ;
	Button processFileButton ;
	OrderInfo orderInfo;
	List<OrderInfo> orderInfoList;
	String totalCnt;
	String failedCnt;
	String totalSuccessCount;
	String fileStatus;
	
	int orderQuantity;
	public UploadResponseFilePage(){
		setSizeFull();
		panel = new Panel();
		panel.setCaption("Upload Response File");
		panel.setWidth("700px");
		panel.setHeight("200px");
		panel.setScrollable(true);
	
		//HorizontalLayout horizontalLayout = new HorizontalLayout();
		
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(true);
		formLayout.setMargin(false, false, true, true);
		panel.setContent(formLayout);
		//orderIdTextField = new TextField("Enter Order No");
		
		orderIdDropDown = new NativeSelect("Select Order Id:");
		orderIdDropDown.setWidth(COMMON_FIELD_WIDTH);
		orderIdDropDown.setNullSelectionAllowed(false);
		orderIdDropDown.setImmediate(true);
		orderIdDropDown.addListener(this);
		//get orderList with status ORDER_SUBMITED
		orderInfoList = DBManager.getOrderListFromOrder_Details_Table();
		for (int i = 0; i < orderInfoList.size(); i++) {
			OrderInfo orderInfo = orderInfoList.get(i);
			orderQuantity= Integer.parseInt(orderInfo.getQuantity());
			
			orderIdDropDown.addItem(orderInfo.getOrderId());
		}
		myUploadFileClass = new MyUploadFileClass("","Upload & Validate",responseFilePath);
		myUploadFileClass.setEnabled(false);
		processFileButton = new Button("Process Response File");
		processFileButton.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				/** db call....  **/
				
				boolean flag = DBManager.checkOrderQuantity(orderIdDropDown.getValue().toString(),myUploadFileClass.getFile().getName(),
						myUploadFileClass.getFile().getPath(),orderInfo.getOrder_type(), orderQuantity);
				if(!flag){
					getWindow().showNotification("Please Upload Valid File,Order Quantity Count and File Records Count are not matched.",Notification.TYPE_ERROR_MESSAGE);
					return;
				}
				
				String result =uploadSimInfoTable();
				processFileButton.setEnabled(false);
				if(result.equalsIgnoreCase("success")){
					getOrderDetails(orderIdDropDown.getValue().toString(),orderInfo.getOrder_type());
					if(reportPanel!=null) { 
						removeComponent(reportPanel);
					}
					showReport();
					processFileButton.setEnabled(true);
				}//else if (result.equalsIgnoreCase("orderCountStatus")){
				//	processFileButton.setEnabled(true);
					//  getWindow().showNotification("Please Upload Valid File,Order Quantity and File Records are not same",Notification.TYPE_ERROR_MESSAGE);
					 // getWindow().showNotification("Please Upload Valid File.",Notification.TYPE_ERROR_MESSAGE);
				//}
			else{
					processFileButton.setEnabled(true);
					getWindow().showNotification("Invalid File / File has been already processed.", Notification.TYPE_ERROR_MESSAGE);
				}
			}

		});
		
		panel.addComponent(orderIdDropDown);
		panel.addComponent(myUploadFileClass);
		panel.addComponent(processFileButton);
		
		addComponent(panel);
		
	}
	private void getOrderDetails(String	orderId,String	cardType) {
		String[] info =	DBManager.getImsiProvisionInfo(orderId,cardType);
		imsiSummary = new IMSISummary();
		
		if(info!=null)
		{
			imsiSummary.setProvImsi(info[0]);
			imsiSummary.setNotProImsi(info[1]);
			int total = Integer.parseInt(info[0])+Integer.parseInt(info[1]);
			String totalstr = String.valueOf(total);
			imsiSummary.setQuantity(totalstr);
		}


	}

	protected void showReport() {
		reportPanel = new Panel();
		reportPanel.setCaption("Process Report");
		reportPanel.setWidth("700px");
		reportPanel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
	//	FormLayout formLayout = new FormLayout();
	//	formLayout.setMargin(true, true, true, true);
	//	reportPanel.setContent(formLayout);
		orderId = new Label("Order No: "+orderIdDropDown.getValue());
		orderId.setStyleName("labelStyle");
		//orderId.setValue(orderIdTextField.getValue());
		totalImsi = new Label("Total IMSI:"+totalCnt);
		totalImsi.setStyleName("labelStyle");
		//totalImsi.setValue(imsiSummary.getQuantity());
		successFullCount = new Label("Successfully Update: "+totalSuccessCount);
		successFullCount.setStyleName("labelStyle");
		//successFullCount.setValue(imsiSummary.getProvImsi());
		failedCount = new Label("Failed Due To IMSI Conflict: "+failedCnt);
		failedCount.setStyleName("labelStyle");
		//failedCount.setValue(imsiSummary.getNotProImsi());
		reportPanel.addComponent(orderId);
		reportPanel.addComponent(totalImsi);
		reportPanel.addComponent(successFullCount);
		reportPanel.addComponent(failedCount);
		addComponent(reportPanel);
		
	}
	protected String uploadSimInfoTable() {
		String result="";
		try{
		//	LoggerExtension.log(1, "fileName:"+myUploadFileClass.getFile().getName());
			LoggerExtension.log(1, "file path:"+myUploadFileClass.getFile().getPath());
			 double current = 0.0;
			 
			String[] status = DBManager.uploadResponseFileSimInfo(orderIdDropDown.getValue().toString(),myUploadFileClass.getFile().getName(),
					myUploadFileClass.getFile().getPath(),orderInfo.getOrder_type());
			processFileButton.setEnabled(false);
			result = status[0];
			totalCnt = status[1];
			failedCnt = status[2];
			totalSuccessCount = status[3];
			fileStatus = status[4];
			if(fileStatus.equalsIgnoreCase("INVALID")&& 
					status[0].equalsIgnoreCase("failed")&&Integer.parseInt(totalSuccessCount)==0){
				getWindow().showNotification("Invalid Response File",Notification.TYPE_ERROR_MESSAGE);
			}
			
		}catch (Exception e) {
			LoggerExtension.log(1, "Error in uploadSimInfoTable");
			
			
		}
		return result;
	}
	
	
	@Override
	public void valueChange(ValueChangeEvent event) {
		Property property = event.getProperty();

			orderInfo = DBManager.getOrderTypeFromOrderDetails(orderIdDropDown.getValue().toString());
			myUploadFileClass.setEnabled(true);

	}

}
