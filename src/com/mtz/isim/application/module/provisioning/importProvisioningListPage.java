package com.mtz.isim.application.module.provisioning;

import java.util.List;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.util.PropertyManager;
import com.mtz.isim.application.vo.IMSISummary;
import com.mtz.isim.application.vo.OrderInfo;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class importProvisioningListPage extends VerticalLayout implements Property.ValueChangeListener {
	private NativeSelect orderIdDropDown;
	private Panel panel;
	
	Panel reportPanel;
	Label orderId;
	Label totalImsi;
	Label successFullCount;
	Label failedCount;
	IMSISummary imsiSummary;
	Button processFileButton;
//	Button attachMsisdnButton;
	MyUploadFileClass myUploadFileClass;
	String filePathForImportProvList = PropertyManager.getProperty("filePathForImportProvList");
	private static final String COMMON_FIELD_WIDTH = "12em";
	
	OrderInfo orderInfo;
	List<OrderInfo> orderInfoList;
	
	String filePathToExport = PropertyManager.getProperty("filePathToExport");
	String postfilePathToExport;
	String prefilePathToExport;

	public importProvisioningListPage(){
		setSizeFull();
		panel = new Panel();
		panel.setCaption("Import Provisioning List");
		panel.setWidth("700px");
		panel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(false, false, true, true);
		panel.setContent(formLayout);
		
		orderIdDropDown = new NativeSelect("Select Order Id:");
		//orderIdDropDown.setStyleName("textFieldStyle");
		orderIdDropDown.setWidth(COMMON_FIELD_WIDTH);
		orderIdDropDown.setNullSelectionAllowed(false);
		orderIdDropDown.setImmediate(true);
		orderIdDropDown.addListener(this);
		orderInfoList = DBManager.getOrderListFromOrder_Details_Table();
		for (int i = 0; i < orderInfoList.size(); i++) {
			OrderInfo orderInfo = orderInfoList.get(i);
			orderIdDropDown.addItem(orderInfo.getOrderId());
		}
		myUploadFileClass = new MyUploadFileClass("","Import & Validate",filePathForImportProvList);
		processFileButton = new Button("Process Provisioned File");
		processFileButton.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				String result =updateSimInfoTableForFinalProvisioning(orderInfo.getOrder_type());
				if(result.equalsIgnoreCase("success")){
					getAttachMSISDNReport(orderIdDropDown.getValue().toString(),orderInfo.getOrder_type());
					if(reportPanel!=null) removeComponent(reportPanel);
					showReport();
				}else{
					getWindow().showNotification("Please import valid provisioned file.",Notification.TYPE_ERROR_MESSAGE);
				}
			}

		});
		panel.addComponent(orderIdDropDown);
		panel.addComponent(myUploadFileClass);
		panel.addComponent(processFileButton);
		addComponent(panel);
		
	}
	protected String updateSimInfoTableForFinalProvisioning(String card_type) {
		LoggerExtension.log(1, "updateSimInfoTable fileName:"+myUploadFileClass.getFile().getName());
		LoggerExtension.log(1, "updateSimInfoTable file path:"+myUploadFileClass.getFile().getPath());
		String result = DBManager.updateSimInfoTableForFinalProvisioning(orderIdDropDown.getValue().toString(),myUploadFileClass.getFile().getName(),myUploadFileClass.getFile().getPath(),card_type);
		return result;
	}
//	protected String exportProvisioingList(String orderID,String fileType) {
//		String result="success";
//		Date date = new Date();
//		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddss");
//		String strDate = dateFormat.format(date);
//		String fileNamePre = "ProvisioningFile_"+orderID+"_Prepaid_"+strDate+".csv";
//		String fileNamePost = "ProvisioningFile_"+orderID+"_Postpaid_"+strDate+".csv";
//		
//		if(fileType.equalsIgnoreCase("prepaid")){
//			prefilePathToExport = filePathToExport+"/"+fileNamePre;
//			result = DBManager.exportSimInfoForPrepaid(orderID,fileType,prefilePathToExport);
//		}
//		else if(fileType.equalsIgnoreCase("postpaid")){
//			prefilePathToExport = filePathToExport+"/"+fileNamePost;
//			result = DBManager.exportSimInfoForPostpaid(orderID,fileType,prefilePathToExport);
//		}
//		return result;
//	}
	protected void getMsisdnList(String orderId, String fileType) {
		
		int orderIdCount = DBManager.getOrderIdCount(orderId,fileType);
		List<String> msisdnList= DBManager.getMsisdnList(orderIdCount);
		
		//update sim_info table for each msisdn
		
	//	DBManager.assignMsisdnToIMSI(orderId,msisdnList,fileType);
		for (int i = 0; i < msisdnList.size(); i++) {
			DBManager.assignMsisdnToIMSI(orderId,msisdnList.get(i),fileType);	
		}
		//update msisdn_master 
		
		
	}
	private void getAttachMSISDNReport(String orderId,String fileType) {
		String[] info =	DBManager.getAttachMSISDNReport(orderId,fileType);
		imsiSummary = new IMSISummary();
		
		if(info!=null)
		{
			imsiSummary.setProvImsi(info[1]);
			imsiSummary.setNotProImsi(info[0]);
			int total = Integer.parseInt(info[0])+Integer.parseInt(info[1]);
			String totalstr = String.valueOf(total);
			imsiSummary.setQuantity(totalstr);
		}


	}

	protected void showReport() {
		reportPanel = new Panel();
		reportPanel.setCaption("Attach MSISDN Report");
		reportPanel.setWidth("700px");
		reportPanel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
	//	FormLayout formLayout = new FormLayout();
	//	formLayout.setMargin(true, true, true, true);
	//	reportPanel.setContent(formLayout);
		orderId = new Label("Order No: "+orderIdDropDown.getValue());
		orderId.setStyleName("labelStyle");
		//orderId.setValue(orderIdTextField.getValue());
		totalImsi = new Label("Total IMSI:"+imsiSummary.getQuantity());
		totalImsi.setStyleName("labelStyle");
		//totalImsi.setValue(imsiSummary.getQuantity());
		successFullCount = new Label("Provision Successfully Done: "+imsiSummary.getProvImsi());
		successFullCount.setStyleName("labelStyle");
		//successFullCount.setValue(imsiSummary.getProvImsi());
		failedCount = new Label("Provision Failed: "+imsiSummary.getNotProImsi());
		failedCount.setStyleName("labelStyle");
		//failedCount.setValue(imsiSummary.getNotProImsi());
		reportPanel.addComponent(orderId);
		reportPanel.addComponent(totalImsi);
		reportPanel.addComponent(successFullCount);
		reportPanel.addComponent(failedCount);
		addComponent(reportPanel);
		
	}
	@Override
	public void valueChange(ValueChangeEvent event) {
		Property property = event.getProperty();
	
		if(property == orderIdDropDown){
			orderInfo = DBManager.getOrderTypeFromOrderDetails(orderIdDropDown.getValue().toString());
		}	
	
	}
	private void disableSaveCancelFields() {
		// TODO Auto-generated method stub
		//attachMsisdnButton.setEnabled(false);
	}
	

}
