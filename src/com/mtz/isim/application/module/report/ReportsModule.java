package com.mtz.isim.application.module.report;

import java.io.Serializable;

import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.module.AbstractOsgiModule;
import com.vaadin.ui.Component;

public class ReportsModule extends AbstractOsgiModule implements Serializable {

	private static final String BUNDLE_SYMBOLIC_NAME = "com.zenithss.together.modules.admin";

	private static final long serialVersionUID = 132456754L;
	
	
	public String getName() {
		return "Reports";
	}
	
	public String getDescription() {
		return "This module contains sim reports functionality.";
	}
	
	public String getBundleSymbolicName() {
		return BUNDLE_SYMBOLIC_NAME;
	}
	
	public String getImage() {
		return "/com/mtz/isim/application/module/report/report-small.png";
	}

	public Class<?> getImplementationClass() {
		return getClass();
	}

	public Component getView(SacSession session) {
		//SacSession newSession = new SacSession(session.getApplication(),applicationContext,session.getUser());
		return new ReportsModuleView(session);

	}

	public void deactivate() {
	}

	public boolean isVisible() {
		return true;
	}

	
}