package com.mtz.isim.application.module.report;

import com.mobilethinkez.sim.application.tabs.ui.OrderList;
import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.LoggerExtension;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;

public class ReportsModuleView extends HorizontalSplitPanel implements Accordion.SelectedTabChangeListener{

	private static final long serialVersionUID = -7949452745004903078L;
	private SacSession mSession;
	//	private Logger logger = Logger.getLogger(getClass());
	VerticalLayout rightSide;	
	OrderList orderList;
	ViewInventoryPage viewInventoryPage;
	public final String VIEW_INVENTORY = "View Inventory";
	public final String REPORTS = "Reports";
	Button viewInvButtonLink;
	public ReportsModuleView(SacSession aSession) {
		mSession = aSession;
		setSplitPosition(230, UNITS_PIXELS);
		setLocked(false);
		setFirstComponent(getSearchAccordian());
		setSizeFull();
	}
	private Accordion getSearchAccordian() {
		Accordion a = new Accordion();
        a = new Accordion();
        a.setHeight("300px");
        a.setWidth("450px");
        addaInventoryTab(a);
        showViewInventory();
        addReprotTab(a);
        a.addListener(this);
		return a;
	}
	private void addaInventoryTab(Accordion a) {
		VerticalLayout lSearchLayout = new VerticalLayout();
		a.addTab(lSearchLayout, VIEW_INVENTORY);
		a.setSizeFull();	
	}
	
	private void addReprotTab(Accordion a) {

		VerticalLayout lSearchLayout = new VerticalLayout();
		lSearchLayout.setSizeFull();
		lSearchLayout.setMargin(true);

		Label createOrderLabel = new Label("Report");
		lSearchLayout.addComponent(createOrderLabel);
		
		a.addTab(lSearchLayout, "Report");

		a.setSizeFull();
		
	}
	private void showViewInventory() {
		LoggerExtension.log(1, "inside the showExportProvisioningView view..");
		rightSide = new VerticalLayout();	
		rightSide.setSizeFull();
		rightSide.addComponent(getViewInventoryRightPage());
		setSecondComponent(rightSide);

	}
	private ViewInventoryPage getViewInventoryRightPage() {
		
		viewInventoryPage = new ViewInventoryPage();
		return viewInventoryPage;
	}
	
	@Override
	public void selectedTabChange(SelectedTabChangeEvent event) {

        TabSheet tabsheet = event.getTabSheet();
        Tab tab = tabsheet.getTab(tabsheet.getSelectedTab());
        if (tab != null) {
        	if(tab.getCaption().equalsIgnoreCase(VIEW_INVENTORY)) {
        		showViewInventory();
        	}
        	else if(tab.getCaption().equalsIgnoreCase("Reports")) {
					
			}
        	setMargin(true);
        }
	
		
	}
	

}
