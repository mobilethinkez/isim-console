package com.mtz.isim.application.module.report;

import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.vo.IMSISummary;
import com.mtz.isim.application.vo.SimInfo;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ViewInventoryPage extends VerticalLayout implements Property.ValueChangeListener {
	private NativeSelect viewReportType;
	private Panel panel;
	
	Panel reportPanel;
	Panel orderIDReportPanel;
	
	Label orderId;
	Label totalImsi;
	Label successFullCount;
	Label failedCount;
	Label mdnUnavailabilityCount;
	Label fileNameLabel;
	Label provDoneSimCountLabel;
	IMSISummary imsiSummary;
	Button processFileButton;
	Button attachMsisdnButton;
	private static final String COMMON_FIELD_WIDTH = "12em";
	
	int orderIdCount;
	int provDoneCount;
	int provNotCount;
	int provRaisedCount;
	int prepaidCount;
	int postpaidCount;
	
	int totalsimCount;
	int prepaidSimCount;
	int postpaidSimCount;
	int provDoneSimCount;
	int provRaisedSimCount;
	int proNotSimCount;
	
	String fileName;
	private TextField orderIdTextField;

	SimInfo simInfo;
	public ViewInventoryPage(){
		setSizeFull();
		panel = new Panel();
		panel.setCaption("Report");
		panel.setWidth("700px");
		panel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
		FormLayout formLayout = new FormLayout();
		formLayout.setMargin(false, false, true, true);
		panel.setContent(formLayout);
		
		viewReportType = new NativeSelect("Select ");
		viewReportType.addItem("ORDER ID");
		viewReportType.addItem("IMSI NO");
		viewReportType.addItem("SIM Availability");
		viewReportType.setWidth(COMMON_FIELD_WIDTH);
		viewReportType.addListener(this);
		viewReportType.setNullSelectionAllowed(false);
		viewReportType.setImmediate(true);
		
		orderIdTextField = new TextField("Enter Value");
		orderIdTextField.setStyleName("textFieldStyle");
		attachMsisdnButton = new Button("VIEW REPORT");
		attachMsisdnButton.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				if(reportPanel==null){
				}else{
					removeComponent(reportPanel);
				}
				if(orderIDReportPanel==null){
					LoggerExtension.log(1, "attachedMsisdnPanel panel null......");
				}else{
					LoggerExtension.log(1, "attachedMsisdnPanel panel not null....");
					removeComponent(orderIDReportPanel);
				}
				if(viewReportType.getValue().toString().equalsIgnoreCase("ORDER ID")){
					getOrderIDReport();
					showOrderIDReport();	
				}else if(viewReportType.getValue().toString().equalsIgnoreCase("IMSI NO")){
					getIMSIReport();
					showIMSIReport();	
				}else if(viewReportType.getValue().toString().equalsIgnoreCase("SIM Availability")){
					getSimReport();
					showSimReport();	
				}
				
			}

		});
		panel.addComponent(viewReportType);
		panel.addComponent(orderIdTextField);
		panel.addComponent(attachMsisdnButton);
		addComponent(panel);
		
	}
	protected void getSimReport() {
		String [] simStatus = DBManager.getSimReportDetails();
		String [] simCardType = DBManager.getSimReporCardTypetDetails();

		
		prepaidSimCount = Integer.parseInt(simCardType[0]);
		postpaidSimCount =  Integer.parseInt(simCardType[1]);
		
		totalsimCount =  prepaidSimCount+postpaidSimCount;
		provDoneSimCount =  Integer.parseInt(simStatus[2]);
		provRaisedSimCount =  Integer.parseInt(simStatus[1]);
		proNotSimCount = Integer.parseInt(simStatus[0]);

	}

	protected void showSimReport() {
		orderIDReportPanel = new Panel();
		orderIDReportPanel.setCaption("SIM AVAILABILITY");
		orderIDReportPanel.setWidth("700px");
		orderIDReportPanel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
	//	FormLayout formLayout = new FormLayout();
	//	formLayout.setMargin(true, true, true, true);
	//	attachedMsisdnPanel.setContent(formLayout);
		orderId = new Label("TOTAL  SIM:"+totalsimCount);
		orderId.setStyleName("labelStyle");
		//orderId.setValue(orderIdTextField.getValue());
		totalImsi = new Label("PREPAID SIM:"+prepaidSimCount);
		totalImsi.setStyleName("labelStyle");
		//totalImsi.setValue(imsiSummary.getQuantity());
		successFullCount = new Label("POSTPAID SIM:"+postpaidSimCount);
		successFullCount.setStyleName("labelStyle");
		
		provDoneSimCountLabel = new Label("Provision Done:"+provDoneSimCount);
		provDoneSimCountLabel.setStyleName("labelStyle");
	
		//successFullCount.setValue(imsiSummary.getProvImsi());
		failedCount = new Label("Provision Raised :"+provRaisedSimCount);
		failedCount.setStyleName("labelStyle");
	
		mdnUnavailabilityCount = new Label("Not Provision:"+proNotSimCount);
		mdnUnavailabilityCount.setStyleName("labelStyle");
	
		//failedCount.setValue(imsiSummary.getNotProImsi());
		orderIDReportPanel.addComponent(orderId);
		orderIDReportPanel.addComponent(totalImsi);
		orderIDReportPanel.addComponent(successFullCount);
		orderIDReportPanel.addComponent(provDoneSimCountLabel);
		orderIDReportPanel.addComponent(failedCount);
		orderIDReportPanel.addComponent(mdnUnavailabilityCount);
		
		addComponent(orderIDReportPanel);
		
	}

	
	protected void getIMSIReport() {
		simInfo = DBManager.getIMSIDetails(orderIdTextField.getValue().toString());
		
	}

	protected void getOrderIDReport(){
		String [] orderIdStatus = DBManager.getOrderIdStatusDetails(orderIdTextField.getValue().toString());
		String [] orderIdCardType = DBManager.getOrderIdCardTypeDetails(orderIdTextField.getValue().toString());
		
		prepaidCount=Integer.parseInt(orderIdCardType[0]);
		postpaidCount=Integer.parseInt(orderIdCardType[1]);
		
		provNotCount=Integer.parseInt(orderIdStatus[0]);
		provRaisedCount=Integer.parseInt(orderIdStatus[1]);
		provDoneCount=Integer.parseInt(orderIdStatus[2]);
		
		orderIdCount=prepaidCount+postpaidCount;
	}
	
	protected void showOrderIDReport() {
		orderIDReportPanel = new Panel();
		orderIDReportPanel.setCaption("Order ID Summary");
		orderIDReportPanel.setWidth("700px");
		orderIDReportPanel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
	//	FormLayout formLayout = new FormLayout();
	//	formLayout.setMargin(true, true, true, true);
	//	attachedMsisdnPanel.setContent(formLayout);
		orderId = new Label("Order No:"+orderIdTextField.getValue());
		orderId.setStyleName("labelStyle");
		//orderId.setValue(orderIdTextField.getValue());
		totalImsi = new Label("Total Order Quantity:"+orderIdCount);
		totalImsi.setStyleName("labelStyle");
		//totalImsi.setValue(imsiSummary.getQuantity());
		successFullCount = new Label("Provision Successfully Done:"+provDoneCount);
		successFullCount.setStyleName("labelStyle");
		//successFullCount.setValue(imsiSummary.getProvImsi());
		failedCount = new Label("Provision Raised :"+provRaisedCount);
		failedCount.setStyleName("labelStyle");
	
		mdnUnavailabilityCount = new Label("Not Provision:"+provNotCount);
		mdnUnavailabilityCount.setStyleName("labelStyle");
	
		//failedCount.setValue(imsiSummary.getNotProImsi());
		orderIDReportPanel.addComponent(orderId);
		orderIDReportPanel.addComponent(totalImsi);
		orderIDReportPanel.addComponent(successFullCount);
		orderIDReportPanel.addComponent(failedCount);
		orderIDReportPanel.addComponent(mdnUnavailabilityCount);
		addComponent(orderIDReportPanel);
		
	}

	protected void showIMSIReport() {
		reportPanel = new Panel();
		reportPanel.setCaption("IMSI Details");
		reportPanel.setWidth("700px");
		reportPanel.setScrollable(false);
		/* Use a FormLayout as main layout for this Panel */
	//	FormLayout formLayout = new FormLayout();
	//	formLayout.setMargin(true, true, true, true);
	//	reportPanel.setContent(formLayout);
		orderId = new Label("Order No:"+simInfo.getOrder_id());
		orderId.setStyleName("labelStyle");
		//orderId.setValue(orderIdTextField.getValue());
		totalImsi = new Label("MSISDN:"+simInfo.getMsisdn());
		totalImsi.setStyleName("labelStyle");
		//totalImsi.setValue(imsiSummary.getQuantity());
		successFullCount = new Label("SERIAL NUMBER:"+simInfo.getSerial_number());
		successFullCount.setStyleName("labelStyle");
		//successFullCount.setValue(imsiSummary.getProvImsi());
		failedCount = new Label("CARD TYPE:"+simInfo.getCard_type());
		failedCount.setStyleName("labelStyle");
		//failedCount.setValue(imsiSummary.getNotProImsi());
		fileNameLabel = new Label("PROVISION STATUS: "+simInfo.getStatus());
		fileNameLabel.setStyleName("labelStyle");
		
		reportPanel.addComponent(orderId);
		reportPanel.addComponent(totalImsi);
		reportPanel.addComponent(successFullCount);
		reportPanel.addComponent(failedCount);
		reportPanel.addComponent(fileNameLabel);
		addComponent(reportPanel);
		
	}
	@Override
	public void valueChange(ValueChangeEvent event) {
		Property property = event.getProperty();
	
		if (property == viewReportType) {
			String selectedfileType = viewReportType.getValue().toString();
			LoggerExtension.log(1, "selected selectedfileType id...."+selectedfileType);
		    if(selectedfileType.equalsIgnoreCase("SIM Availability"))	{
		    	orderIdTextField.setValue("");
		    	orderIdTextField.setEnabled(false);
		    }else{
		    	
		    	orderIdTextField.setEnabled(true);
		    	orderIdTextField.setValue("");
			}
		}
	
	}
	private void disableSaveCancelFields() {
		// TODO Auto-generated method stub
		attachMsisdnButton.setEnabled(false);
	}
	

}
