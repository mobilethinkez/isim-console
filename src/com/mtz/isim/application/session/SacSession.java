package com.mtz.isim.application.session;

import com.mtz.isim.application.vo.UserData;
import com.mtz.isim.module.ModulableApplication;
import com.vaadin.service.ApplicationContext;

public class SacSession {

	private UserData user;
	private transient ModulableApplication sacApplication;
	private transient ApplicationContext applicationContext;

	public SacSession(ModulableApplication aApp, ApplicationContext aCtx) {
		sacApplication = aApp;
		this.applicationContext = aCtx;
	}

	public SacSession(ModulableApplication aApp, ApplicationContext aCtx, UserData user) {
		sacApplication = aApp;
		this.applicationContext = aCtx;
		this.user=user;
	}
	
	

	public void setUser(UserData user) {
		this.user = user;
	}

	public UserData getUser() {
		return user;
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public ModulableApplication getApplication() {
		return sacApplication;
	}

}
