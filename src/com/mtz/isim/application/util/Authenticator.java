package com.mtz.isim.application.util;

import java.util.Properties;

import com.mtz.isim.application.vo.UserData;

public class Authenticator {
	static Properties prop;

	/*static {
		initAuthenticator();
	}*/
	
	public static String enryptString(String strTest){
		// Hash a password for the first time
		//String hashed = BCrypt.hashpw(password, BCrypt.gensalt());

		// gensalt's log_rounds parameter determines the complexity
		// the work factor is 2**log_rounds, and the default is 10
		String hashed = BCrypt.hashpw(strTest, BCrypt.gensalt(12));

		// Check that an unencrypted password matches one that has
		// previously been hashed
		/*if (BCrypt.checkpw(candidate, hashed))
			System.out.println("It matches");
		else
			System.out.println("It does not match");*/
		return hashed;
	}
	/*public static void initAuthenticator(){
		prop=new Properties();
		try {
			InputStream inputStream = Authenticator.class.getClassLoader().getResourceAsStream("users.properties");

			prop.load(inputStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} */
	public static boolean validateUser(String user,String password){
		System.out.println("validateUser :"+user);
		DBManager dbMgr = DBManager.getInstance();
		String hashedPassword = dbMgr.getHashedPasswordForUser(user);
		if(hashedPassword!=null && checkMatch(password,hashedPassword)){
			return true;
		}else{
			return false;
		}
	}
	public static UserData getUserDetails(String user){
		System.out.println("getUserDetails :"+user);
		DBManager dbMgr = DBManager.getInstance();
		UserData userData = dbMgr.getRoleForUser(user);
		return userData;
	}
	public static boolean checkMatch(String candidate,String hashed){
		if (BCrypt.checkpw(candidate, hashed))
		{
			return true;
		}
		else
			return false;
	}
}
