package com.mtz.isim.application.util;

import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;

public class BasicDataSourceWrapper {
		private static PropertyManager pm = PropertyManager.getPropertyManager();
		private static org.apache.commons.dbcp.BasicDataSource dataSource=null;
		public static String dburl = PropertyManager.getProperty("OS_DBURL");
		public static String user = PropertyManager.getProperty("OS_USER");
		public static String password = PropertyManager.getProperty("OS_PASSWORD");
		public static String driver = PropertyManager.getProperty("OS_DBDRIVER");
		public static int poolsize = Integer.parseInt(PropertyManager.getProperty("OS_POOLSIZE"));
		public static BasicDataSourceWrapper basicDataSourceWrapper = new BasicDataSourceWrapper();
		private  BasicDataSourceWrapper () {
	    	BasicDataSource ds = new BasicDataSource();
	        ds.setDriverClassName(driver);
	        ds.setUsername(user);
	        ds.setPassword(password);
	        ds.setUrl(dburl);
	        ds.setMaxActive(poolsize);
	        ds.setInitialSize(5);
	        ds.setMaxIdle(5);
	        ds.setMaxWait(1000);
	        ds.setNumTestsPerEvictionRun(1);
	        ds.setTimeBetweenEvictionRunsMillis(1000);
	        ds.setMinEvictableIdleTimeMillis(1000);
	        ds.setTestWhileIdle(true);
	        ds.setValidationQuery("select current_date");
	        dataSource = ds;
	    }

	    public static org.apache.commons.dbcp.BasicDataSource getInstance() {
	    //	LoggerExtension.log(11, "Entered BasicDataSource getInstance()");
	    	return BasicDataSourceWrapper.dataSource;
	    }
	    
	    public static org.apache.commons.dbcp.BasicDataSource restartDataSource() throws SQLException {
	        dataSource.close();
	        BasicDataSource ds = new BasicDataSource();
	        ds.setDriverClassName(driver);
	        ds.setUsername(user);
	        ds.setPassword(password);
	        ds.setUrl(dburl);
	        ds.setMaxActive(poolsize);
	        ds.setMaxIdle(1);
	        ds.setMaxWait(2000);
	        ds.setNumTestsPerEvictionRun(5);
	        ds.setTimeBetweenEvictionRunsMillis(2000);
	        ds.setMinEvictableIdleTimeMillis(2000);
	        ds.setTestWhileIdle(true);
	        ds.setValidationQuery("select current_date");
	        dataSource = ds;
	        return dataSource;
	    }
	}
