package com.mtz.isim.application.util;

import java.io.InputStream;

import com.vaadin.terminal.StreamResource;

public class ClasspathStreamSource implements StreamResource.StreamSource {
	private static final long serialVersionUID = 1L;
	private String path;
	private Class<?> implClass;

	public ClasspathStreamSource(String path, Class<?> implClass) {
		this.path = path;
		this.implClass = implClass;
	}

	public InputStream getStream() {
		return implClass.getResourceAsStream(path);
	}
}