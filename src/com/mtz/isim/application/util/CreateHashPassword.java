/**
 * 
 */
package com.mtz.isim.application.util;

/**
 * @author Satish.Yadav
 *
 */
public class CreateHashPassword {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String pw = BCrypt.hashpw("admin", BCrypt.gensalt(12));
		System.out.println("Password::"+pw);
		boolean flag = BCrypt.checkpw("admin", "$2a$12$/PNiWwV6/lU1GBE8oPmDmunjToknaeQyyLpL8ce4tVLdipEJ/B6de");
		System.out.println("matched::"+flag);
	}

}
