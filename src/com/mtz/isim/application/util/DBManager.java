package com.mtz.isim.application.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.dbcp.BasicDataSource;

import com.mobilethinkez.sim.application.tabs.ui.SearchFilter;
import com.mtz.isim.application.vo.CustomerInfo;
import com.mtz.isim.application.vo.ImsiMappingInfo;
import com.mtz.isim.application.vo.OrderInfo;
import com.mtz.isim.application.vo.SimInfo;
import com.mtz.isim.application.vo.User;
import com.mtz.isim.application.vo.UserData;
import com.mtz.isim.application.vo.VendorInfo;
import com.mtz.sim.application.data.OrderContainer;
import com.mtz.sim.application.data.SimInfoContainer;

public class DBManager {
	private static PropertyManager pm = PropertyManager.getPropertyManager();
	private transient static BasicDataSource ds=null;
	private static DBManager dbManager = null;
	private static String className="DBManager ";
	BufferedReader br = null;
	public DBManager(){
		ds=BasicDataSourceWrapper.getInstance();
		LoggerExtension.log(2, " got instance of BasicDataSourceWrapper");
	}
	/**
	 * 
	 * @return
	 */
	public synchronized static DBManager getInstance(){
		if(dbManager == null){
			try{
				LoggerExtension.log(2,"Class "+className+"Inside the DBManager getInstance to initialize the dbManager");
				dbManager = new DBManager();	
			}catch(Exception e){
				LoggerExtension.log(3,"Exception is  "+ e);
			}
		}
		return dbManager;
	}
	/**
	 * 
	 * @param cConnection
	 * @return
	 */ 
	public static boolean releaseConnection(Connection cConnection) {
		try{  
			if(cConnection!= null){
				cConnection.close();
				//LoggerExtension.log(2,"Class "+className+"Db Connection is closed.....");
				return true;
			}else{
				LoggerExtension.log(2,"Class "+className+"There was no connection present");;
				return false;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * 
	 * @param throwable
	 * @return
	 */
	public static String getStackTrace(Throwable throwable) {
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		throwable.printStackTrace(printWriter);
		return writer.toString();
	}
	public static User getUserByEmail(String aEmail) {
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		User user = new User();
		try {
			connection = ds.getConnection();
			String query = "SELECT * FROM users WHERE email=?";
			LoggerExtension.log(2,"Class "+className+"getContestInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, aEmail);

			rs = psmt.executeQuery();
			while(rs.next()){
				user.setEmail(rs.getString("email"));
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return user;
	}
	public static User findByEmailAndPassword(String aEmail, String aPassword) {
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		User user = new User();
		try {
			connection = ds.getConnection();
			String query = "SELECT * FROM users WHERE email=? and password=?";
			LoggerExtension.log(2,"Class "+className+"getContestInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, aEmail);
			psmt.setString(2, aPassword);
			rs = psmt.executeQuery();
			while(rs.next()){
				user.setEmail(rs.getString("email"));
				user.setRole(rs.getString("role"));
				user.setDomain(rs.getString("domain"));
				user.setStatus(rs.getString("status"));
				user.setFirstName(rs.getString("first_name"));
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(1,"User::"+user.toString());
		return user;
	}

	public static String saveCustomerInfo(CustomerInfo customerInfo){
		LoggerExtension.log(2,"Class "+className+"saveCustomerInfo query: "+customerInfo.toString());
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			//INSERT INTO CUSTOMER_INFO (name,address,city,state,pincode,country,email,phone,customer_id) VALUES('SATISH','SUS','PUNE','MAHARASTRA',41,'INIDA','SMAIL.COM',73,'sdff')
			String query = "INSERT INTO CUSTOMER_INFO (name,address,city,state,pincode,country,email,phone,customer_id) VALUES(?,?,?,?,?,?,?,?,?)";
			LoggerExtension.log(2,"Class "+className+"saveCustomerInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, customerInfo.getName());
			psmt.setString(2, customerInfo.getAddress());
			psmt.setString(3, customerInfo.getCity());
			psmt.setString(4, customerInfo.getState());
			psmt.setString(5, customerInfo.getPinCode());
			psmt.setString(6, customerInfo.getCountry());
			psmt.setString(7, customerInfo.getEmail());
			psmt.setString(8, customerInfo.getPhoneNumber());
			psmt.setString(9, customerInfo.getCustomerId());
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}	
		}catch (Exception e) {
			result="failed";
			LoggerExtension.log(3,"Exception while retrieving saveCustomerInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in saveCustomerInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"saveCustomerInfo query result: "+result);
		return result;
	}

	public static List<CustomerInfo>  getCustomerList() {
		List<CustomerInfo> list = new ArrayList<CustomerInfo>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			String query = "SELECT name,address,city,state,pincode,country,email,phone,customer_id FROM CUSTOMER_INFO";
			LoggerExtension.log(2,"Class "+className+" getCustomerList query : "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()) {
				CustomerInfo customerInfo = new CustomerInfo();

				customerInfo.setName(rs.getString("name"));
				customerInfo.setAddress(rs.getString("address"));
				customerInfo.setCity(rs.getString("city"));
				customerInfo.setState(rs.getString("state"));
				customerInfo.setPinCode(rs.getString("pincode"));
				customerInfo.setCountry(rs.getString("country"));
				customerInfo.setEmail(rs.getString("email"));
				customerInfo.setPhoneNumber(rs.getString("phone"));
				customerInfo.setCustomerId(rs.getString("customer_id"));

				LoggerExtension.log(2,"Class "+className+" getCustomerList customerInfo: "+customerInfo);

				list.add(customerInfo);
			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting Role For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return list;
	}

	public static String saveVendorInfo(VendorInfo vendorInfo) {
		LoggerExtension.log(2,"Class "+className+"saveVendorInfo query: "+vendorInfo.toString());
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="";
		try {
			connection = ds.getConnection();
			//INSERT INTO CUSTOMER_INFO (name,address,city,state,pincode,country,email,phone,customer_id) VALUES('SATISH','SUS','PUNE','MAHARASTRA',41,'INIDA','SMAIL.COM',73,'sdff')
			String query = "INSERT INTO VENDOR_INFO (name,address,city,state,pincode,country,email,phone,vendor_Id) VALUES(?,?,?,?,?,?,?,?,?)";
			LoggerExtension.log(2,"Class "+className+"saveCustomerInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, vendorInfo.getName());
			psmt.setString(2, vendorInfo.getAddress());
			psmt.setString(3, vendorInfo.getCity());
			psmt.setString(4, vendorInfo.getState());
			psmt.setString(5, vendorInfo.getPinCode());
			psmt.setString(6, vendorInfo.getCountry());
			psmt.setString(7, vendorInfo.getEmail());
			psmt.setString(8, vendorInfo.getContactNumber());
			psmt.setString(9, vendorInfo.getVendorId());
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}	
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving saveVendorInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in saveVendorInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"saveVendorInfo query result: "+result);
		return result;
	}


	public static String saveOrderInfo(OrderInfo orderInfo) {
		LoggerExtension.log(2,"Class "+className+"saveOrderInfo query: "+orderInfo.toString());
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		Date tmpDate = orderInfo.getExpectedDeliveryDate();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String currdt = sdf.format(tmpDate);
		LoggerExtension.log(1, "curreDate:"+currdt);
		getImsiRange(orderInfo);
		LoggerExtension.log(1,"start:"+orderInfo.getStart_imsi_range()+" end:"+orderInfo.getEnd_imsi_range());
		try {
			connection = ds.getConnection();
			//  insert into order_details(order_id,quantity,sim_prefix,imsi_prefix,sim_card_format,transport_key,electrical_profile,carrier_name,brand,
			//start_sim,start_imsi,graphical_profile,network_element) values('1',10000000,364390001120449,890139120027030000,'a','3','a','pre','aa',
			//364390001120449,890139120027030000,'a','a')

			//order_id,customer_id,vendor_id,quantity,sim_prefix,imsi_prefix,start_sim,start_imsi,sim_card_format,transport_key,electrical_profile,
			//carrier_name,brand,graphical_profile,network_element,card_type,order_type,expectedDeliveryDate,creationDate,modifyDate,status,
			String query = "INSERT INTO ORDER_DETAILS (order_id,quantity,sim_prefix,imsi_prefix,sim_card_format," +
					"transport_key,electrical_profile,carrier_name,brand,start_sim," +
					"start_imsi,graphical_profile,network_element,customer_id,status,vendor_id,card_type,order_type,expectedDeliveryDate,start_imsi_range,end_imsi_range)" +
					" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			LoggerExtension.log(2,"Class "+className+"saveOrderInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderInfo.getOrderId());
			psmt.setLong(2,Long.parseLong(orderInfo.getQuantity()));
			psmt.setLong(3,Long.parseLong(orderInfo.getSim_prefix()));
			psmt.setLong(4,Long.parseLong(orderInfo.getImsi_prefix()));
			psmt.setString(5, (orderInfo.getSim_card_format()));
			psmt.setString(6, (orderInfo.getTransport_key()));
			psmt.setString(7, orderInfo.getElectrical_profile());
			psmt.setString(8, orderInfo.getCarrier_name());
			psmt.setString(9, orderInfo.getBrand());
			psmt.setLong(10,Long.parseLong(orderInfo.getStart_sim()));
			psmt.setLong(11,Long.parseLong(orderInfo.getStart_imsi()));
			psmt.setString(12, orderInfo.getGraphical_profile());
			psmt.setString(13, orderInfo.getNetwork_element());
			psmt.setString(14, orderInfo.getCustomerId());
			psmt.setString(15, orderInfo.getStatus());

			psmt.setString(16, orderInfo.getVendorId());
			psmt.setString(17, orderInfo.getCard_type());
			psmt.setString(18, orderInfo.getOrder_type());

			psmt.setDate(19,java.sql.Date.valueOf(currdt));
			psmt.setLong(20, orderInfo.getStart_imsi_range());
			psmt.setLong(21, orderInfo.getEnd_imsi_range());

			if(psmt.executeUpdate() > 0) {
				result = "success";
			}	
		}catch (Exception e) {
			result="failed";
			LoggerExtension.log(3,"Exception while retrieving saveCustomerInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in saveCustomerInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"saveCustomerInfo query result: "+result);
		return result;
	}

	private static void getImsiRange(OrderInfo orderInfo) {
		// TODO Auto-generated method stub

		long quantity = Long.parseLong(orderInfo.getQuantity());;
		long Imsiprefix = Long.parseLong(orderInfo.getImsi_prefix()); //6 digit   123234
		long startImsi = Long.parseLong(orderInfo.getStart_imsi());  // 9 digit   000000001

		String startImsiStr = orderInfo.getImsi_prefix()+orderInfo.getStart_imsi();

		LoggerExtension.log(1, "startImsiStr:"+startImsiStr);
		long startImsiInt = Long.parseLong(startImsiStr);
		LoggerExtension.log(1, "startImsiInt:"+startImsiInt);
		long endImsirange = startImsiInt+quantity;
		LoggerExtension.log(1, "endImsirange:"+endImsirange);

		orderInfo.setStart_imsi_range(startImsiInt);
		orderInfo.setEnd_imsi_range(endImsirange-1);

	}
	public static OrderContainer getOrderList(OrderContainer orderContainer,SearchFilter searchFilter) throws InstantiationException, IllegalAccessException{
		OrderContainer container = new OrderContainer();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		//		"orderId", "quantity", "sim_prefix", "imsi_prefix", "sim_card_format","transport_key", "electrical_profile",
		//"carrier_name","brand","start_imsi","graphical_profile","network_element","status","deliveryDate"

		String term = searchFilter.getTerm();
		String propertyId = searchFilter.getPropertyId().toString();
		LoggerExtension.log(2,"Class "+className+"getOrderList propertyId: "+propertyId+" ,term:"+term);

		try {
			connection = ds.getConnection();
			String query = "SELECT * FROM ORDER_DETAILS where "+propertyId+"=?";
			LoggerExtension.log(2,"Class "+className+"getOrderList query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, term);
			rs = psmt.executeQuery();
			while(rs.next()){
				OrderInfo orderInfo = new OrderInfo();
				orderInfo.setOrderId(rs.getString("order_Id"));
				orderInfo.setCustomerId(rs.getString("customer_Id"));
				orderInfo.setVendorId(rs.getString("vendor_Id"));
				orderInfo.setQuantity(rs.getString("quantity"));
				orderInfo.setSim_prefix(rs.getString("sim_prefix"));
				orderInfo.setImsi_prefix(rs.getString("imsi_prefix"));
				orderInfo.setSim_card_format(rs.getString("sim_card_format"));
				orderInfo.setTransport_key(rs.getString("transport_key"));
				orderInfo.setElectrical_profile(rs.getString("electrical_profile"));
				orderInfo.setCarrier_name(rs.getString("carrier_name"));
				orderInfo.setBrand(rs.getString("brand"));
				orderInfo.setStart_imsi(rs.getString("start_imsi"));
				orderInfo.setGraphical_profile(rs.getString("graphical_profile"));
				orderInfo.setNetwork_element(rs.getString("network_element"));
				orderInfo.setStatus(rs.getString("status"));
				orderInfo.setExpectedDeliveryDate(rs.getDate("expectedDeliveryDate"));
				orderInfo.setCreationDate(rs.getDate("creationDate"));
				orderInfo.setModifyDate(rs.getDate("modifyDate"));
				orderInfo.setCard_type(rs.getString("card_type"));
				orderInfo.setOrder_type(rs.getString("order_type"));
				LoggerExtension.log(2,"Class "+className+"getOrderList OrderInfo: "+orderInfo.toString());

				container.addItem(orderInfo);
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return container;
	}

	public static OrderContainer getOrderList(OrderContainer orderContainer,String orderId) throws InstantiationException, IllegalAccessException{
		OrderContainer container = new OrderContainer();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		//order_id, customer_id, vendor_id, quantity, sim_prefix, imsi_prefix, start_sim, start_imsi, sim_card_format, transport_key, electrical_profile, 
		//carrier_name, brand, graphical_profile, network_element, card_type, order_type, expectedDeliveryDate, creationDate, modifyDate, status, start_imsi_range, end_imsi_range
		LoggerExtension.log(2,"Class "+className+"getOrderList orderId: "+orderId);

		try {
			connection = ds.getConnection();
			String query = "SELECT * FROM ORDER_DETAILS where order_id=?";
			LoggerExtension.log(2,"Class "+className+"getOrderList query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			rs = psmt.executeQuery();
			while(rs.next()){
				OrderInfo orderInfo = new OrderInfo();
				orderInfo.setOrderId(rs.getString("order_Id"));
				orderInfo.setCustomerId(rs.getString("customer_Id"));
				orderInfo.setVendorId(rs.getString("vendor_Id"));
				orderInfo.setQuantity(rs.getString("quantity"));
				orderInfo.setSim_prefix(rs.getString("sim_prefix"));
				orderInfo.setImsi_prefix(rs.getString("imsi_prefix"));
				orderInfo.setSim_card_format(rs.getString("sim_card_format"));
				orderInfo.setTransport_key(rs.getString("transport_key"));
				orderInfo.setElectrical_profile(rs.getString("electrical_profile"));
				orderInfo.setCarrier_name(rs.getString("carrier_name"));
				orderInfo.setBrand(rs.getString("brand"));
				orderInfo.setStart_imsi(rs.getString("start_imsi"));
				orderInfo.setStart_sim(rs.getString("start_sim"));
				orderInfo.setGraphical_profile(rs.getString("graphical_profile"));
				orderInfo.setNetwork_element(rs.getString("network_element"));
				orderInfo.setStatus(rs.getString("status"));
				orderInfo.setExpectedDeliveryDate(rs.getDate("expectedDeliveryDate"));
				orderInfo.setCreationDate(rs.getDate("creationDate"));
				orderInfo.setModifyDate(rs.getDate("modifyDate"));
				orderInfo.setCard_type(rs.getString("card_type"));
				orderInfo.setOrder_type(rs.getString("order_type"));

				orderInfo.setStart_imsi_range(rs.getLong("start_imsi_range"));
				orderInfo.setEnd_imsi_range(rs.getLong("end_imsi_range"));

				LoggerExtension.log(2,"Class "+className+"getOrderList OrderInfo: "+orderInfo.toString());

				container.addItem(orderInfo);
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return container;
	}

	public static List<VendorInfo> getVendorList(){
		/*
		List<VendorListInfo> list = new ArrayList<VendorListInfo>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;

		try {
			connection = ds.getConnection();
			String query = "SELECT * FROM vendor_list";
			LoggerExtension.log(2,"Class "+className+"getVendorList query: "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()){
				VendorListInfo vendorListInfo = new VendorListInfo();
				vendorListInfo.setVendorName(rs.getString("vendorName"));
				LoggerExtension.log(1, "vendor list:"+vendorListInfo.toString());
				list.add(vendorListInfo);
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(1, "vendor list:"+list.size());
		return list;
		 */

		List<VendorInfo> list = new ArrayList<VendorInfo>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			String query = "SELECT name,address,city,state,pincode,country,email,phone,vendor_id FROM VENDOR_INFO";
			LoggerExtension.log(2,"Class "+className+" getVendorList query : "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()) {
				VendorInfo customerInfo = new VendorInfo();

				customerInfo.setName(rs.getString("name"));
				customerInfo.setAddress(rs.getString("address"));
				customerInfo.setCity(rs.getString("city"));
				customerInfo.setState(rs.getString("state"));
				customerInfo.setPinCode(rs.getString("pincode"));
				customerInfo.setCountry(rs.getString("country"));
				customerInfo.setEmail(rs.getString("email"));
				customerInfo.setContactNumber(rs.getString("phone"));
				customerInfo.setVendorId(rs.getString("vendor_id"));

				LoggerExtension.log(2,"Class "+className+" getVendorList customerInfo: "+customerInfo);

				list.add(customerInfo);
			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting Role For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return list;


	}
	public static String getHashedPasswordForUser(String user) {
		String hashedPassword = null;
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			String query = "SELECT PASSWORD FROM SAC_USERS WHERE USER_NAME='"+user+"'";
			LoggerExtension.log(2,"Class "+className+"query : "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			if(rs.next()) {
				hashedPassword = rs.getString("PASSWORD");
			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting HashedPassword For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return hashedPassword;
	}

	public static UserData  getRoleForUser(String user) {
		UserData userData = new UserData();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			String query = "SELECT USER_NAME, USER_ROLE FROM SAC_USERS WHERE USER_NAME='"+user+"'";
			LoggerExtension.log(2,"Class "+className+"query : "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()) {
				userData.setUserName(rs.getString("USER_NAME"));
				userData.setUserRole(rs.getString("USER_ROLE"));
			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting Role For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return userData;
	}
	public static SimInfoContainer getImsiList(SimInfoContainer orderContainer,	SearchFilter searchFilter) throws InstantiationException, IllegalAccessException {
		SimInfoContainer container = new SimInfoContainer();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String term = searchFilter.getTerm();
		String propertyId = searchFilter.getPropertyId().toString();
		LoggerExtension.log(2,"Class "+className+"getImsiList propertyId: "+propertyId+" ,term:"+term);

		try {


			connection = ds.getConnection();
			String query = "SELECT * FROM SIM_INFO where "+propertyId+"=?";
			LoggerExtension.log(2,"Class "+className+"getImsiList query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, term);
			rs = psmt.executeQuery();
			while(rs.next()){
				SimInfo simInfo = new SimInfo();
				simInfo.setOrder_id(rs.getString("order_Id"));
				simInfo.setImsi(rs.getString("imsi"));
				simInfo.setSerial_number(rs.getString("serial_number"));
				simInfo.setPin1(rs.getString("pin1"));
				simInfo.setPuk1(rs.getString("puk1"));

				simInfo.setPin2(rs.getString("pin2"));
				simInfo.setPuk2(rs.getString("puk2"));
				simInfo.setAdm1(rs.getString("adm1"));
				simInfo.setKi(rs.getString("ki"));
				simInfo.setAccess_control(rs.getString("access_control"));
				simInfo.setMsisdn(rs.getString("msisdn"));
				simInfo.setStatus(rs.getString("status"));

				LoggerExtension.log(2,"Class "+className+"getImsiList simInfo: "+simInfo.toString());

				container.addItem(simInfo);
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getImsiList:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return container;
	}
	public static SimInfo getImsiList(SearchFilter searchFilter) throws InstantiationException, IllegalAccessException {
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String term = searchFilter.getTerm();
		String propertyId = searchFilter.getPropertyId().toString();
		LoggerExtension.log(2,"Class "+className+"getImsiList propertyId: "+propertyId+" ,term:"+term);
		SimInfo simInfo = new SimInfo();

		try {


			connection = ds.getConnection();
			String query = "SELECT * FROM SIM_INFO where "+propertyId+"=?";
			LoggerExtension.log(2,"Class "+className+"getImsiList query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, term);
			rs = psmt.executeQuery();
			while(rs.next()){
				simInfo.setOrder_id(rs.getString("order_Id"));
				simInfo.setImsi(rs.getString("imsi"));
				simInfo.setSerial_number(rs.getString("serial_number"));
				simInfo.setPin1(rs.getString("pin1"));
				simInfo.setPuk1(rs.getString("puk1"));

				simInfo.setPin2(rs.getString("pin2"));
				simInfo.setPuk2(rs.getString("puk2"));
				simInfo.setAdm1(rs.getString("adm1"));
				simInfo.setKi(rs.getString("ki"));
				simInfo.setAccess_control(rs.getString("access_control"));
				simInfo.setMsisdn(rs.getString("msisdn"));
				simInfo.setStatus(rs.getString("status"));

				LoggerExtension.log(2,"Class "+className+"getImsiList simInfo: "+simInfo.toString());
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getImsiList:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return simInfo;
	}
	public static String[] getImsiProvisionInfo(SearchFilter searchFilter) {
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo...");

		String[] imsiStatus={"0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String term = searchFilter.getTerm();
		String propertyId = searchFilter.getPropertyId().toString();
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo propertyId: "+propertyId+" ,term:"+term);
		try {
			connection = ds.getConnection();
			String query = "SELECT status,count(*) as count FROM sim_info where order_id=? group by status";
			LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, term);
			rs = psmt.executeQuery();
			while(rs.next()){
				String statusType = rs.getString("status");
				String count = rs.getString("count");
				if(statusType.equalsIgnoreCase("NotProvisioned")){
					imsiStatus[0]=count;
				}else if(statusType.equalsIgnoreCase("ProvsioningRaised")){
					imsiStatus[1]=count;
				}	
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getImsiProvisionInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: Assigned:"+imsiStatus[0]+" ,UnAssigned:"+imsiStatus[1]);
		return imsiStatus;
	}
	public static String doDeProvision(String colName, String colValue) {
		LoggerExtension.log(2,"Class "+className+"doDeProvision...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE SIM_INFO SET STATUS='UnAssigned' where "+colName+"=?";
			LoggerExtension.log(2,"Class "+className+"doDeProvision query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, colValue);
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving doDeProvision:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in doDeProvision :"+getStackTrace(e));
			}
		}
		return result;
	}
	public static String doProvision(String colName, String colValue) {
		LoggerExtension.log(2,"Class "+className+"doDeProvision...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE SIM_INFO SET STATUS='Assigned' where "+colName+"=?";
			LoggerExtension.log(2,"Class "+className+"doDeProvision query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, colValue);
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving doDeProvision:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in doDeProvision :"+getStackTrace(e));
			}
		}
		return result;
	}
	public static CustomerInfo getCustomerInfo(String custID) {
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		CustomerInfo customerInfo = new CustomerInfo();

		try {
			connection = ds.getConnection();
			String query = "SELECT name,address,city,state,pincode,country,email,phone,customer_id FROM CUSTOMER_INFO where customer_id=?";
			LoggerExtension.log(2,"Class "+className+" getCustomerList query : "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, custID);
			rs = psmt.executeQuery();
			while(rs.next()) {

				customerInfo.setName(rs.getString("name"));
				customerInfo.setAddress(rs.getString("address"));
				customerInfo.setCity(rs.getString("city"));
				customerInfo.setState(rs.getString("state"));
				customerInfo.setPinCode(rs.getString("pincode"));
				customerInfo.setCountry(rs.getString("country"));
				customerInfo.setEmail(rs.getString("email"));
				customerInfo.setPhoneNumber(rs.getString("phone"));
				customerInfo.setCustomerId(custID);

				LoggerExtension.log(2,"Class "+className+" getCustomerList customerInfo: "+customerInfo);


			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting Role For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return customerInfo;
	}
	public static String updateCustomerInfo(CustomerInfo customerInfo) {
		LoggerExtension.log(2,"Class "+className+"updateCustomerInfo..."+customerInfo);
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE customer_info SET name=?,address=?,city=?,state=?,pincode=?,email=?,country=?,phone=? where customer_id=?";
			LoggerExtension.log(2,"Class "+className+"updateCustomerInfo query: "+query+ "customerInfo.getCustomerId():"+customerInfo.getCustomerId());
			psmt = connection.prepareStatement(query);
			psmt.setString(1, customerInfo.getName());
			psmt.setString(2, customerInfo.getAddress());
			psmt.setString(3, customerInfo.getCity());
			psmt.setString(4, customerInfo.getState());
			psmt.setString(5, customerInfo.getPinCode());
			psmt.setString(6, customerInfo.getEmail());
			psmt.setString(7, customerInfo.getCountry());
			psmt.setString(8, customerInfo.getPhoneNumber());
			psmt.setString(9, customerInfo.getCustomerId());
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving updateCustomerInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateCustomerInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(3,"updateCustomerInfo result:"+result);

		return result;
	}
	public static VendorInfo getVendorInfo(String custID) {
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		VendorInfo customerInfo = new VendorInfo();

		try {
			connection = ds.getConnection();
			String query = "SELECT name,address,city,state,pincode,country,email,phone FROM VENDOR_INFO where vendor_id=?";
			LoggerExtension.log(2,"Class "+className+" getCustomerList query : "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, custID);
			rs = psmt.executeQuery();
			while(rs.next()) {

				customerInfo.setName(rs.getString("name"));
				customerInfo.setAddress(rs.getString("address"));
				customerInfo.setCity(rs.getString("city"));
				customerInfo.setState(rs.getString("state"));
				customerInfo.setPinCode(rs.getString("pincode"));
				customerInfo.setCountry(rs.getString("country"));
				customerInfo.setEmail(rs.getString("email"));
				customerInfo.setContactNumber(rs.getString("phone"));
				customerInfo.setVendorId(custID);

				LoggerExtension.log(2,"Class "+className+" getvendorList getVendorInfo: "+customerInfo);


			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting Role For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return customerInfo;
	}
	public static String updateVendorInfo(VendorInfo vendorInfo) {
		LoggerExtension.log(2,"Class "+className+"updateVendorInfo..."+vendorInfo);
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE vendor_info SET name=?,address=?,city=?,state=?,pincode=?,email=?,country=?,phone=? where vendor_id=?";
			LoggerExtension.log(2,"Class "+className+"updateVendorInfo query: "+query+ "customerInfo.getCustomerId():"+vendorInfo.getVendorId());
			psmt = connection.prepareStatement(query);
			psmt.setString(1, vendorInfo.getName());
			psmt.setString(2, vendorInfo.getAddress());
			psmt.setString(3, vendorInfo.getCity());
			psmt.setString(4, vendorInfo.getState());
			psmt.setString(5, vendorInfo.getPinCode());
			psmt.setString(6, vendorInfo.getEmail());
			psmt.setString(7, vendorInfo.getCountry());
			psmt.setString(8, vendorInfo.getContactNumber());
			psmt.setString(9, vendorInfo.getVendorId());
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving updateVendorInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateVendorInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(3,"updateVendorInfo result:"+result);

		return result;
	}
	public static String[] uploadResponseFileSimInfo(String orderId,String fileName, String filePathWithName,String fileType) {
		LoggerExtension.log(2,"Class "+className+"filePathWithName..."+filePathWithName+" orderId="+orderId);
		BufferedReader br = null;
		String newFilePath = filePathWithName.replace('\\', '/');
		String[] status ={"success","0","0","0","fileStatus"};
		String statusInsert="";
		StringBuffer sb = new StringBuffer();
		sb.append("q");
		String fileStatus="INVALID";
		try {
			String sCurrentLine;
			int totalOrderNoCount=0;
			int totalFailedCount=0;
			int totalSuccessCount=0;
			br = new BufferedReader(new FileReader(newFilePath));
			while ((sCurrentLine = br.readLine()) != null) {
				//364390001121447,8901391200270309986F,1111,92882773,2222,87097770,C8C3BF780AB1B0CD8E35E4CAA5E9707C,64713814,80
				//IMSI/Ser_nb/PIN1/PUK1/PIN2/PUK2/KI/ADM1/Access_Control :9 field
			//	LoggerExtension.log(1, sCurrentLine);
				StringTokenizer skt = new StringTokenizer(sCurrentLine, " ");
				String imsi="";
				String PIN1="";
				String PUK1="";
				String PIN2="";
				String PUK2="";
				String KI="";
				String Ser_nb="";
				String ADM1="";
				String Access_Control="";
				while (skt.hasMoreElements()) {
					//IMSI,Ser_nb,MSISDN ,Provsiosning Status
				//	LoggerExtension.log(1,"total count..."+skt.countTokens());
					if(skt.countTokens()==9){
						totalOrderNoCount=totalOrderNoCount+1;
						imsi = skt.nextElement().toString();
						Ser_nb = skt.nextElement().toString();
						PIN1 = skt.nextElement().toString();
						PUK1 = skt.nextElement().toString();
						PIN2 = skt.nextElement().toString();
						PUK2 = skt.nextElement().toString();
						KI = skt.nextElement().toString();
						ADM1 = skt.nextElement().toString();
						Access_Control = skt.nextElement().toString();
						fileStatus = "VALID";
					//	LoggerExtension.log(1, imsi+" ,"+Ser_nb+" ,"+PIN1+" ,"+PUK1+","+" ,"+PIN2+","+PUK2+" ,"+KI+","+ADM1+" ,"+Access_Control+ "fileStatus:"+fileStatus);
						statusInsert = insertResponseFileSimInfo(orderId,imsi,Ser_nb,PIN1,PUK1,PIN2,PUK2,KI,ADM1,Access_Control,fileType);	
						if(statusInsert.equalsIgnoreCase("success")){
							status[0]="SUCCESS";
							totalSuccessCount=totalSuccessCount+1;
						}else{
							totalFailedCount=totalFailedCount+1;
						}
						fileStatus = "VALID";

					}else{
						fileStatus = "INVALID";
						String s1 = skt.nextElement().toString();
						status[0]="FAILED";
						status[1]=""+totalOrderNoCount;
						status[2]=""+totalFailedCount;
						status[3]=""+totalSuccessCount;
				//		LoggerExtension.log(1,"Invalid File Entry...continue to read the file.."+fileStatus+" status::"+status[0]);

						//return status;
						break;
					}


				}
				
			}//end of out while

			status[1]=""+totalOrderNoCount;
			status[2]=""+totalFailedCount;
			status[3]=""+totalSuccessCount;
			LoggerExtension.log(1,"Uploaded file.........totalOrderNoCount::"+totalOrderNoCount+ " totalFailedCount:"+totalFailedCount+ " totalSuccessCount:"+totalSuccessCount);

		} catch (Exception e) {
			status[0]="FAILED";
			status[1]="0";
			status[2]="0";
			status[3]="0";
			e.printStackTrace();
			LoggerExtension.log(1, getStackTrace(e));
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return status;
		/*
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		String path  = filePathWithName.replace('\\','/' );
		try {
			connection = ds.getConnection();
			String query = "load data local infile '"+path+"' into table sim_info fields terminated by ',' enclosed by '\"' lines terminated by '\n' (imsi, serial_number, pin1,puk1,pin2,puk2,ki,adm1,access_control)";
		//	LoggerExtension.log(2,"Class "+className+"updateVendorInfo query: "+query+ "customerInfo.getCustomerId():"+vendorInfo.getVendorId());
			psmt = connection.prepareStatement(query);
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while loading data,in uploadResponseFileSimInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateVendorInfo :"+getStackTrace(e));
			}
		}

		 */


	}
	private static String insertResponseFileSimInfo(String orderId, String imsi,
			String ser_nb, String pIN1, String pUK1, String pIN2, String pUK2,
			String kI, String aDM1, String access_Control, String fileType) {
		//	LoggerExtension.log(2,"Class "+className+"insertResponseFileSimInfo query: ");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			//INSERT INTO CUSTOMER_INFO (name,address,city,state,pincode,country,email,phone,customer_id) VALUES('SATISH','SUS','PUNE','MAHARASTRA',41,'INIDA','SMAIL.COM',73,'sdff')
			String query = "INSERT INTO SIM_INFO (order_Id,imsi,serial_number,pIN1,pUK1,pIN2,pUK2,kI,aDM1,access_Control,card_type,status) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			//LoggerExtension.log(2,"Class "+className+"insertResponseFileSimInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			psmt.setString(2, imsi);
			psmt.setString(3, ser_nb);
			psmt.setString(4, pIN1);
			psmt.setString(5, pUK1);
			psmt.setString(6, pIN2);
			psmt.setString(7, pUK2);
			psmt.setString(8, kI);
			psmt.setString(9, aDM1);
			psmt.setString(10, access_Control);
			psmt.setString(11, fileType);
			psmt.setString(12, "NotProvisioned");
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}	
		}catch (Exception e) {
			if(getStackTrace(e).contains("Duplicate entry")){
				//count++;
			}

			LoggerExtension.log(3,"Exception while retrieving insertResponseFileSimInfo:"+getStackTrace(e).contains("Duplicate entry"));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in saveCustomerInfo :"+getStackTrace(e));
			}
		}
		//LoggerExtension.log(2,"Class "+className+"insertResponseFileSimInfo query result: "+result);
		return result;
	}
	public static String[] getImsiProvisionInfo(String orderId, String cardType) {
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo...");

		String[] imsiStatus={"0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo  orderId:"+orderId);
		try {
			connection = ds.getConnection();
			String query = "SELECT status,count(*) as count FROM sim_info where order_id=? and card_type=? group by status";
			LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			psmt.setString(2, cardType);
			rs = psmt.executeQuery();
			while(rs.next()){
				String statusType = rs.getString("status");
				String count = rs.getString("count");
				if(statusType.equalsIgnoreCase("NotProvisioned")){
					imsiStatus[0]=count;
				}else if(statusType.equalsIgnoreCase("ProvsioningRaised")){
					imsiStatus[1]=count;
				}	
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getImsiProvisionInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: Assigned:"+imsiStatus[0]+" ,UnAssigned:"+imsiStatus[1]);
		return imsiStatus;
	}
	public static int getOrderIdCount(String orderId, String fileType) {
		LoggerExtension.log(2,"Class "+className+"getOrderIdCount...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		int orderIdCount=0; 
		LoggerExtension.log(2,"Class "+className+"getOrderIdCount  orderId:"+orderId);
		try {
			connection = ds.getConnection();
			String query = "SELECT count(*) as count FROM sim_info where order_id=? and status=? and card_type=?";
			LoggerExtension.log(2,"Class "+className+"getOrderIdCount query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			psmt.setString(2, "NotProvisioned");
			psmt.setString(3, fileType);
			rs = psmt.executeQuery();
			while(rs.next()){
				orderIdCount = rs.getInt("count");
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getOrderIdCount:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getOrderIdCount :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getOrderIdCount query: UnAssigned:"+orderIdCount);
		return orderIdCount;
	}
	public static List<String> getMsisdnList(int orderIdCount) {

		LoggerExtension.log(2,"Class "+className+"getMsisdnList...");
		List<String> msisdnList = new ArrayList<String>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		LoggerExtension.log(2,"Class "+className+"getMsisdnList  orderId:"+orderIdCount);
		try {
			connection = ds.getConnection();
			String query = "SELECT msisdn FROM msisdn_master where status=? limit "+orderIdCount+"";
			LoggerExtension.log(2,"Class "+className+"getMsisdnList query: "+query);
			psmt = connection.prepareStatement(query);

			psmt.setString(1, "UnAssigned");

			rs = psmt.executeQuery();
			while(rs.next()){
				msisdnList.add(rs.getString("msisdn"));
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getOrderIdCount:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getOrderIdCount :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getOrderIdCount query: msisdnList:"+msisdnList);
		return msisdnList;

	}
	public static String assignMsisdnToIMSI(String orderId,String msisdn, String fileType) {
		LoggerExtension.log(2,"Class "+className+"assignMsisdnToIMSI...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE SIM_INFO SET msisdn=?, STATUS='ProvsioningRaised' where order_id=? and status=? and card_type=? limit 1";
			LoggerExtension.log(2,"Class "+className+"assignMsisdnToIMSI query: "+query);
			LoggerExtension.log(2,"Class "+className+"assignMsisdnToIMSI msisdnList size is: "+msisdn);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, msisdn);
			psmt.setString(2, orderId);
			psmt.setString(3, "NotProvisioned");
			psmt.setString(4, fileType);

			if(psmt.executeUpdate() > 0) {
				result = "success";
			}

		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving assignMsisdnToIMSI:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in assignMsisdnToIMSI :"+getStackTrace(e));
			}
		}
		return result;
	}

	public static String exportSimInfoForPrepaid(String orderID, String fileType, String prefilePathToExport){
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result = "success";
		LoggerExtension.log(1,"exportSimInfoForPrepaid filePathToExport::"+ prefilePathToExport);
		try {
			connection = ds.getConnection();
			String query = "SELECT imsi,serial_number,msisdn into OUTFILE  '"+prefilePathToExport+"' FIELDS TERMINATED BY ',' FROM sim_info where order_id=? and status=? and card_type=?";
			LoggerExtension.log(2,"Class "+className+"exportSimInfoForPrepaid query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderID);
			psmt.setString(2, "ProvsioningRaised");
			psmt.setString(3, fileType);
			psmt.executeQuery();

		}catch (Exception e) {
			result = "failed";
			LoggerExtension.log(3,"Exception while retrieving getImsiList:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return result;
	}
	public static String exportSimInfoForPostpaid(String orderID,String c,String filePathToExport ){
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="succes";
		LoggerExtension.log(1,"exportSimInfoForPostpaid filePathToExport::"+ filePathToExport);
		try {
			connection = ds.getConnection();
			String query = "SELECT imsi,serial_number into OUTFILE  '"+filePathToExport+"' FIELDS TERMINATED BY ',' FROM sim_info where order_id=? and status=? and card_type=?";
			LoggerExtension.log(2,"Class "+className+"getImsiList query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderID);
			psmt.setString(2, "ProvsioningRaised");
			psmt.setString(3, c);
			psmt.executeQuery();

		}catch (Exception e) {
			result="failed";
			LoggerExtension.log(3,"Exception while retrieving getImsiList:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return result;
	}
	public static String[] getAttachMSISDNReport(String orderId, String cardType) {
		LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport...");

		String[] imsiStatus={"0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport  orderId:"+orderId);
		try {
			connection = ds.getConnection();
			String query = "SELECT status,count(*) as count FROM sim_info where order_id=? and card_type=? group by status";
			LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			psmt.setString(2, cardType);
			rs = psmt.executeQuery();
			while(rs.next()){
				String statusType = rs.getString("status");
				String count = rs.getString("count");
				if(statusType.equalsIgnoreCase("ProvsioningRaised")){
					imsiStatus[0]=count;
				}else if(statusType.equalsIgnoreCase("ProvisioningDone")){
					imsiStatus[1]=count;
				}	
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getAttachMSISDNReport:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: Assigned:"+imsiStatus[0]+" ,UnAssigned:"+imsiStatus[1]);
		return imsiStatus;
	}
	public static String updateSimInfoTableForFinalProvisioning(String orderId,String filename, String filepathwithname, String card_type) {

		BufferedReader br = null;
		String result="success";
		String newFilePath = filepathwithname.replace('\\', '/');
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(newFilePath));
			while ((sCurrentLine = br.readLine()) != null) {
				//IMSI,Ser_nb,MSISDN ,Provsiosning Status
			//	LoggerExtension.log(1, sCurrentLine);
				StringTokenizer skt = new StringTokenizer(sCurrentLine, ",;");
				String imsi="";
				String msisdn="";
				String Ser_nb="";
				String provisioningState="";
				while (skt.hasMoreElements()) {
					//IMSI,Ser_nb,MSISDN ,Provsiosning Status
					if(skt.countTokens()==4){
						imsi = skt.nextElement().toString();
						Ser_nb = skt.nextElement().toString();
						msisdn = skt.nextElement().toString();
						provisioningState = skt.nextElement().toString();	
					}else{
					//	LoggerExtension.log(1, "Invalid File not 4 coloumn are present in file..");
						return "failed";
					}
				}
				updateSimInfoTableStatus(orderId,imsi,msisdn,provisioningState,card_type);
			}

		} catch (IOException e) {
			result="failed";
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	private static void updateSimInfoTableStatus(String orderID,String imsi, String msisdn,String provisioningState, String card_type) {
		LoggerExtension.log(2,"Class "+className+"updateSimInfoTableStatus...orderID="+orderID+" ,card_type="+card_type+" ,msisdn="+msisdn+" imsi="+imsi);
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();

			String query;

			if(provisioningState.equalsIgnoreCase("yes")){
				query = "UPDATE SIM_INFO SET STATUS='ProvisioningDone' where order_id=? and imsi=? and msisdn=? and card_type=?";
			}else{
				//	query = "UPDATE SIM_INFO SET STATUS='ProvsioningRaised' where order_id=? and imsi=? and msisdn=? and card_type=?";	
				LoggerExtension.log(2,"Class "+className+"updateSimInfoTableStatus...provisioningState is NO so return...provisioningState::"+provisioningState);
				return;
			}

			LoggerExtension.log(2,"Class "+className+"updateSimInfoTableStatus query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderID);
			psmt.setString(2, imsi);
			psmt.setString(3, msisdn);
			psmt.setString(4, card_type);
			if(psmt.executeUpdate() > 0) {
				LoggerExtension.log(2,"Class "+className+"updateSimInfoTableStatus success....");
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving updateSimInfoTableStatus:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateSimInfoTableStatus :"+getStackTrace(e));
			}
		}
	}
	public static void updateOrderStatus(String orderId){

		LoggerExtension.log(2,"Class "+className+"updateOrderStatus...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE ORDER_DETAILS SET STATUS='ORDER_SUBMITTED' where order_id=?";
			LoggerExtension.log(2,"Class "+className+"updateOrderStatus query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving updateOrderStatus:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateOrderStatus :"+getStackTrace(e));
			}
		}

	}

	public static void updateOrderId(String orderId, String cardType) {
		LoggerExtension.log(2,"Class "+className+"updateOrderId...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE SIM_INFO SET order_id=?,STATUS='NotProvisioned',card_type=? where order_id=?";
			LoggerExtension.log(2,"Class "+className+"updateOrderId query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			psmt.setString(2, cardType);
			psmt.setString(3, "0");
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving updateOrderId:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateSimInfoTableStatus :"+getStackTrace(e));
			}
		}
	}
	public static void updateMsisdnMasterTable(String orderId, String fileType,	List<String> msisdnList) {
		LoggerExtension.log(2,"Class "+className+"updateMsisdnMasterTable...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE msisdn_master SET orderid=?,STATUS='Assigned', imsi=(select imsi from sim_info where msisdn=? and order_id='"+orderId+"') where msisdn=?";
			LoggerExtension.log(2,"Class "+className+"updateMsisdnMasterTable query: "+query);

			for (int i = 0; i < msisdnList.size(); i++) {
				LoggerExtension.log(2,"Class "+className+"updateMsisdnMasterTable msisdn: "+ msisdnList.get(i));
				psmt = connection.prepareStatement(query);
				psmt.setString(1, orderId);
				psmt.setString(2, msisdnList.get(i));
				psmt.setString(3, msisdnList.get(i));
				if(psmt.executeUpdate() > 0) {
					result = "success";
				}

			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving updateMsisdnMasterTable:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateSimInfoTableStatus :"+getStackTrace(e));
			}
		}

		// TODO Auto-generated method stub

	}
	public static String[] getOrderIdStatusDetails(String orderId) {
		LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport...");

		String[] imsiStatus={"0","0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport  orderId:"+orderId);
		try {
			connection = ds.getConnection();
			String query = "SELECT status,count(*) as count FROM sim_info where order_id=? group by status";
			LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			rs = psmt.executeQuery();
			while(rs.next()){
				String statusType = rs.getString("status");
				String count = rs.getString("count");
				if(statusType.equalsIgnoreCase("NotProvisioned")){
					imsiStatus[0]=count;
				}else if(statusType.equalsIgnoreCase("ProvsioningRaised")){
					imsiStatus[1]=count;
				}else if(statusType.equalsIgnoreCase("ProvisioningDone")){
					imsiStatus[2]=count;
				}	
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getAttachMSISDNReport:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: NotProvisioned:"+imsiStatus[0]+" ,ProvsioningRaised:"+imsiStatus[1]+" ProvisioningDone"+imsiStatus[2]);
		return imsiStatus;
	}


	public static String[] getOrderIdCardTypeDetails(String orderId) {
		LoggerExtension.log(2,"Class "+className+"getOrderIdCardTypeDetails...");

		String[] imsiStatus={"0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		LoggerExtension.log(2,"Class "+className+"getOrderIdCardTypeDetails  orderId:"+orderId);
		try {
			connection = ds.getConnection();
			String query = "SELECT card_type,count(*) as count FROM sim_info where order_id=? group by card_type";
			LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			rs = psmt.executeQuery();
			while(rs.next()){
				String statusType = rs.getString("card_type");
				String count = rs.getString("count");
				if(statusType.equalsIgnoreCase("prepaid")){
					imsiStatus[0]=count;
				}else if(statusType.equalsIgnoreCase("postpaid")){
					imsiStatus[1]=count;
				}	
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getAttachMSISDNReport:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: Prepaid:"+imsiStatus[0]+" ,Postpaid:"+imsiStatus[1]);
		return imsiStatus;
	}
	public static SimInfo getIMSIDetails(String imsi) {

		LoggerExtension.log(2,"Class "+className+"getIMSIDetails..."+imsi);
		String[] imsiStatus={"0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		SimInfo simInfo = new SimInfo();
		LoggerExtension.log(2,"Class "+className+"getOrderIdCardTypeDetails  imsi:"+imsi);
		try {
			connection = ds.getConnection();
			String query = "SELECT order_id,msisdn,serial_number,card_type,status FROM sim_info where imsi=?";
			LoggerExtension.log(2,"Class "+className+"getIMSIDetails query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, imsi);
			rs = psmt.executeQuery();
			while(rs.next()){
				String order_id = rs.getString("order_id");
				simInfo.setOrder_id(order_id);
				String msisdn = rs.getString("msisdn");
				simInfo.setMsisdn(msisdn);
				String serial_number = rs.getString("serial_number");
				simInfo.setSerial_number(serial_number);
				String card_type = rs.getString("card_type");
				simInfo.setCard_type(card_type);
				String status = rs.getString("status");
				simInfo.setStatus(status);
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getIMSIDetails:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getIMSIDetails query: simInfo:"+simInfo.toString());
		return simInfo;
	}
	public static String[] getSimReportDetails() {
		//total cnt
		//pre
		//post
		//prov not
		//prov done
		//prov raised


		LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport...");
		String[] imsiStatus={"0","0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			String query = "SELECT status,count(*) as count FROM sim_info group by status";
			LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport query: "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()){
				String statusType = rs.getString("status");
				String count = rs.getString("count");
				if(statusType.equalsIgnoreCase("NotProvisioned")){
					imsiStatus[0]=count;
				}else if(statusType.equalsIgnoreCase("ProvsioningRaised")){
					imsiStatus[1]=count;
				}else if(statusType.equalsIgnoreCase("ProvisioningDone")){
					imsiStatus[2]=count;
				}	
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getAttachMSISDNReport:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: Assigned:"+imsiStatus[0]+" ,UnAssigned:"+imsiStatus[1]);
		return imsiStatus;

	}
	public static String[] getSimReporCardTypetDetails() {
		LoggerExtension.log(2,"Class "+className+"getOrderIdCardTypeDetails...");

		String[] imsiStatus={"0","0"};
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			String query = "SELECT card_type,count(*) as count FROM sim_info group by card_type";
			LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport query: "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()){
				String statusType = rs.getString("card_type");
				String count = rs.getString("count");
				if(statusType.equalsIgnoreCase("prepaid")){
					imsiStatus[0]=count;
				}else if(statusType.equalsIgnoreCase("postpaid")){
					imsiStatus[1]=count;
				}	
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getAttachMSISDNReport:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		LoggerExtension.log(2,"Class "+className+"getImsiProvisionInfo query: Prepaid:"+imsiStatus[0]+" ,Postpaid:"+imsiStatus[1]);
		return imsiStatus;
	}
	public static ImsiMappingInfo getMSISDNDetails(String msisdn) {
		ImsiMappingInfo imsiMappingInfo = new ImsiMappingInfo();
		imsiMappingInfo.setMsisdn(msisdn);
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			String query = "SELECT imsi,card_type,status FROM sim_info where msisdn=?";
			LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, msisdn);
			rs = psmt.executeQuery();
			while(rs.next()){
				imsiMappingInfo.setImsi(rs.getString("imsi"));
				imsiMappingInfo.setStatus(rs.getString("status"));
				imsiMappingInfo.setCard_type(rs.getString("card_type"));
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getAttachMSISDNReport:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}

		}
		return imsiMappingInfo;

	}

	//	public static String getNewImsiDetails(String newimsi) {
	//		Connection connection = null;
	//		PreparedStatement psmt = null;
	//		ResultSet rs = null;
	//		String newImsi="";
	//		try {
	//			connection = ds.getConnection();
	//			String query = "SELECT imsi FROM sim_info where newimsi=? and status=?";
	//			LoggerExtension.log(2,"Class "+className+"getAttachMSISDNReport query: "+query);
	//			psmt = connection.prepareStatement(query);
	//			psmt.setString(1, newimsi);
	//			psmt.setString(2, "NotProvisioned");
	//			rs = psmt.executeQuery();
	//			while(rs.next()){
	//				newImsi = (rs.getString("imsi"));
	//			}
	//		}catch (Exception e) {
	//			LoggerExtension.log(3,"Exception while retrieving getAttachMSISDNReport:"+getStackTrace(e));
	//		}finally{
	//			try{
	//				if(rs!=null){
	//					rs.close();
	//				}
	//				if(psmt!=null){
	//					psmt.close();
	//				}
	//				if(connection!=null){
	//					releaseConnection(connection);
	//				}
	//			}catch(Exception e){
	//				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
	//			}
	//		
	//		}
	//		return newImsi;
	//
	//	}
	public static void mappedNewImsiToMSISDN(ImsiMappingInfo imsiMappingInfo2,	String newIMSI2) {
		LoggerExtension.log(2,"Class "+className+"mappedNewImsiToMSISDN...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE SIM_INFO SET msisdn=?,STATUS='NEWSIM' where imsi=?";
			LoggerExtension.log(2,"Class "+className+"mappedNewImsiToMSISDN query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1,imsiMappingInfo2.getMsisdn() );
			psmt.setString(2,  newIMSI2);
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving mappedNewImsiToMSISDN:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in mappedNewImsiToMSISDN :"+getStackTrace(e));
			}
		}
	}
	public static void updateOldImsiRecord(ImsiMappingInfo imsiMappingInfo2) {
		LoggerExtension.log(2,"Class "+className+"updateOldImsiRecord...");
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		String result="failed";
		try {
			connection = ds.getConnection();
			String query = "UPDATE SIM_INFO SET msisdn=?,STATUS=? where imsi=?";
			LoggerExtension.log(2,"Class "+className+"updateOldImsiRecord query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1,"");
			psmt.setString(2,"NotProvisioned");
			psmt.setString(3, imsiMappingInfo2.getImsi() );
			if(psmt.executeUpdate() > 0) {
				result = "success";
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving updateOldImsiRecord:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in updateOldImsiRecord :"+getStackTrace(e));
			}
		}
	}
	public static List<OrderInfo> getOrderListFromOrder_Details_Table() {
		List<OrderInfo> list = new ArrayList<OrderInfo>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			//order_id, customer_id, vendor_id, quantity, sim_prefix, imsi_prefix, start_sim, start_imsi, 
			//sim_card_format, transport_key, electrical_profile, carrier_name, brand, graphical_profile,
			//network_element, card_type, order_type, expectedDeliveryDate, creationDate, modifyDate, status, start_imsi_range, end_imsi_range
			String query = "SELECT order_id, quantity, sim_prefix, imsi_prefix, start_sim, start_imsi," +
					"  expectedDeliveryDate, creationDate,status FROM order_details where status=?";
			LoggerExtension.log(2,"Class "+className+" getOrderListFromOrder_Details_Table query : "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, "ORDER_SUBMITTED");
			rs = psmt.executeQuery();
			while(rs.next()) {
				OrderInfo customerInfo = new OrderInfo();

				customerInfo.setOrderId(rs.getString("order_id"));
				customerInfo.setQuantity(rs.getString("quantity"));
				customerInfo.setSim_prefix(rs.getString("sim_prefix"));
				customerInfo.setImsi_prefix(rs.getString("imsi_prefix"));
				customerInfo.setStart_sim(rs.getString("start_sim"));
				customerInfo.setStart_imsi(rs.getString("start_imsi"));
				customerInfo.setExpectedDeliveryDate(rs.getDate("expectedDeliveryDate"));
				customerInfo.setCreationDate(rs.getDate("creationDate"));
				customerInfo.setStatus(rs.getString("status"));

				LoggerExtension.log(2,"Class "+className+" getCustomerList OrderInfo: "+customerInfo);

				list.add(customerInfo);
			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting Role For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return list;
	}

	public static List<OrderInfo> getOrderListofALLStatusFromOrderDetails() {
		List<OrderInfo> list = new ArrayList<OrderInfo>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			connection = ds.getConnection();
			//order_id, customer_id, vendor_id, quantity, sim_prefix, imsi_prefix, start_sim, start_imsi, 
			//sim_card_format, transport_key, electrical_profile, carrier_name, brand, graphical_profile,
			//network_element, card_type, order_type, expectedDeliveryDate, creationDate, modifyDate, status, start_imsi_range, end_imsi_range
			String query = "SELECT order_id, quantity, sim_prefix, imsi_prefix, start_sim, start_imsi," +
					"  expectedDeliveryDate, creationDate,status FROM order_details";
			LoggerExtension.log(2,"Class "+className+" getOrderListFromOrder_Details_Table query : "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()) {
				OrderInfo customerInfo = new OrderInfo();

				customerInfo.setOrderId(rs.getString("order_id"));
				customerInfo.setQuantity(rs.getString("quantity"));
				customerInfo.setSim_prefix(rs.getString("sim_prefix"));
				customerInfo.setImsi_prefix(rs.getString("imsi_prefix"));
				customerInfo.setStart_sim(rs.getString("start_sim"));
				customerInfo.setStart_imsi(rs.getString("start_imsi"));
				customerInfo.setExpectedDeliveryDate(rs.getDate("expectedDeliveryDate"));
				customerInfo.setCreationDate(rs.getDate("creationDate"));
				customerInfo.setStatus(rs.getString("status"));

				LoggerExtension.log(2,"Class "+className+" getCustomerList OrderInfo: "+customerInfo);

				list.add(customerInfo);
			}
		}catch (SQLException e) {
			LoggerExtension.log(3,"Exception while getting Role For User :"+e.getMessage());
		}finally{
			try{
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection :"+e.getMessage());
			}
		}
		return list;
	}

	public static List<OrderInfo> getIMSIRangeFrom_OrderTable() {
		List<OrderInfo> list = new ArrayList<OrderInfo>();
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		//		"orderId", "quantity", "sim_prefix", "imsi_prefix", "sim_card_format","transport_key", "electrical_profile",
		//"carrier_name","brand","start_imsi","graphical_profile","network_element","status","deliveryDate"

		LoggerExtension.log(2,"Class "+className+"getIMSIRangeFrom_OrderTable.... ");

		try {
			connection = ds.getConnection();
			String query = "SELECT * FROM ORDER_DETAILS  order by creationDate desc limit 5";
			LoggerExtension.log(2,"Class "+className+"getIMSIRangeFrom_OrderTable query: "+query);
			psmt = connection.prepareStatement(query);
			//psmt.setString(1, orderId);
			rs = psmt.executeQuery();
			while(rs.next()){
				OrderInfo orderInfo = new OrderInfo();
				orderInfo.setOrderId(rs.getString("order_Id"));
				orderInfo.setCustomerId(rs.getString("customer_Id"));
				orderInfo.setVendorId(rs.getString("vendor_Id"));
				orderInfo.setQuantity(rs.getString("quantity"));
				orderInfo.setSim_prefix(rs.getString("sim_prefix"));
				orderInfo.setImsi_prefix(rs.getString("imsi_prefix"));
				orderInfo.setSim_card_format(rs.getString("sim_card_format"));
				orderInfo.setTransport_key(rs.getString("transport_key"));
				orderInfo.setElectrical_profile(rs.getString("electrical_profile"));
				orderInfo.setCarrier_name(rs.getString("carrier_name"));
				orderInfo.setBrand(rs.getString("brand"));
				orderInfo.setStart_imsi(rs.getString("start_imsi"));
				orderInfo.setGraphical_profile(rs.getString("graphical_profile"));
				orderInfo.setNetwork_element(rs.getString("network_element"));
				orderInfo.setStatus(rs.getString("status"));
				orderInfo.setExpectedDeliveryDate(rs.getDate("expectedDeliveryDate"));
				orderInfo.setCreationDate(rs.getDate("creationDate"));
				orderInfo.setModifyDate(rs.getDate("modifyDate"));
				orderInfo.setCard_type(rs.getString("card_type"));
				orderInfo.setOrder_type(rs.getString("order_type"));

				orderInfo.setStart_imsi_range(rs.getLong("start_imsi_range"));
				orderInfo.setEnd_imsi_range(rs.getLong("end_imsi_range"));

				LoggerExtension.log(2,"Class "+className+"getOrderList OrderInfo: "+orderInfo.toString());

				list.add(orderInfo);
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return list;
	}
	public static OrderInfo getOrderTypeFromOrderDetails(String orderId) {
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		//		"orderId", "quantity", "sim_prefix", "imsi_prefix", "sim_card_format","transport_key", "electrical_profile",
		//"carrier_name","brand","start_imsi","graphical_profile","network_element","status","deliveryDate"
		OrderInfo orderInfo = new OrderInfo();

		LoggerExtension.log(2,"Class "+className+"getOrderTypeFromOrderDetails.... ");

		try {
			connection = ds.getConnection();
			String query = "SELECT * FROM ORDER_DETAILS  where order_id=?";
			LoggerExtension.log(2,"Class "+className+"getOrderTypeFromOrderDetails query: "+query);
			psmt = connection.prepareStatement(query);
			psmt.setString(1, orderId);
			rs = psmt.executeQuery();
			while(rs.next()){
				orderInfo.setOrderId(rs.getString("order_Id"));
				orderInfo.setCustomerId(rs.getString("customer_Id"));
				orderInfo.setVendorId(rs.getString("vendor_Id"));
				orderInfo.setQuantity(rs.getString("quantity"));
				orderInfo.setSim_prefix(rs.getString("sim_prefix"));
				orderInfo.setImsi_prefix(rs.getString("imsi_prefix"));
				orderInfo.setSim_card_format(rs.getString("sim_card_format"));
				orderInfo.setTransport_key(rs.getString("transport_key"));
				orderInfo.setElectrical_profile(rs.getString("electrical_profile"));
				orderInfo.setCarrier_name(rs.getString("carrier_name"));
				orderInfo.setBrand(rs.getString("brand"));
				orderInfo.setStart_imsi(rs.getString("start_imsi"));
				orderInfo.setGraphical_profile(rs.getString("graphical_profile"));
				orderInfo.setNetwork_element(rs.getString("network_element"));
				orderInfo.setStatus(rs.getString("status"));
				orderInfo.setExpectedDeliveryDate(rs.getDate("expectedDeliveryDate"));
				orderInfo.setCreationDate(rs.getDate("creationDate"));
				orderInfo.setModifyDate(rs.getDate("modifyDate"));
				orderInfo.setCard_type(rs.getString("card_type"));
				orderInfo.setOrder_type(rs.getString("order_type"));

				orderInfo.setStart_imsi_range(rs.getLong("start_imsi_range"));
				orderInfo.setEnd_imsi_range(rs.getLong("end_imsi_range"));

			//	LoggerExtension.log(2,"Class "+className+"getOrderTypeFromOrderDetails OrderInfo: "+orderInfo.toString());


			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return orderInfo;
	}
	public static OrderInfo getIMSIValidationInfo(String imsiPrefix, String imsiStart) {
		Connection connection = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		//		"orderId", "quantity", "sim_prefix", "imsi_prefix", "sim_card_format","transport_key", "electrical_profile",
		//"carrier_name","brand","start_imsi","graphical_profile","network_element","status","deliveryDate"
		OrderInfo orderInfo=null;

		String imsiStr = imsiPrefix+imsiStart;
		LoggerExtension.log(2,"Class "+className+"getIMSIValidationInfo.... imsiStr::"+imsiStr);

		try {
			connection = ds.getConnection();
			String query = "SELECT order_Id,start_imsi_range,end_imsi_range,status,creationDate FROM ORDER_DETAILS  WHERE "+imsiStr+" BETWEEN start_imsi_range and end_imsi_range";
			LoggerExtension.log(2,"Class "+className+"getIMSIValidationInfo query: "+query);
			psmt = connection.prepareStatement(query);
			rs = psmt.executeQuery();
			while(rs.next()){
				orderInfo = new OrderInfo();
				orderInfo.setOrderId(rs.getString("order_Id"));
				orderInfo.setStatus(rs.getString("status"));
				orderInfo.setCreationDate(rs.getDate("creationDate"));
				orderInfo.setStart_imsi_range(rs.getLong("start_imsi_range"));
				orderInfo.setEnd_imsi_range(rs.getLong("end_imsi_range"));
				LoggerExtension.log(2,"Class "+className+"getIMSIValidationInfo OrderInfo: "+orderInfo.toString());
			}
		}catch (Exception e) {
			LoggerExtension.log(3,"Exception while retrieving getContestInfo:"+getStackTrace(e));
		}finally{
			try{
				if(rs!=null){
					rs.close();
				}
				if(psmt!=null){
					psmt.close();
				}
				if(connection!=null){
					releaseConnection(connection);
				}
			}catch(Exception e){
				LoggerExtension.log(3,"Exception while closing connection in getContestInfo :"+getStackTrace(e));
			}
		}
		return orderInfo;
	}
	public static boolean checkOrderQuantity(String orderId,String fileName, String filePathWithName,String fileType, int orderQuantity) {
		LoggerExtension.log(2,"Class "+className+"checkOrderQuantity..."+filePathWithName+" orderId="+orderId);
		BufferedReader br = null;
		String newFilePath = filePathWithName.replace('\\', '/');
		String sCurrentLine;
		int totalOrderNoCount=0;
		boolean flag=false;
		try {

			br = new BufferedReader(new FileReader(newFilePath));
			while ((sCurrentLine = br.readLine()) != null) {
				StringTokenizer skt = new StringTokenizer(sCurrentLine," ");
				//System.out.println( sCurrentLine);
			//	LoggerExtension.log(2,"Class "+className+"checkOrderQuantity..."+sCurrentLine);
				String imsi="";
				String PIN1="";
				String PUK1="";
				String PIN2="";
				String PUK2="";
				String KI="";
				String Ser_nb="";
				String ADM1="";
				String Access_Control="";
				while (skt.hasMoreElements()) {
					//IMSI,Ser_nb,MSISDN ,Provsiosning Status
					//	System.out.println("total count..."+skt.countTokens());
					if(skt.countTokens()==9){
				//		LoggerExtension.log(2,"Class "+className+"checkOrderQuantity...skt.countTokens()==9 totalOrderNoCount::"+totalOrderNoCount);
						totalOrderNoCount=totalOrderNoCount+1;
						
						imsi = skt.nextElement().toString();
						Ser_nb = skt.nextElement().toString();
						PIN1 = skt.nextElement().toString();
						PUK1 = skt.nextElement().toString();
						PIN2 = skt.nextElement().toString();
						PUK2 = skt.nextElement().toString();
						KI = skt.nextElement().toString();
						ADM1 = skt.nextElement().toString();
						Access_Control = skt.nextElement().toString();

					}else{
							//return status;
					//	LoggerExtension.log(2,"Class "+className+"checkOrderQuantity...skt.countTokens()#9 totalOrderNoCount::"+totalOrderNoCount);
						
						break;
					}
				}
			}
			
			if(totalOrderNoCount==orderQuantity){
				flag = true;
			}else{
				flag =false;
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			LoggerExtension.log(1, getStackTrace(e));
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		LoggerExtension.log(1, "Check totalOrderNoCount="+totalOrderNoCount+" orderQuantity="+orderQuantity+" CheckValidation of File:"+flag);
		return flag;
	}
}
