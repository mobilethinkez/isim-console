package com.mtz.isim.application.util;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



/**
 * 
 * @author satish.yadav
 *
 */
public class LoggerExtension {
  public static Logger genLogger = Logger.getLogger("GenLogger");
  
  /**
   * 
   * @param loglevel
   * @param msg
   */
  public static void log(int loglevel, String msg) {
	 // genLogger.info(msg);
	  if(genLogger.isDebugEnabled() && loglevel==1) {
 			genLogger.debug(msg);
	  }else if(genLogger.isInfoEnabled() && loglevel==2) {
   			genLogger.info(msg);
	  }else if(genLogger.isTraceEnabled()&&loglevel==3) {
   			genLogger.trace(msg);
	  }else{
		  	genLogger.error(msg);
	  }
  } // End of log method
  
  public static void info(String className, String msg) {
		 if(genLogger.isInfoEnabled()) {
	   			genLogger.info(className+"::"+msg);
		  }
  } // End of log method
	  
  public static void debug(String className, String msg) {
		 if(genLogger.isDebugEnabled()) {
	   			genLogger.debug(className+"::"+msg);
		  }
  } // End of log method
  public static void error(String className, String msg) {
		 if(genLogger.isDebugEnabled()) {
	   			genLogger.error(className+"::"+msg);
		  }
  } 
  public static void trace(String className,String msg) {
		 if(genLogger.isDebugEnabled()) {
	   			genLogger.trace(className+"::"+msg);
		  }
  } 
  
  
  public LoggerExtension() {
  }
  
 	 
  /**
   *init method : used to initialize LoggerExtension instance.
   */
  public static void init() 
  {
	  try
	  {
		  PropertyManager.getPropertyManager();
		  String log4jprops = PropertyManager.getProperty("LOG4JPROPERTYFILE");
		  System.out.println("log4jprops location is = "+log4jprops);
		  PropertyConfigurator.configureAndWatch(log4jprops);
	  }
	  catch (Exception ex) {
		  System.out.println("Failed to initialize logger  ==>" + ex.getMessage());
		  ex.printStackTrace();
	  }
  } // End of init method
  
}