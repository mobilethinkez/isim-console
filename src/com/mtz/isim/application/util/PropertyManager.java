package com.mtz.isim.application.util;



import java.util.Enumeration;
import java.util.Hashtable;
import java.util.ResourceBundle;


public class PropertyManager {
	ResourceBundle rb;
	private static Hashtable Propertytable;
	/**
	 * 
	 */
	private PropertyManager() {
		rb = ResourceBundle.getBundle("propertyList");
		init();
	}

	private static PropertyManager propertymanager=null;
	
	static {
		if (propertymanager == null) {
			propertymanager = new PropertyManager();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	synchronized public static PropertyManager getPropertyManager() {
		if (propertymanager == null) {
			propertymanager = new PropertyManager();
		}
		return propertymanager;
	}
	/**
	 * 
	 */
	public void init() {
		Enumeration en = rb.getKeys();
		Propertytable = new Hashtable();
		while (en.hasMoreElements()) {
			String propertyfile = (String) en.nextElement();
			loadProperty(propertyfile);
		}
	}

	synchronized public static PropertyManager resetPropertyManager()
	{
		propertymanager = null;
		propertymanager = new PropertyManager();
		return propertymanager;
	}

	private void loadProperty(String propertyfile) {
		ResourceBundle resourcebundle = ResourceBundle.getBundle(propertyfile);
		Enumeration en = resourcebundle.getKeys();
		while (en.hasMoreElements()) {
			String s = (String) en.nextElement();
			String value = resourcebundle.getString(s);
			System.out.println(s+"->"+value);
			Propertytable.put(s, value);
		}
	}

	public static String getProperty(String skey) {
		return (String) Propertytable.get(skey);
	}
	public static void setProperty(String skey,String val) {
		Propertytable.put(skey,val);
	}
}	