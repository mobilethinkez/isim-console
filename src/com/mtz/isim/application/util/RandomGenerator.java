package com.mtz.isim.application.util;

import java.math.BigInteger;
import java.util.Random;

public class RandomGenerator {

    protected static char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    
    protected int serialLength=10;

    protected BigInteger minValue = new BigInteger("111111111");

    protected BigInteger maxValue = new BigInteger("999999999");

    public int getSerialLength() {
        return serialLength;
    }

    public void setSerialLength(int serialLength) {
        this.serialLength = serialLength;
    }

    public BigInteger getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(BigInteger maxValue) {
        this.maxValue = maxValue;
    }

    public BigInteger getMinValue() {
        return minValue;
    }

    public void setMinValue(BigInteger minValue) {
        this.minValue = minValue;
    }

    public BigInteger generateRandomNumber() {
        BigInteger randNum = null;
        while (randNum == null || randNum.compareTo(minValue) == -1) {
            String randomNumString = ""
                    + (long) (Math.random() * Long.MAX_VALUE)
                    + System.currentTimeMillis()
                    + (long) (Math.random() * Long.MAX_VALUE);
            BigInteger bigInt1 = new BigInteger(randomNumString);
            randNum = bigInt1.mod(maxValue);
        }
        return randNum;
    }

    public String generateRandomAlphaNumeric() {
        StringBuffer result = new StringBuffer();
        Random rand = new Random();
        for (int i = 0; i < serialLength; i++) {
            result.append(letters[rand.nextInt(36)]);
        }
        return result.toString();
    }
    
}
