package com.mtz.isim.application.util;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.mtz.isim.application.vo.CustomerInfo;
import com.mtz.isim.application.vo.OrderInfo;
import com.mtz.isim.application.vo.VendorInfo;
/*
 * 
 * ***************************************
 *	HEADER DESCRIPTION
 ***************************************
 * Input-File:	BTC00005.inp
 * Date:		29.03.2012
 * Type:		SIM BAP CARDS Oberthur 64K
 * Manufacturer:	Oberthur Technologies
 * DeliveryDate:	April 2012
 * Vendor:	Oberthur			
Customer:	THE BAHAMAS TELECOMMUNICATIONS COMPANY
Quantity:	10000
Type:		SIM BAP CARDS Oberthur 64K
Artwork:
Graph_ref:
Profile:	2.01
Address1:	THE BAHAMAS TELECOMMUNICATIONS COMPANY
Address2:	John F. Kennedy Drive
Address3:	PO Box N-3048
Address4:	Nassau, Bahamas
Batch:		5
Transport_key:	27
query1.append("\nGraph_ref:");query1.append("\nProfile:");query1.append("\nAddress1:");query1.append("\nAddress2:");query1.append("\nAddress3:");query1.append("\nBatch:");
query1.append("\nTransport_key:");query1.append("\nIMSI:");query1.append("\nSer_NB:");
 *************************************
 *	INPUT VARIABLES
 *************************************
Var_In_list:
IMSI:		364390001120449
Ser_NB:		890139120027030000
 *************************************
 * 
 * 
 */
public class SACUtility {
	public static BufferedWriter bufferedWriter = null;
	public static String REQUEST_FILE_PATH = PropertyManager.getProperty("REQUEST_FILE_PATH");
	static String finalFilePath;
	public static String createRequestFile(OrderInfo orderInfo, CustomerInfo customerInfo, VendorInfo vendorInfo){
		try {
			LoggerExtension.log(1, "SACUtility..."+REQUEST_FILE_PATH);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String toDayDate = sdf.format(date);
			Date tmpDate = orderInfo.getExpectedDeliveryDate();

			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			String expectedDate = sdf1.format(tmpDate);	

			//File Name format:: < Orderid>_<vendorname>_<requesteddate>.csv
			String fileName1=orderInfo.getOrderId()+"_"+orderInfo.getVendorId()+"_"+toDayDate+".csv";
			finalFilePath = REQUEST_FILE_PATH+"/"+fileName1;
			bufferedWriter = new BufferedWriter(new FileWriter(new File(REQUEST_FILE_PATH+"/"+fileName1),true));
			StringBuffer query1 = new StringBuffer("***************************************");
			query1.append("\n*     CUSTOMER INFORMATION                  ");

			query1.append("\n*********************************************");

			query1.append("\n* Customer ID:"+customerInfo.getCustomerId());

			query1.append("\n* Customer Name:"+customerInfo.getName());

			query1.append("\n* Address:"+customerInfo.getAddress());
			query1.append("\n* City:"+customerInfo.getCity());
            query1.append("\n* State:"+customerInfo.getState());
			query1.append("\n* Country:"+customerInfo.getCountry());
            query1.append("\n* Pincode:"+customerInfo.getPinCode());
			query1.append("\n* Contact No:"+customerInfo.getPhoneNumber());

			query1.append("\n*********************************************");

			query1.append("\n*     SIM VENDOR INFORMATION                  ");

			query1.append("\n*********************************************");

			query1.append("\n* Manfacturer:"+vendorInfo.getName());

			query1.append("\n* Address:"+vendorInfo.getAddress());

			query1.append("\n* Email ID:"+vendorInfo.getEmail());
			query1.append("\n* City:"+vendorInfo.getCity());
            query1.append("\n* State:"+vendorInfo.getState());
			query1.append("\n* Country:"+vendorInfo.getCountry());
            query1.append("\n* Pincode:"+vendorInfo.getPinCode());
			query1.append("\n* Contact No:"+vendorInfo.getContactNumber());

			query1.append("\n**********************************************");

			query1.append("\n*     ORDER INFORMATION                        ");

			query1.append("\n**********************************************");

			query1.append("\n* Order Number:"+orderInfo.getOrderId());

			query1.append("\n* Order Quantity:"+orderInfo.getQuantity());

			query1.append("\n* SIM Prefix:"+orderInfo.getSim_prefix());

			query1.append("\n* START SIM Range:"+orderInfo.getStart_sim());

			query1.append("\n* IMSI Prefix:"+orderInfo.getImsi_prefix());

			query1.append("\n* START IMSI Range:"+orderInfo.getStart_imsi_range());

			query1.append("\n* END IMSI Range:"+orderInfo.getEnd_imsi_range());

			query1.append("\n* Order Type:"+orderInfo.getOrder_type());

			query1.append("\n* Card Type:"+orderInfo.getCard_type());

			query1.append("\n* Transport Key:"+orderInfo.getTransport_key());
			query1.append(("\n* Electronic Profile:"+orderInfo.getElectrical_profile()));
			query1.append(("\n* Graphic Profile:"+orderInfo.getGraphical_profile()));
            query1.append(("\n* Carrier Name:"+orderInfo.getCarrier_name()));
			query1.append(("\n* Network Element:"+orderInfo.getNetwork_element()));
			query1.append("\n* Expected Delivery Date:"+expectedDate);
			query1.append("\n**********************************************");
			LoggerExtension.log(1, "SACUtility...query1.toString()=="+query1.toString());
			bufferedWriter.write(query1.toString());
			bufferedWriter.newLine();
			bufferedWriter.flush();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return finalFilePath;
	}

}