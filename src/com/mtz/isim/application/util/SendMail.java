package com.mtz.isim.application.util;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
/**
 * 
 * @author Satish.Yadav
 *
 * Send a email after successfully creation of user
 */
public class SendMail {
 
	private String from;
	private String to;
	private String subject;
	private String text;
	private String password;
 
	public SendMail(String from, String to, String subject, String text,String password){
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.text = text;
		this.password=password;
	}
 
	public void send(){
 
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.auth", "true");
		props.setProperty( "mail.smtp.port", "587" );
		props.put("mail.smtp.starttls.enable", "true");
	
		Session mailSession = Session.getDefaultInstance(props,new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(
						to, password);//SenderID and Password.
			}
	});
		Message simpleMessage = new MimeMessage(mailSession);
 
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		try {
			fromAddress = new InternetAddress(from);
			toAddress = new InternetAddress(to);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
		try {
			simpleMessage.setFrom(fromAddress);
			simpleMessage.setRecipient(RecipientType.TO, toAddress);
			simpleMessage.setSubject(subject);
			simpleMessage.setText(text);
 
			Transport.send(simpleMessage);
			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}