package com.mtz.isim.application.views;

import java.util.List;

import com.mtz.isim.application.ISIMApplication;
import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.module.OsgiModule;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class HomeView extends HorizontalSplitPanel
{

	private static final long serialVersionUID = -7949452745004903078L;
	private SacSession mSession;
	private VerticalLayout rightSide;	

	public HomeView(ISIMApplication simWebApplication, SacSession aSession, List<OsgiModule> osgiModuleList) {
		mSession = aSession;

		VerticalLayout leftSide = new VerticalLayout();	
		HorizontalLayout leftHLSide =new HorizontalLayout();
		leftSide.setSizeFull();
		leftSide.setMargin(true);
		leftSide.setSpacing(true);
		addComponent(leftSide);

		Component component = null;
		//	for (final OsgiModule module : osgiModuleList) {
		//	if (module.isVisible()&& Entitlements.hasAccess(role, Action.ACCESS, module.getBundleSymbolicName()))
		for (int i = 0; i < osgiModuleList.size(); i++) {
			{
				System.out.println("osgiModuleList.get(i)::"+osgiModuleList.get(i));
				osgiModuleList.get(i).setSession(mSession);
				component = osgiModuleList.get(i).createComponent();
				if(i==0 || i==2 || i==4){
					leftHLSide = new HorizontalLayout();
				}
				leftHLSide.setSizeFull();
				leftHLSide.setMargin(true);
				leftHLSide.setSpacing(true);
				leftHLSide.addComponent(component);
				leftSide.addComponent(leftHLSide);
				//leftSide.setComponentAlignment(component, Alignment.MIDDLE_CENTER);
			}
		}	
		getHomePageUpdates();

	}

	public void getHomePageUpdates(){
		rightSide = new VerticalLayout();		
		rightSide.addComponent(new Label("What is Together? ... WIP ... waiting for the text to be displayed ... "));
		addComponent(rightSide);
		setSizeFull();
		setMargin(true);
	}


}
