package com.mtz.isim.application.views;


import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.Authenticator;
import com.mtz.isim.application.util.LoggerExtension;
import com.mtz.isim.application.vo.UserData;
import com.vaadin.event.Action;
import com.vaadin.event.MouseEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Reindeer;

/*******************************************************************************
 * Description:
 ******************************************************************************/
public class LoginView extends CustomComponent implements Action.Handler {
	private static final long serialVersionUID = 4044393703701535102L;

	private SacSession mSession;
	private TextField mUserNameTextField;
	private PasswordField mPasswordField;
	private Action submitFormAction;
	private Action oneClickLoginAction;
	private Action[] formPanelActions;

	/*******************************************************************************
	 * 
	 ******************************************************************************/
	public LoginView(SacSession aSession) {

		mSession = aSession;

		submitFormAction = new ShortcutAction("do login",
				ShortcutAction.KeyCode.ENTER, null);

		oneClickLoginAction = new ShortcutAction("do it",
				ShortcutAction.KeyCode.SPACEBAR, null);

		formPanelActions = new Action[] { submitFormAction, oneClickLoginAction };

		_init();
	}

	/*******************************************************************************
	 * 
	 ******************************************************************************/
	@SuppressWarnings("serial")
	private void _init() {
		CustomLayout lCustomLayout = new CustomLayout("loginForm");
		lCustomLayout.setCaption(null);

		// Create a few components and bind them to the location tags
		// in the custom layout.
		mUserNameTextField = new TextField();
		mUserNameTextField.setWidth("205px");
		//mUserNameTextField.setHeight("30px");
		mUserNameTextField.setTabIndex(1);

		lCustomLayout.addComponent(mUserNameTextField, "UserName");

		mPasswordField = new PasswordField();
		mPasswordField.setWidth("205px");
		//mPasswordField.setHeight("30px");
		mPasswordField.setTabIndex(2);

		lCustomLayout.addComponent(mPasswordField, "Password");

		Embedded lLoginButton = new Embedded(null, new ThemeResource("../tadtheme/img/submit.png"));
		lLoginButton.setStyleName("loginButton");

		lCustomLayout.addComponent(lLoginButton, "LoginButton");

		Button cancelButton = new Button("Cancel");
		//lSignUpButton.setStyleName(BaseTheme.BUTTON_LINK);
		cancelButton.addStyleName("link");
		cancelButton.setDescription("Cancel a Login");
		lCustomLayout.addComponent(cancelButton, "Cancel");

		Button lForgotPwd = new Button("Forgot password");
		lForgotPwd.setStyleName(BaseTheme.BUTTON_LINK);
		lForgotPwd.addStyleName("link");
		lForgotPwd.setDescription("Click to reset your password");
		
		lCustomLayout.addComponent(lForgotPwd, "ForgotPassword");

		// A layout structure used for composition
		final Panel lPanel = new Panel();
		lPanel.setStyleName(Reindeer.PANEL_LIGHT);
		lPanel.setCaption(null);
		lPanel.setContent(lCustomLayout);
		lPanel.setSizeFull();

		setCompositionRoot(lPanel);

		lPanel.addActionHandler(this);

		// Login action handler.
		lLoginButton.addListener(new MouseEvents.ClickListener() {
			public void click(com.vaadin.event.MouseEvents.ClickEvent aEvent) {
				doLogin();
			}
		});
		// Signup action handler.
		cancelButton.addListener(new Button.ClickListener() {
			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
				lPanel.requestRepaint();
				lPanel.requestRepaintAll();
				mUserNameTextField.setValue("");
				mPasswordField.setValue("");

			}
		});
		// password reminder action handler.
		lForgotPwd.addListener(new Button.ClickListener() {
			public void buttonClick(com.vaadin.ui.Button.ClickEvent event) {
			
				Window lReminderModalWin = new Window("Password reminder");
				lReminderModalWin.setModal(true);
				PasswordReminderView passwordReminderView = new PasswordReminderView(mSession, lReminderModalWin);

				lReminderModalWin.setContent(passwordReminderView);

				getWindow().addWindow(lReminderModalWin);
				lReminderModalWin.center();
			}
		});

	}
	protected void cancelLogin(){

	}
	protected void doLogin() {
		try {
			String user = mUserNameTextField.getValue().toString();
			String pass = mPasswordField.getValue().toString();
			UserData userData = new UserData();
			if (user != null && pass != null && Authenticator.validateUser(user, pass)) {
				userData = Authenticator.getUserDetails(user);
				if(mSession==null){
					LoggerExtension.log(1, "msession is null");
					//	mSession =new SIMManagerSession(aApp);
				}else{
					mSession.setUser(userData);
					mSession.getApplication().getModules();
					mSession.getApplication().initHomePage();
					mSession.getApplication().renderHomePage();

				}

			}else {
				getWindow().showNotification(
						"Invalid Email / Password combination. Please retry",
						Notification.TYPE_ERROR_MESSAGE);
			}
		} catch (Exception e) {

			e.printStackTrace();
			getWindow().showNotification(
					"Invalid Email / Password combination. Please retry",
					Notification.TYPE_ERROR_MESSAGE);
		}
	}


	//@Override
	public Action[] getActions(Object target, Object sender) {
		return formPanelActions;
	}

	//@Override
	public void handleAction(Action action, Object sender, Object target) {
		if (action == submitFormAction) {
			doLogin();
		}

		if (action == oneClickLoginAction) {
			mUserNameTextField.setValue("prashanth238@gmail.com");
			mPasswordField.setValue("testing");
			doLogin();
		}
	}
}