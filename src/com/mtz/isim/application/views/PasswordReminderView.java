package com.mtz.isim.application.views;

import java.util.Arrays;

import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.Authenticator;
import com.mtz.isim.application.vo.User;
import com.mtz.isim.application.vo.UserData;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.service.ApplicationContext;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

@SuppressWarnings("serial")
public class PasswordReminderView extends VerticalLayout {

    private SacSession mSession;
    private UserData userData;
    public PasswordReminderView(SacSession aSession,final Window subwindow) {
    	userData = new UserData();
    	mSession = aSession;
        BeanItem<UserData> userItem = new BeanItem<UserData>(userData);

        // Create the Form
        final Form registerForm = new Form();

        registerForm.setCaption("Please fill in your registered email");
        registerForm.setWriteThrough(false); // we want explicit 'apply'
        registerForm.setInvalidCommitted(false); // no invalid values in
                                                 // datamodel
        registerForm.setImmediate(true);
        // FieldFactory for customizing the fields and adding validators
        registerForm.setFormFieldFactory(new UserFieldFactory());
        registerForm.setItemDataSource(userItem); // bind to POJO via BeanItem

        // Determines which properties are shown, and in which order:
        registerForm.setVisibleItemProperties(Arrays.asList(new String[] { "userName" }));

        // Add form to layout
        addComponent(registerForm);

        // The cancel / apply buttons
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        Button apply = new Button("Submit", new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
                try {
                	if(!registerForm.isValid()){
						
					}
                    registerForm.commit();
                //    UserData userData = new UserData();
                    String userName = userData.getUserName();
                    userData = Authenticator.getUserDetails(userName);
                    if (userData != null) {
                        getWindow().showNotification("Password reminder successfull. ","An email has been sent to the registered email address. ");
                    } else {
                        getWindow().showNotification("Password reminder failed", "The email doesn't exist. Please try again.", Notification.TYPE_ERROR_MESSAGE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //getWindow().showNotification("Password reminder failed","The email doesn't exist. Please try again.", Notification.TYPE_ERROR_MESSAGE);
                }
            }
        });
        buttons.addComponent(apply);

        Button discardChanges = new Button("Clear", new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
                registerForm.discard();
            }
        });
        buttons.addComponent(discardChanges);

        Button close = new Button("Close", new Button.ClickListener() {            
			// inline click-listener            
			public void buttonClick(ClickEvent event) {                
				// close the window by removing it from the parent window                
				(subwindow.getParent()).removeWindow(subwindow);            
			}        
        });  
        buttons.addComponent(close);
        registerForm.getFooter().addComponent(buttons);
        registerForm.getFooter().setMargin(true, true, true, true);
        setSpacing(true);
        setMargin(true);
       setSizeUndefined();
    }

    private class UserFieldFactory extends DefaultFieldFactory {

        @Override
        public Field createField(Item item, Object propertyId, Component uiContext) {
            Field f = super.createField(item, propertyId, uiContext);
            if ("userName".equals(propertyId)) {
                TextField tf = (TextField) f;
                tf.setRequired(true);
                tf.setRequiredError("Please enter an email id");
                tf.setWidth("300px");
                tf.setNullRepresentation("");
                tf.addValidator(new StringLengthValidator("Email must be 3-255 characters", 3, 255, false));
            }
            return f;
        }
    }
}
