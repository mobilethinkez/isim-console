/**
 * 
 */
package com.mtz.isim.application.vo;

/**
 * @author Satish.Yadav
 *
 */
public class CustomerInfo {
	
	public String name="";
	public String address="";
	public String city="";
	public String state="";
	public String country="";
	public String pinCode="";
	public String email="";
	public String phoneNumber="";
	public String customerId="";
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String toString(){
		return "CustomerInfo[customerId:"+customerId+" ,name:"+name+" ,address:"+address+" ,city:"+city+" ,state:"+state+" ,country:"+country
				+" ,pincode:"+pinCode+" ,email:"+email+" ,phoneNumber:"+phoneNumber+" ]";
	}

}
