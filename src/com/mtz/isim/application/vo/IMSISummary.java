package com.mtz.isim.application.vo;

public class IMSISummary {

	public String quantity="";
	public String provImsi="";
	public String notProImsi="";
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getProvImsi() {
		return provImsi;
	}
	public void setProvImsi(String provImsi) {
		this.provImsi = provImsi;
	}
	public String getNotProImsi() {
		return notProImsi;
	}
	public void setNotProImsi(String notProImsi) {
		this.notProImsi = notProImsi;
	}

}
