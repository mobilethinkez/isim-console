package com.mtz.isim.application.vo;

public class ImsiMappingInfo {
	public String imsi="";
	public String msisdn="";
	public String status="";
	public String card_type="";
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "imsi:"+imsi+" msisdn:"+msisdn+" status:"+status+" card_type:"+card_type;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCard_type() {
		return card_type;
	}
	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}


}
