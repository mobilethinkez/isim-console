package com.mtz.isim.application.vo;

import java.util.Date;


public class OrderInfo {
	public String orderId="";
	public String customerId="";
	public String vendorId="";
	public String quantity="";
	public String sim_prefix="890139";
	public String imsi_prefix="364390";
	public String start_sim="120";
	public String start_imsi="";
	
	public long start_imsi_range;
	public long end_imsi_range;
	
	
	public String sim_card_format="";
	public String transport_key="";
	public String electrical_profile="";
	public String carrier_name="";
	public String brand="";
	public String graphical_profile="";
	public String network_element="";
	public String card_type="";
	public String order_type="";
	public Date expectedDeliveryDate;
	public Date creationDate;
	public Date modifyDate;
	public String status="NEW";
	///public String wareHouseCode="";
	
	
	public String toString(){
		return "OrderInfo[orderId:"+orderId+" ,quantity:"+quantity+" ,sim_prefix:"+sim_prefix+" ,imsi_prefix:"+imsi_prefix+" ,sim_card_format:"+sim_card_format
				+" ,transport_key:"+transport_key+" ,electrical_profile:"+electrical_profile+" ,carrier_name:"+carrier_name+" ,brand:"+brand+" ,start_sim:"+start_sim
				+" ,start_imsi:"+start_imsi+" ,graphical_profile:"+graphical_profile+" ,network_element:"+network_element+
				", status:"+status+" ,expectedDeliveryDate:"+expectedDeliveryDate+" ,creationDate:"+creationDate+"" +
						",modifyDate:"+modifyDate+",customerId:"+customerId+",card_type:"+card_type+",customerId:"+customerId+",order_type"+order_type+" ]";
	}
	
	public long getStart_imsi_range() {
		return start_imsi_range;
	}

	public void setStart_imsi_range(long start_imsi_range) {
		this.start_imsi_range = start_imsi_range;
	}


	public long getEnd_imsi_range() {
		return end_imsi_range;
	}

	public void setEnd_imsi_range(long end_imsi_range) {
		this.end_imsi_range = end_imsi_range;
	}

//	public String getWareHouseCode() {
//		return wareHouseCode;
//	}
//
//	public void setWareHouseCode(String wareHouseCode) {
//		this.wareHouseCode = wareHouseCode;
//	}

	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getCard_type() {
		return card_type;
	}
	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}
	public String getOrder_type() {
		return order_type;
	}
	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}
	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getSim_prefix() {
		return sim_prefix;
	}
	public void setSim_prefix(String sim_prefix) {
		this.sim_prefix = sim_prefix;
	}
	public String getImsi_prefix() {
		return imsi_prefix;
	}
	public void setImsi_prefix(String imsi_prefix) {
		this.imsi_prefix = imsi_prefix;
	}
	public String getSim_card_format() {
		return sim_card_format;
	}
	public void setSim_card_format(String sim_card_format) {
		this.sim_card_format = sim_card_format;
	}
	public String getTransport_key() {
		return transport_key;
	}
	public void setTransport_key(String transport_key) {
		this.transport_key = transport_key;
	}
	public String getElectrical_profile() {
		return electrical_profile;
	}
	public void setElectrical_profile(String electrical_profile) {
		this.electrical_profile = electrical_profile;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getStart_sim() {
		return start_sim;
	}
	public void setStart_sim(String start_sim) {
		this.start_sim = start_sim;
	}
	public String getStart_imsi() {
		return start_imsi;
	}
	public void setStart_imsi(String start_imsi) {
		this.start_imsi = start_imsi;
	}
	public String getGraphical_profile() {
		return graphical_profile;
	}
	public void setGraphical_profile(String graphical_profile) {
		this.graphical_profile = graphical_profile;
	}
	public String getNetwork_element() {
		return network_element;
	}
	public void setNetwork_element(String network_element) {
		this.network_element = network_element;
	}

	
}
