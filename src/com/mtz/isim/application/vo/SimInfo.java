package com.mtz.isim.application.vo;

public class SimInfo {

	public String order_id="";
	public String imsi="";
	public String serial_number="";
	public String pin1="";
	public String puk1="";
	public String pin2="";
	public String puk2="";
	public String ki="";
	public String adm1="";
	public String access_control="";
	public String msisdn="";
	public String status="";
	public String card_type="";
	
	public String toString(){
		return "SIMINFO[order_id:"+order_id+ " ,imsi:"+imsi+" ,serial_number:"+serial_number+" ,pin1:"+pin1+" ,puk1:"+puk1+" ,pin2:"+pin2+" ,puk2:"+puk2
				+" ki:"+ki+" ,adm1:"+adm1+" ,access_control:"+access_control+" ,status:"+status+" ,msisdn:"+msisdn+" card_type:"+card_type+" ]";
	}
	
	public String getCard_type() {
		return card_type;
	}

	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getSerial_number() {
		return serial_number;
	}
	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}
	public String getPin1() {
		return pin1;
	}
	public void setPin1(String pin1) {
		this.pin1 = pin1;
	}
	public String getPuk1() {
		return puk1;
	}
	public void setPuk1(String puk1) {
		this.puk1 = puk1;
	}
	public String getPin2() {
		return pin2;
	}
	public void setPin2(String pin2) {
		this.pin2 = pin2;
	}
	public String getPuk2() {
		return puk2;
	}
	public void setPuk2(String puk2) {
		this.puk2 = puk2;
	}
	public String getKi() {
		return ki;
	}
	public void setKi(String ki) {
		this.ki = ki;
	}
	public String getAdm1() {
		return adm1;
	}
	public void setAdm1(String adm1) {
		this.adm1 = adm1;
	}
	public String getAccess_control() {
		return access_control;
	}
	public void setAccess_control(String access_control) {
		this.access_control = access_control;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}


	
}
