/**
 * 
 */
package com.mtz.isim.application.vo;

/**
 * @author Satish.Yadav
 *
 */
import java.io.Serializable;
import java.util.Date;

public class UserData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -614314401434294973L;
	private String userName;
	private String userRole;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String phoneNumber;
	private String status;
	private Date lastLoggedIn;
	private Date registerDate;
	
	public UserData(String userName, String userRole, boolean canEdit) {
		super();
		this.userName = userName;
		this.userRole = userRole;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastLoggedIn() {
		return lastLoggedIn;
	}

	public void setLastLoggedIn(Date lastLoggedIn) {
		this.lastLoggedIn = lastLoggedIn;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public UserData(String userName, String userRole) {
		super();
		this.userName = userName;
		this.userRole = userRole;
	}


	public UserData() {
		super();
	}

	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserRole() {
		return userRole;
	}
	
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	

	
	
		@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(userName);
		builder.append(":");
		builder.append(userRole);
		builder.append(":");
		return builder.toString();
	}
	
}

