package com.mtz.isim.application.vo;

import java.util.Date;

public class VendorInfo {
	public String name="";
	public String address="";
	public String city="";
	public String state="";
	public String country="";
	public String pinCode="";
	public String email="";
	public String contactNumber="";
	public String vendorId="";

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getVendorId() {
		return vendorId;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String toString(){
		return "VendorInfo [vendorId:"+vendorId+" ,name:"+name+" ,address:"+address+" ,city:"+city+" ,state:"+state+" ,country:"+country
				+" ,pincode:"+pinCode+" ,email:"+email+" ,phoneNumber:"+contactNumber+" ]";
	}
}
