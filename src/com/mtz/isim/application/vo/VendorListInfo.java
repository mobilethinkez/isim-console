package com.mtz.isim.application.vo;

public class VendorListInfo {
	public String vendorName;

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String toString(){
		return "VendorName:"+vendorName;
	}
}
