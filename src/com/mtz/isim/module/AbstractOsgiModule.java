package com.mtz.isim.module;

import org.apache.log4j.Logger;

import com.mtz.isim.application.session.SacSession;
import com.mtz.isim.application.util.ClasspathStreamSource;
import com.vaadin.Application;
import com.vaadin.event.MouseEvents;
import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.terminal.StreamResource;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;

public abstract class AbstractOsgiModule implements OsgiModule {

	public static final String BUNDLE_SYMBOLIC_NAME = "com.zenithss.together.common";
	private static final String DEFAULT_VERSION = "1.0.0";
	private Logger logger = Logger.getLogger(getClass());
	private SacSession session;

	// Abstract Methods
	public abstract String getName();

	public abstract String getDescription();
	
	public abstract String getBundleSymbolicName();

	public abstract String getImage();

	public abstract Class<?> getImplementationClass();

	public abstract Component getView(SacSession session);

	public abstract void deactivate();

	// Concrete Methods
	public String getVersion() {
		return DEFAULT_VERSION;
	}

	public Component createComponent() {
		System.out.println("createComponent method...........");
		Application app = (Application) session.getApplication();
		StreamResource.StreamSource streamSource = new ClasspathStreamSource(getImage(), getImplementationClass());
		StreamResource resource = new StreamResource(streamSource, getImage(),app);
		Embedded lButton = new Embedded(null, resource);
		lButton.setDescription(getName());
		lButton.addListener(new MouseEvents.ClickListener() {
			private static final long serialVersionUID = 15721354980L;

			public void click(ClickEvent event) {
				Component view = getView(session);
				session.getApplication().showView(view);
			}
		});
		return lButton;
	}



	public void setSession(SacSession session) {
		System.out.println("setSession method::"+session.toString());
		this.session = session;
	}

	public String toString() {
		return getName() + "Module";
	}
}
