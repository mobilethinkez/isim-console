package com.mtz.isim.module;

import java.util.List;

import com.vaadin.ui.Component;

public interface ModulableApplication {

	public void showView(final Component aComp);
	public List<OsgiModule> getModules();
	public void renderHomePage();
	public void initHomePage();
}
