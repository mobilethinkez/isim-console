package com.mtz.isim.module;

import com.mtz.isim.application.session.SacSession;
import com.vaadin.ui.Component;

public interface OsgiModule {

	public String getName();
	
	public String getDescription();
	
	public String getBundleSymbolicName();

	public String getVersion();
	
	public Component createComponent();

	public void setSession(SacSession session);

	public void deactivate();
	
	public boolean isVisible();
}
