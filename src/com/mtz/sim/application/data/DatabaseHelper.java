package com.mtz.sim.application.data;import java.io.Serializable;
import java.sql.SQLException;

import com.vaadin.addon.sqlcontainer.SQLContainer;
import com.vaadin.addon.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.addon.sqlcontainer.connection.SimpleJDBCConnectionPool;
import com.vaadin.addon.sqlcontainer.query.TableQuery;

@SuppressWarnings("serial")
public class DatabaseHelper implements Serializable {
    
	/**
	 * Natural property order for Person bean. Used in tables and forms.
	 */
	public static final Object[] NATURAL_COL_ORDER_SEARCH = new Object[] {
			"order_Id", "imsi", "serial_number"	};
	
	/**
	 * Natural property order for Person bean. Used in tables and forms.
	 */
	public static final Object[] COL_HEADERS_ENGLISH_SEARCH = new Object[] {
			"ORDER ID", "IMSI PREFIX","SIM SERIAL NO"	};
	
	/**
     * Natural property order for SQLContainer linked with the PersonAddress
     * database table. Used in tables and forms.
     */
    public static final Object[] NATURAL_COL_ORDER = new Object[] {"ID","order_Id", "imsi", "serial_number","msisdn","status"};

    /**
     * "Human readable" captions for properties in same order as in
     * NATURAL_COL_ORDER.
     */
//    public static final String[] COL_HEADERS_ENGLISH = new String[] {"OrderID", "IMSI", "Serial Number","pin1","puk1",
//    	"pin2","puk2","ki","adm1","Access Control","msisdn","status"};

    public static final String[] COL_HEADERS_ENGLISH = new String[] {"ID","OrderID", "IMSI", "Serial Number","MSISDM","PROV STATUS"};

    /**
     * JDBC Connection pool and the two SQLContainers connecting to the persons
     * and cities DB tables.
     */
    private JDBCConnectionPool connectionPool = null;
    private SQLContainer simInfoContainer = null;
    private SQLContainer imsiStatusInfoContainer = null;

    /**
     * Enable debug mode to output SQL queries to System.out.
     */
    private boolean debugMode = false;

    public DatabaseHelper() {
        initConnectionPool();
       // initDatabase();
        initContainers();
      //  fillContainers();
    }

    private void initConnectionPool() {
        try {
            //connectionPool = new SimpleJDBCConnectionPool( "org.hsqldb.jdbc.JDBCDriver",  "jdbc:hsqldb:mem:sqlcontainer", "SA", "", 2, 5);
            connectionPool = new SimpleJDBCConnectionPool( "com.mysql.jdbc.Driver",  "jdbc:mysql://localhost:3306/sac_db", "root", "mysql", 2, 5);
        
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

   
    private void initContainers() {
        try {
            /* TableQuery and SQLContainer for personaddress -table */
            TableQuery q1 = new TableQuery("sim_info", connectionPool);
            q1.setVersionColumn("VERSION");
            q1.setDebug(debugMode);
            simInfoContainer = new SQLContainer(q1);
            simInfoContainer.setDebugMode(debugMode);

            /* TableQuery and SQLContainer for city -table */
            TableQuery q2 = new TableQuery("imsi_status_info", connectionPool);
            q2.setVersionColumn("VERSION");
            q2.setDebug(debugMode);
            imsiStatusInfoContainer = new SQLContainer(q2);
            imsiStatusInfoContainer.setDebugMode(debugMode);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

  
//    public SQLContainer get() {
//        return simInfoContainer;
//    }
//
//    public SQLContainer getCityContainer() {
//        return imsiStatusInfoContainer;
//    }

//     public String getImsiNumber(int orderId) {
//        Object orderItemId = imsiStatusInfoContainer.getIdByIndex(orderId);
//      //  Object orderItemId = imsiStatusInfoContainer.getItem(orderId);
//        return imsiStatusInfoContainer.getItem(orderItemId).getItemProperty("IMSI") .getValue().toString();
//    }

	public SQLContainer getSimInfoContainer() {
		return simInfoContainer;
	}

//	public void setSimInfoContainer(SQLContainer simInfoContainer) {
//		this.simInfoContainer = simInfoContainer;
//	}

	public SQLContainer getImsiStatusInfoContainer() {
		return imsiStatusInfoContainer;
	}

//	public void setImsiStatusInfoContainer(SQLContainer imsiStatusInfoContainer) {
//		this.imsiStatusInfoContainer = imsiStatusInfoContainer;
//	}
    

   }