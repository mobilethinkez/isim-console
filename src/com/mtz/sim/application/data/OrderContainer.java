package com.mtz.sim.application.data;

import java.io.Serializable;

import com.mobilethinkez.sim.application.tabs.ui.SearchFilter;
import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.vo.OrderInfo;
import com.vaadin.data.util.BeanItemContainer;

public class OrderContainer  extends BeanItemContainer<OrderInfo> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Natural property order for Person bean. Used in tables and forms.
	 */
	
	
	/**
	 * Natural property order for Person bean. Used in tables and forms.
	 */
	public static final Object[] NATURAL_COL_ORDER_SEARCH = new Object[] {
			"order_Id", "imsi_prefix","sim_prefix","start_imsi","start_sim"	};
	
	/**
	 * Natural property order for Person bean. Used in tables and forms.
	 */
	public static final Object[] COL_HEADERS_ENGLISH_SEARCH = new Object[] {
			"ORDER ID", "IMSI PREFIX","SIM PREFIX","IMSI SERIAL NO","SIM SERIAL NO"	};
	
	
	
	/**
	 * "Human readable" captions for properties in same order as in
	 * NATURAL_COL_ORDER.
	 */
	//order_id, customer_id, vendor_id, quantity, sim_prefix, imsi_prefix, start_sim, start_imsi, 
		//sim_card_format, transport_key, electrical_profile, carrier_name, brand, graphical_profile,
		//network_element, card_type, order_type, expectedDeliveryDate, creationDate, modifyDate, status, start_imsi_range, end_imsi_range

//	public static final Object[] NATURAL_COL_ORDER = new Object[] {
//				"orderId","customerId","vendorId", "quantity", "sim_prefix", "imsi_prefix","start_sim","start_imsi", 
//				"sim_card_format","transport_key", "electrical_profile",
//				"carrier_name","brand","start_imsi","graphical_profile","network_element","status","card_type","creationDate","start_imsi_range","end_imsi_range",
//				"order_type","expectedDeliveryDate","modifyDate","status"
//				};
	public static final Object[] NATURAL_COL_ORDER = new Object[] {
		"orderId","creationDate","quantity", "sim_prefix","start_sim","imsi_prefix","status","start_imsi_range","end_imsi_range"
		};	
	public static final String[] COL_HEADERS_ENGLISH = new String[] {
		"orderId","creationDate", "quantity", "sim_prefix","start_sim","imsi_prefix","status","start_imsi_range","end_imsi_range"
		};
/**
public String orderId="";
	public String customerId="";
	public String vendorId="";
	public String quantity="";
	public String sim_prefix="";
	public String imsi_prefix="";
	public String start_sim="";
	public String start_imsi="";
	
	public long start_imsi_range;
	public long end_imsi_range;
	
	
	public String sim_card_format="";
	public String transport_key="";
	public String electrical_profile="";
	public String carrier_name="";
	public String brand="";
	public String graphical_profile="";
	public String network_element="";
	public String card_type="";
	public String order_type="";
	public Date expectedDeliveryDate;
	public Date creationDate;
	public Date modifyDate;
	public String status="NEW";
 */
	
	public OrderContainer() throws InstantiationException,IllegalAccessException {
		super(OrderInfo.class);
	}
	
	public static OrderContainer getOrderList(SearchFilter searchFilter){
		OrderContainer orderContainer= null;
		try{
			orderContainer = new OrderContainer();
			orderContainer = DBManager.getOrderList(orderContainer,searchFilter);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return orderContainer;
	}
	public static OrderContainer getOrderList(String orderid){
		OrderContainer orderContainer= null;
		try{
			orderContainer = new OrderContainer();
			orderContainer = DBManager.getOrderList(orderContainer,orderid);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return orderContainer;
	}
	
}
