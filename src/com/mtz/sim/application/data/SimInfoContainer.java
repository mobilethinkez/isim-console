package com.mtz.sim.application.data;

import java.io.Serializable;

import com.mobilethinkez.sim.application.tabs.ui.SearchFilter;
import com.mtz.isim.application.util.DBManager;
import com.mtz.isim.application.vo.SimInfo;
import com.vaadin.data.util.BeanItemContainer;

public class SimInfoContainer  extends BeanItemContainer<SimInfo> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Natural property order for Person bean. Used in tables and forms.
	 */
	public static final Object[] NATURAL_COL_ORDER_SEARCH = new Object[] {
			"order_id", "imsi", "serial_number"	};
	
	/**
	 * Natural property order for Person bean. Used in tables and forms.
	 */
	public static final Object[] COL_HEADERS_ENGLISH_SEARCH = new Object[] {
			"ORDER ID", "IMSI PREFIX","SIM SERIAL NO"	};
	
	  public static final Object[] NATURAL_COL_ORDER = new Object[] {"order_id", "imsi", "serial_number","msisdn","status"};

	    /**
	     * "Human readable" captions for properties in same order as in
	     * NATURAL_COL_ORDER.
	     */
//	    public static final String[] COL_HEADERS_ENGLISH = new String[] {"OrderID", "IMSI", "Serial Number","pin1","puk1",
//	    	"pin2","puk2","ki","adm1","Access Control","msisdn","status"};

	    public static final String[] COL_HEADERS_ENGLISH = new String[] {"OrderID", "IMSI", "Serial Number","MSISDM","PROV STATUS"};

	
	

	
	public SimInfoContainer() throws InstantiationException,IllegalAccessException {
		super(SimInfo.class);
	}
	
	public static SimInfoContainer getImsiList(SearchFilter searchFilter){
		SimInfoContainer orderContainer= null;
		try{
			orderContainer = new SimInfoContainer();
			orderContainer = DBManager.getImsiList(orderContainer,searchFilter);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return orderContainer;
	}
	
}

