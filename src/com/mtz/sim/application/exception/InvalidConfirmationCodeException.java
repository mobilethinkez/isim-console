package com.mtz.sim.application.exception;

public class InvalidConfirmationCodeException extends Exception {

	private static final long serialVersionUID = -1289802601179391254L;

	public InvalidConfirmationCodeException() {
	}

	public InvalidConfirmationCodeException(String arg0) {
		super(arg0);
	}

	public InvalidConfirmationCodeException(Throwable arg0) {
		super(arg0);
	}

	public InvalidConfirmationCodeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}