package com.mtz.sim.application.exception;

public class LoginFailedException extends Exception {

	private static final long serialVersionUID = 8452844947703655545L;

	public LoginFailedException() {
	}

	public LoginFailedException(String arg0) {
		super(arg0);
	}

	public LoginFailedException(Throwable arg0) {
		super(arg0);
	}

	public LoginFailedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
