package com.mtz.sim.application.exception;

public class UserExistsException extends Exception {

	private static final long serialVersionUID = 1393951880741353284L;

	public UserExistsException() {
	}

	public UserExistsException(String arg0) {
		super(arg0);
	}

	public UserExistsException(Throwable arg0) {
		super(arg0);
	}

	public UserExistsException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}