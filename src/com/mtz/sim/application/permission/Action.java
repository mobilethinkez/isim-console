package com.mtz.sim.application.permission;

public class Action {
	public final static String VIEW="view";
	public final static String ACCESS="access";
	public final static String EDIT="edit";
	public final static String DELETE="delete";
}
