package com.mtz.sim.application.permission;

import org.vaadin.appfoundation.authorization.Permissions;
import org.vaadin.appfoundation.authorization.Resource;
import org.vaadin.appfoundation.authorization.Role;
import org.vaadin.appfoundation.authorization.memory.MemoryPermissionManager;

import com.vaadin.Application;

public class Entitlements {

	public static void load(Application application) {
		Permissions.initialize(application, new MemoryPermissionManager());
		Role testerRole = new TogetherRole("Tester");
		Role adminRole = new TogetherRole("Admin");
		Resource adminModule = new TogetherResource("com.zenithss.together.modules.admin");
		
		Permissions.allow(adminRole, Action.ACCESS, adminModule);
		Permissions.allowAll(adminRole, new Resource() {
			
			@Override
			public String getIdentifier() {
				// TODO Auto-generated method stub
				return "ADMIN";
			}
		});
	}
	
	public static boolean hasAccess(Role role, String action, String resource ){
		return Permissions.hasAccess(role, action, new TogetherResource(resource));
	}
}
