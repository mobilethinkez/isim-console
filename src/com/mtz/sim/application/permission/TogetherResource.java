package com.mtz.sim.application.permission;

import org.vaadin.appfoundation.authorization.Resource;

public class TogetherResource implements Resource {

	private String identifier;

	public TogetherResource(String identifier) {
		this.identifier = identifier;
	}

	@Override
	public String getIdentifier() {
		return identifier;
	}

}
