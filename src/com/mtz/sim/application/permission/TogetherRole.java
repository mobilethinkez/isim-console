package com.mtz.sim.application.permission;

import java.util.HashSet;
import java.util.Set;

import org.vaadin.appfoundation.authorization.Role;

public class TogetherRole implements Role {
	
	private String identifier;
	private Set<Role> roles = new HashSet<Role>();
	
	public TogetherRole(String identifier){
		this.identifier=identifier;
	}
	
	@Override
	public String getIdentifier() {
		return identifier;
	}

	@Override
	public Set<Role> getRoles() {
		return roles;
	}

	@Override
	public void setRoles(Set<Role> roles) {
		this.roles=roles;
	}

	@Override
	public void addRole(Role role) {
		this.roles.add(role);
	}

	@Override
	public void removeRole(Role role) {
		this.roles.remove(role);
	}

}
